﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace GameObjectHolders
{
    [CustomEditor(typeof(GameObjectHolder))]
    public class GameObjectHolderDrawer : Editor
    {
        private void OnEnable()
        {

        }

        public override void OnInspectorGUI()
        {
            GameObjectHolder holder = (GameObjectHolder)target;

            GameObject heldObject = holder.GetGameObject();

            if (heldObject)
            {
                EditorGUILayout.LabelField("Name: " + heldObject.name + " ID: " + heldObject.GetInstanceID());
            }
            else
            {
                EditorGUILayout.LabelField("There is no GameObject currently held in this holder.");
            }

            if (!Application.isPlaying)
            {
                EditorGUILayout.LabelField("Game is currently in editor mode. Information displayed may be inaccurate.");

                if (heldObject)
                {
                    if (GUILayout.Button("Reset Holder"))
                    {
                        holder.SetGameObject(null);
                    }
                }
            }

            if (GUILayout.Button("Check for update"))
            {
                Debug.Log("TEST BUTTON");
            }
        }
    }
}
