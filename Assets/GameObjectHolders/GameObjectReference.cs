﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GameObjectHolders
{
    [Serializable]
    public class GameObjectReference
    {
        public enum GOReferenceType
        {
            Holder,
            Direct
        }

        public GOReferenceType referenceType;

        [SerializeField]    //these should not be settable outside the inspector; private + serializefield ensures we use GetGameObject.
        private GameObjectHolder holder;
        [SerializeField]
        private GameObject direct;

        public GameObject GetGameObject()
        {
            if (referenceType == GOReferenceType.Holder)
            {
                if (!holder) return null;

                return holder.GetGameObject();
            }
            else return direct;
        }

        /// <summary>
        /// Get a component from the referenced gameobject.
        /// </summary>
        public T GetReferencedComponent<T>()
        {
            return GetGameObject().GetComponent<T>();
        }

        public List<T> GetReferencedComponents<T>()
        {
            return GetGameObject().GetComponents<T>().ToList();
        }
    }
}
