﻿using UnityEngine;

namespace GameObjectHolders
{
    [CreateAssetMenu(fileName = "GameObjectHolder", menuName = "GameObject Holder", order = 555)]
    public class GameObjectHolder : ScriptableObject
    {
        private GameObject gameObject;

        public GameObject GetGameObject()
        {
            return gameObject;
        }

        public T GetComponent<T>()
        {
            return gameObject.GetComponent<T>();
        }

        public void SetGameObject(GameObject gameObject)
        {
            this.gameObject = gameObject;
        }

        private void OnDisable()
        {
            gameObject = null;  //reset gameobject on exiting play mode
        }

        public void SendMessage(string message)
        {
            GetGameObject().SendMessage(message);
        }

        public void SendMessageUpwards(string message)
        {
            GetGameObject().SendMessageUpwards(message);
        }
    }
}