﻿using UnityEngine;
using System.Collections;

namespace GameObjectHolders
{
    public class GameObjectHolderSetter : MonoBehaviour
    {
        public GameObjectHolder holder;
        [Tooltip("If this is true, if the gameobject holder already has a gameobject set, it will overwrite it. Otherwise, it will do nothing.")]
        public bool canOverwrite = true;
        [Tooltip("If this is true, if we can't overwrite - canOverwrite == false - destroy our gameobject.")]
        public bool destroyIfNoOverwrite;

        private void Awake()
        {
            if (canOverwrite)
            {   //if we can overwrite, we can set the gameobject regardless of state.
                holder.SetGameObject(gameObject);
            }
            else if (!holder.GetGameObject())
            {   //if we can't overwrite, but there isn't anything to overwrite, we can set the gameobject.
                holder.SetGameObject(gameObject);
            }
            else
            {   //if we can't overwrite, and there is a gameobject set
                if (destroyIfNoOverwrite)
                {
                    Destroy(gameObject);
                }
            }

        }
    }
}
