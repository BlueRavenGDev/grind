﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System.Reflection;
using System;
using System.Linq;

namespace CustomInputManager
{
    [CustomEditor(typeof(Inputs))]
    public class InputsDrawer : Editor
    {
        private Inputs input;

        private List<OurInput> defaultInputs;

        private SerializedProperty defaultInputsSer;

        private void OnEnable()
        {
            defaultInputsSer = serializedObject.FindProperty("defaultInputs");

            Inputs baseInputs = serializedObject.targetObject as Inputs;
            input = baseInputs;  //get the base inputs
            defaultInputs = baseInputs.defaultInputs;

            if (defaultInputs == null)
            {
                defaultInputs = new List<OurInput>();
                baseInputs.defaultInputs = defaultInputs;   //we don't do this and we don't share a reference
            }
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUI.BeginChangeCheck();

            for (int i = 0; i < defaultInputs.Count; i++)
            {
                OurInput input = defaultInputs[i];

                input.inputName = EditorGUILayout.TextField("Input Name:", input.inputName);
                input.inputValue = (KeyCode)EditorGUILayout.EnumPopup("Input Value:", input.inputValue);

                defaultInputs[i] = input;

                ButtonMoveUp(i);
                ButtonMoveDown(i);

                if (GUILayout.Button("Remove"))
                {
                    this.input.defaultInputs.RemoveAt(i);
                }

                EditorGUILayout.Space();
            }

            if (GUILayout.Button("Add"))
            {
                this.input.defaultInputs.Add(new OurInput("InputName", KeyCode.None));
            }

            if (GUILayout.Button("Reset Player Prefs"))
                ResetPlayerPrefs();

            input.defaultInputs = defaultInputs;

            if (EditorGUI.EndChangeCheck())
            {
                EditorUtility.SetDirty(input);
                AssetDatabase.SaveAssets();
            }
        }

        private void ButtonMoveUp(int i)
        {
            if (i != 0)
            {
                if (GUILayout.Button("Move Up"))
                {   //note that this moves UP visually and DOWN in the list.
                    this.input.defaultInputs.Move(i, i - 1);
                }
            }
        }

        private void ButtonMoveDown(int i)
        {
            if (i != defaultInputs.Count - 1)
            {
                if (GUILayout.Button("Move Down"))
                {
                    this.input.defaultInputs.Move(i, i + 1);
                }
            }
        }

        private void ResetPlayerPrefs()
        {   //remove all our inputs
            foreach (OurInput input in defaultInputs)
            {
                PlayerPrefs.DeleteKey(input.inputName);
            }
        }
    }

    public static class InputsExtentions
    {   //source https://stackoverflow.com/questions/450233/generic-list-moving-an-item-within-the-list
        public static void Move<T>(this List<T> list, int oldIndex, int newIndex)
        {
            // exit if positions are equal or outside array
            if ((oldIndex == newIndex) || (0 > oldIndex) || (oldIndex >= list.Count) || (0 > newIndex) ||
                (newIndex >= list.Count)) return;
            // local variables
            var i = 0;
            T tmp = list[oldIndex];
            // move element down and shift other elements up
            if (oldIndex < newIndex)
            {
                for (i = oldIndex; i < newIndex; i++)
                {
                    list[i] = list[i + 1];
                }
            }
            // move element up and shift other elements down
            else
            {
                for (i = oldIndex; i > newIndex; i--)
                {
                    list[i] = list[i - 1];
                }
            }
            // put element from position 1 to destination
            list[newIndex] = tmp;
        }
    }
}
