﻿using UnityEngine;
using System.Collections;

namespace CustomInputManager
{
    public class InputManager : MonoBehaviour
    {
        public static InputManager instance;

        public Inputs inputs;

        public void Awake()
        {
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public void StartListenForKey(string inputName)
        {
            StartCoroutine(inputs.ListenForAndChangeInput(inputName));
        }

        private IEnumerator EndListenForKey()
        {
            yield return new WaitForSeconds(inputs.inputWaitTime);
            StopAllCoroutines();
        }
    }
}