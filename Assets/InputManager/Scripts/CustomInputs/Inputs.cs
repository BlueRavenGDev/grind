﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace CustomInputManager
{
    [CreateAssetMenu(fileName = "Inputs", order = 101)]
    public class Inputs : ScriptableObject
    {
        public List<OurInput> defaultInputs;
        public Dictionary<string, KeyCode> currentInputs;

        public int inputWaitTime = 6;

        public KeyCode[] modKeys = new KeyCode[]
        {
        KeyCode.LeftAlt,
        KeyCode.RightAlt,
        KeyCode.LeftShift,
        KeyCode.RightShift,
        KeyCode.LeftControl,
        KeyCode.RightControl,
        KeyCode.LeftWindows,
        KeyCode.RightWindows,
        KeyCode.LeftApple,
        KeyCode.RightApple,
        KeyCode.LeftCommand,
        KeyCode.RightCommand
        };
        public int modWaitTime = 3;

        public void OnEnable()
        {
            currentInputs = new Dictionary<string, KeyCode>();
            //set current inputs to defaults
            foreach (OurInput input in defaultInputs)
            {
                currentInputs.Add(input.inputName, input.inputValue);
            }

            LoadInputs();
        }

        private void LoadInputs()
        {   //TODO do this better
            for (int i = 0; i < defaultInputs.Count; i++)
            {
                //get the input for this iteration
                OurInput input = defaultInputs[i];
                //Get the input value, if it exists, from the PlayerPrefs. If it can't find it, it uses the default value at the current defaultInputs index

                int thisinput = PlayerPrefs.GetInt(input.inputName, (int)input.inputValue);

                currentInputs[input.inputName] = (KeyCode)thisinput;
            }
        }


        /// <summary>
        /// Coroutine function that waits for an input key.
        /// If one is found, it sets the given input to that key.
        /// </summary>
        /// <param name="toChange">The input name to change.</param>
        public IEnumerator ListenForAndChangeInput(string toChange)
        {
            while (true)
            {
                string key = UnityEngine.Input.inputString;
                KeyCode fkey = (KeyCode)Enum.Parse(typeof(KeyCode), key);   //TODO this may not work, might need to be rethought
                                                                            //if we were pressing a key this frame
                if (!string.IsNullOrEmpty(key))
                {
                    if (modKeys.ToList().Contains((KeyCode)Enum.Parse(typeof(KeyCode), UnityEngine.Input.inputString)))
                    {   //if our pressed key is a mod key, we wait some time before setting the key instead.
                        yield return new WaitForSeconds(modWaitTime);
                    }

                    ChangeInput(toChange, fkey);
                }

                yield return new WaitForEndOfFrame();
            }
        }

        public bool IsInputDown(string inputName)
        {
            if (currentInputs.ContainsKey(inputName))
                return IsKeyDown(currentInputs[inputName]);
            else
            {
                Debug.LogError("Tried to access input " + inputName + ", which does not exist");
                return false;
            }
        }

        public bool IsInput(string inputName)
        {
            if (currentInputs.ContainsKey(inputName))
                return IsKey(currentInputs[inputName]);
            else
            {
                Debug.LogError("Tried to access input " + inputName + ", which does not exist");
                return false;
            }
        }

        public bool IsInputUp(string inputName)
        {
            if (currentInputs.ContainsKey(inputName))
                return IsKeyUp(currentInputs[inputName]);
            else
            {
                Debug.LogError("Tried to access input " + inputName + ", which does not exist");
                return false;
            }
        }

        public bool IsKeyDown(KeyCode key)
        {
            return UnityEngine.Input.GetKeyDown(key);
        }

        public bool IsKey(KeyCode key)
        {
            return UnityEngine.Input.GetKey(key);
        }

        public bool IsKeyUp(KeyCode key)
        {
            return UnityEngine.Input.GetKeyUp(key);
        }

        public void ChangeInput(OurInput changeTo)
        {
            currentInputs[changeTo.inputName] = changeTo.inputValue;
            PlayerPrefs.SetInt(changeTo.inputName, (int)changeTo.inputValue);
        }

        public void ChangeInput(string inputName, KeyCode inputValue)
        {
            currentInputs[inputName] = inputValue;
            PlayerPrefs.SetInt(inputName, (int)inputValue);
        }
    }

    [Serializable]
    public struct OurInput
    {
        public string inputName;
        public KeyCode inputValue;

        public OurInput(string inputName, KeyCode inputValue)
        {
            this.inputName = inputName;
            this.inputValue = inputValue;
        }
    }
}
