﻿using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Recipe", menuName = "Grind/Recipe")]
public class Recipe : ScriptableObject
{
    public enum RecipeType
    {
        BlacksmithUpgrade,
        BlacksmithCraft,
        InventoryCraft
    }

    public List<ItemStack> inputItems;

    public ItemStack outputItem;

    public bool costsGold;
    public int goldCost;

    public RecipeType recipeType;
}