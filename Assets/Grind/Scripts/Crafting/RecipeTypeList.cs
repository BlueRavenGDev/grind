﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "RecipeTypeList", menuName = "Grind/Recipe Type List")]
public class RecipeTypeList : ScriptableObject
{
    public Recipe.RecipeType recipeType;

    public Equipment.EquipmentType equipmentType;

    public ItemArmor.ArmorType armorType;
    public ItemWeapon.WeaponType weaponType;

    public List<Recipe> recipes;
}
