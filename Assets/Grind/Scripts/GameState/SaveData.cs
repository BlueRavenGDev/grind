﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public struct SaveData 
{
    public bool valid;

    public string saveName;

    public List<SerializableItemInstance> inventoryItems;

    public SaveData(string saveName, List<SerializableItemInstance> inventoryItems)
    {
        valid = true;
        this.saveName = saveName;
        this.inventoryItems = inventoryItems;
    }
}
