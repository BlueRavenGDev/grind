﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MonsterData : ScriptableObject
{
    public GameObject monsterPrefab;

    public List<Vector2> startPositions;

    [Tooltip("invaders do not need to be killed - they do not count towards quest completion nor do they show up on the quest board.")]
    public bool invader;
    [Range(0, 1), Tooltip("Chance, from 0 (0%) to 1 (100%) that this monster will invade. Only used if invader is set to true.")]
    public float invadeChance;
}
