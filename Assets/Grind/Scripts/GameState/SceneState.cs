﻿using UnityEngine;
using System.Collections;
using GameObjectHolders;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// ReturnState determines the rewards you gain and animations that are playing.
/// Failure - Few rewards.
/// Abandon - No rewards.
/// Success - all rewards.
/// </summary>
public enum ReturnState
{
    NotReturning,
    Failure,
    Abandon,
    Success
}

public class SceneState : MonoBehaviour
{
    public GameState.WorldType worldType;

    public GameObjectReference gameState;
    private GameState _gameState;

    public GameObjectReference player;
    private Inventory inventory;

    public GameObjectReference playerPhysical;

    public const float returnToTownTime = 60;
    public bool overrideReturnToTownTime;
    public float overrideTime = 1;

    public int lastSpawnIndex;

    [Header("Scene Data")]
    public List<PlayerSpawn> playerSpawns;
    public bool randomSpawn;

    [Header("Return Rewards")]
    public List<ItemStack> rewards;
    public MenuSelectorUIHandler menuSelector;
    public MenuUIHandler returnItemsMenu;
    public ReturnItemsUIHandler returnItems;

    public int faintsLeft;

    private void Awake()
    {
        _gameState = gameState.GetReferencedComponent<GameState>();
        _gameState.sceneLoadedEvent += OnSceneLoaded;

        inventory = player.GetReferencedComponent<Inventory>();
    }

    public void OnPlayerFaint()
    {
        faintsLeft--;

        if (faintsLeft < 0)
        {
            ReturnFromHunt(ReturnState.Failure);
        }
    }

    private void OnDestroy()
    {
        _gameState.sceneLoadedEvent -= OnSceneLoaded;
    }

    public void BeginHunt()
    {   //set up hunt stuff - spawn monsters, etc.
        Debug.Log("Began hunt " + _gameState.currentQuestInfo.questName);
    }

    public void ReturnedFromHunt(ReturnState returnState)
    {
        Debug.Log("Returned from a hunt. Return State: " + returnState);

        inventory.GetComponent<RefiningInventory>().RefineMaterials();
        inventory.GetComponent<ResourcesManager>().ResetResources();
    }

    /// <summary>
    /// Start playing the return from hunt animation, set up reward stuff, etc.
    /// </summary>
    /// <param name="state">The return state. The state determines what kind of rewards you recieve; 
    /// Abandoning gives none, failing gives few, and Sucess gives the most.</param>
    public void ReturnFromHunt(ReturnState state)
    {
        GameState.loading = true;
        Debug.Log("Started returning to town. Waiting " + returnToTownTime + " seconds.");
        StartCoroutine(PlayReturnAnimation(state));
    }

    public void ReturnFromHuntMenuOption()
    {//TODO change this from success to abandon
        ReturnFromHunt(ReturnState.Success);
    }

    private IEnumerator PlayReturnAnimation(ReturnState returnState)
    {
        if (returnState == ReturnState.Success)
        {
            if (_gameState.currentQuestInfo)
            {
                rewards = _gameState.currentQuestInfo.GetRewards();
            }
        }

        yield return new WaitForSeconds(overrideReturnToTownTime ? overrideTime : returnToTownTime);

        menuSelector.OpenMenuSelector();
        menuSelector.ChangeToMenuFromSelector(returnItemsMenu);
        returnItems.SetReturnItems(rewards);
        yield return new WaitUntil(delegate () { return returnItems.Finished; });
        //_gameState.GoToTown("", returnState);
    }

    private void OnSceneLoaded(string previousScene, string sceneLoaded, ReturnState returnState)
    {
        if (worldType == GameState.WorldType.Hunt)
            BeginHunt();
        else if (worldType == GameState.WorldType.Town && returnState != ReturnState.NotReturning)
            ReturnedFromHunt(returnState);

        SetPlayerSpawn();
    }

    private void SetPlayerSpawn()
    {
        if (!playerPhysical.GetGameObject())
        {
            Debug.LogError("Tried to set the player's spawn, but SceneState does not have an attached playerPhysical.");
            return;
        }

        foreach (PlayerSpawn playerSpawn in playerSpawns)
        {
            if (playerSpawn.isDefaultSpawn)
            {
                playerPhysical.GetGameObject().transform.position = playerSpawn.transform.position;
            }
        }

        if (randomSpawn)
        {
            int spawnIndex = Random.Range(0, playerSpawns.Count);
            playerPhysical.GetGameObject().transform.position = playerSpawns[spawnIndex].transform.position;

            lastSpawnIndex = spawnIndex;
        }
    }
}
