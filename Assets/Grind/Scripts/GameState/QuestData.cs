﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "QuestData", menuName = "Grind/Quest Data")]
public class QuestData : ScriptableObject
{
    /// <summary>
    /// HuntAllMonsters: Hunt all monsters listed in <see cref="monsterData"/> to end the quest.
    /// HuntAnyMonster: Hunt any of the monsters listed in <see cref="monsterData"/>. (any not marked as invader)
    /// FindAllItems: Find all the items listed in <see cref="findItems"/>.
    /// FindAnyItem: Find any item listed in <see cref="findItems"/>.
    /// Timed: Survive a certain amount of time.
    /// </summary>
    public enum QuestType
    {
        HuntAllMonsters,
        HuntAnyMonster,
        FindAllItems,
        FindAnyItem,
        Timed
    }

    public string questName;
    [TextArea]
    public string questDescription;

    [Range(1, 10)]
    public int questDifficulty = 1;

    public QuestType questType;

    [Tooltip("If the quest is finished - e.x. we hunt all the monsters on a HuntAllMonsters quest - " +
        "we will automatically return to base, instead of requiring the player to manually return.")]
    public bool autoEnd;

    [Tooltip("If you die this many times, the player will return to base automatically.")]
    public int maxFaints = 3;

    [Tooltip("Items that are required to be found to finish the quest. If you have these in your " +
        "inventory when you return, the quest is sucessful.")]
    public List<ItemStack> findItems;

    [Tooltip("monsters that show up in the hunt. If the quest type is HuntAllMonsters, all of " +
        "these (that are not marked as invader) will need to be killed before returning.")]
    public List<MonsterData> monsterData;

    [Tooltip("The map on which the hunt is located.")]
    public string huntMap;

    [Tooltip("Important quests are quests that are given by NPCs that unlock something, such as giving" +
        " a special item, unlocking a new quest, or furthering an npc's substoryline.")]
    public bool isImportantQuest;
    [Tooltip("Story quests are quests that follow the main storyline of the game, that actively progress the game." +
        "These often unlock new areas and such.")]
    public bool isStoryQuest;

    public List<DropTable> rewards;

    public int rewardGold;

    public List<ItemStack> GetRewards()
    {
        List<ItemStack> droppedItems = new List<ItemStack>();

        foreach (DropTable drop in rewards)
        {
            ItemStack droppedItem = drop.GetDroppedItem();
            if (droppedItem.attachedItem && droppedItem.stackSize > 0)
                droppedItems.Add(droppedItem);
        }

        return droppedItems;
    }

    public static Color GetColorFromQuestDifficulty(int questDifficulty)
    {
        switch (questDifficulty)
        {
            case 1:
                {
                    return Color.white;
                }
            case 2:
                {
                    return new Color32(211, 221, 255, 255);
                }
            case 3:
                {
                    return new Color32(177, 196, 255, 255);
                }
            case 4:
                {
                    return new Color32(143, 170, 255, 255);
                }
            case 5:
                {
                    return new Color32(110, 143, 255, 255);
                }
            case 6:
                {
                    return new Color32(141, 111, 199, 255);
                }
            case 7:
                {
                    return new Color32(175, 79, 139, 255);
                }
            case 8:
                {
                    return new Color32(209, 45, 81, 255);
                }
            case 9:
                {
                    return new Color32(243, 12, 23, 255);
                }
            case 10:
                {
                    return Color.red;
                }
            default: return Color.white;
        }
    }
}
