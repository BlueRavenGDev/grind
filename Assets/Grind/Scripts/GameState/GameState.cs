﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using GameObjectHolders;
using System;
using System.Collections.Generic;
using System.IO;

/// <summary>
/// Game State class is always active, no matter what scene - loading scene, menu, town, or hunt.
/// It keeps track of, obviously, game state - what the current active scene is, what's being loaded, what you're required to do, etc.
/// </summary>
public class GameState : MonoBehaviour
{
    public enum WorldType
    {
        Town,
        Hunt
    }

    public enum WorldState
    {
        MainMenu,
        Town,
        Loading,
        Hunt
    }

    public WorldState worldState = WorldState.Hunt;

    public string fileName;

    public QuestData currentQuestInfo;

    public GameObjectReference sceneState;

    public GameObjectReference player;

    [Tooltip("In the case that the game tries to load a scene that does not exist, it will fall back to this scene. Probably should be default town scene once made?")]
    public string backupScene = "Town1";
    public string lastTownScene;
    public string currentScene;
    public string loadingScreenScene;

    public Action<string, string, ReturnState> sceneLoadedEvent;

    public static bool loading;

    private void Awake()
    {
        currentScene = gameObject.scene.name;

        DontDestroyOnLoad(gameObject);
    }

    #region Scene Management
    /// <summary>
    /// Goes from a scene to a town scene.
    /// </summary>
    /// <param name="lastTown">if true, the player returns to the town defined at <see cref="lastTownScene"/>.</param>
    public void GoToTown(string town, ReturnState returnState, bool lastTown = true)
    {
        if (lastTown)
        {
            StartLoad(lastTownScene, returnState);
        }
    }

    /// <summary>
    /// goes from a town to a hunt.
    /// </summary>
    public void GoToHunt()
    {
        if (sceneState.GetReferencedComponent<SceneState>().worldType == WorldType.Hunt)
            Debug.LogWarning("!!!Warning!!! GoToHunt was called while already on a hunt. This can work, but is an indicator that something is really messed up!");

        if (currentQuestInfo)
        {   //we have hunt data - so we can start a hunt
            StartLoad(currentQuestInfo.huntMap);
        }
        else
        {
            Debug.Log("Tried to leave to go on a hunt, but we have no quest info.");
        }
    }

    private void StartLoad(string scene, ReturnState returnState = ReturnState.NotReturning)
    {
        if (SceneExists(scene))
        {   //our scene exists
            StartCoroutine(Load(scene, returnState));
        }
        else
        {
            Debug.LogWarning("Could not find scene " + scene + ". Loading backup scene " + backupScene + ".");
            StartCoroutine(Load(backupScene));
        }
    }

    private bool SceneExists(string scene)
    {
        List<string> scenesInBuild = new List<string>();
        for (int i = 0; i < SceneManager.sceneCountInBuildSettings; i++)
        {
            string scenePath = SceneUtility.GetScenePathByBuildIndex(i);
            int lastSlash = scenePath.LastIndexOf("/");
            scenesInBuild.Add(scenePath.Substring(lastSlash + 1, scenePath.LastIndexOf(".") - lastSlash - 1));
        }

        return scenesInBuild.Contains(scene);
    }

    private IEnumerator Load(string scene, ReturnState returnState = ReturnState.NotReturning)
    {
        MenuSelectorUIHandler.ForceReturnFromMenuOrSelector();

        loading = true;
        //fade world out

        float startTime = Time.time;
        //load SMALL loading screen scene
        yield return SceneManager.LoadSceneAsync(loadingScreenScene, LoadSceneMode.Additive);

        //unload current scene
        yield return SceneManager.UnloadSceneAsync(currentScene);

        //load given scene
        yield return SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);

        //run scene loaded function - set various properties such as the current scene
        OnSceneLoaded(currentScene, scene, returnState);

        //unload loading screen scene
        yield return SceneManager.UnloadSceneAsync(loadingScreenScene);

        float endTime = Time.time;
        Debug.Log("Loading scene " + scene + " took: " + (endTime - startTime));

        //other scene setup stuff; creating items, etc. shouldn't take very long, but keep the screen black just in case

        //fade world in
        loading = false;
    }

    private void OnSceneLoaded(string previousScene, string sceneLoaded, ReturnState returnState)
    {
        currentScene = sceneLoaded;

        if (sceneLoadedEvent != null) sceneLoadedEvent.Invoke(previousScene, sceneLoaded, returnState);

        if (returnState != ReturnState.NotReturning)
        {   //reset our current quest
            currentQuestInfo = null;
        }
    }
    #endregion

    #region Save/Load
    public void SaveGameToFile()
    {
        List<SerializableItemInstance> items = player.GetReferencedComponent<Inventory>().ItemsToSerializableForm();
        SaveData saveData = new SaveData(fileName, items);

        if (!Directory.Exists(Application.persistentDataPath + "/SaveData"))
            Directory.CreateDirectory(Application.persistentDataPath + "/SaveData/");

        GrindUtils.Save(saveData, Application.persistentDataPath + "/SaveData/" + fileName);

        Debug.Log("Saved game to file " + fileName + " at " + Application.persistentDataPath);
    }

    public void LoadGameFromFile(string fileName)
    {
        if(!Directory.Exists(Application.persistentDataPath + "/SaveData"))
            Directory.CreateDirectory(Application.persistentDataPath + "/SaveData/");

        SaveData savedData = GrindUtils.Load(Application.persistentDataPath + "/SaveData/" + fileName);

        List<SerializableItemInstance> items = savedData.inventoryItems;
        fileName = savedData.saveName;

        player.GetReferencedComponent<Inventory>().AddSerializedItems(items);

        Debug.Log("Loaded game from file " + fileName + " at " + Application.persistentDataPath);
    }

    public void LoadGameFromFile()
    {
        LoadGameFromFile(fileName);
    }
    #endregion
}
