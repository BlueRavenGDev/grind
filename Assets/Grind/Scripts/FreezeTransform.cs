﻿using UnityEngine;
using System.Collections;

public class FreezeTransform : MonoBehaviour
{
    private Vector3 _position;
    private Vector3 _rotation;
    private Vector3 _scale;

    public bool calculateOffBasePosition;
    public Vector3 position;
    public Vector3 rotation;
    public Vector3 scale;

    public bool freezeLocally;

    private void Start()
    {
        if (calculateOffBasePosition)
        {
            if (freezeLocally)
            {
                _position = transform.localPosition;
                _rotation = transform.localEulerAngles;
                _scale = transform.localScale;
            }
            else
            {
                _position = transform.position;
                _rotation = transform.eulerAngles;
                _scale = transform.localScale;
            }
        }
        else
        {
            _position = position;
            _rotation = rotation;
            _scale = scale;
        }
    }

    private void FixedUpdate()
    {
        if (freezeLocally)
        {
            transform.localPosition = _position;
            transform.localEulerAngles = _rotation;
            transform.localScale = _scale;
        }
        else
        {
            transform.position = _position;
            transform.eulerAngles = _rotation;
            transform.localScale = _scale;
        }
    }
}
