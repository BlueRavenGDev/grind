﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization;
using System;
using System.Reflection;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class GrindUtils
{
    public const string extension = ".gsf";
    // === This is required to guarantee a fixed serialization assembly name, which Unity likes to randomize on each compile
    // Do not change this
    public sealed class VersionDeserializationBinder : SerializationBinder
    {
        public override Type BindToType(string assemblyName, string typeName)
        {
            if (!string.IsNullOrEmpty(assemblyName) && !string.IsNullOrEmpty(typeName))
            {
                Type typeToDeserialize = null;

                assemblyName = Assembly.GetExecutingAssembly().FullName;

                // The following line of code returns the type. 
                typeToDeserialize = Type.GetType(String.Format("{0}, {1}", typeName, assemblyName));

                return typeToDeserialize;
            }

            return null;
        }
    }

    public static void Save(SaveData saveFile, string filePath)
    {
        try
        {
            Stream stream = File.Open(filePath + extension, FileMode.Create);
            BinaryFormatter bformatter = new BinaryFormatter();
            bformatter.Binder = new VersionDeserializationBinder();
            bformatter.Serialize(stream, saveFile);
            stream.Close();
        }
        catch (UnauthorizedAccessException) //many more exception might happen, check documentation
        { }
    }

    public static SaveData Load(string filePath)
    {
        try
        {
            Stream stream = File.Open(filePath + extension, FileMode.Open);
            BinaryFormatter bformatter = new BinaryFormatter();
            bformatter.Binder = new VersionDeserializationBinder();
            SaveData fdata = (SaveData)bformatter.Deserialize(stream);
            stream.Close();

            return fdata;
        }
        catch { }

        return new SaveData();
    }
}
