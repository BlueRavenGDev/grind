﻿using UnityEngine;
using System.Collections;
using CustomInputManager;
using GameObjectHolders;
using UnityEngine.Events;

[RequireComponent(typeof(Collider2D))]
public class UseCollider : MonoBehaviour, IUseRequester
{
    public string inputName = "Use";

    public GameObjectReference usePromptVisual;
    public GameObjectReference player;

    public UnityEvent useEvent;

    [SerializeField]
    private int priority;

    public int Priority
    {
        get { return priority; }
        set { priority = value; }
    }

    public GameObjectReference UseHandler
    {
        get { return player; }
        set { player = value; }
    }

    private bool inside;

    private void Awake()
    {
        Collider2D col = GetComponent<Collider2D>();

        if (!col.isTrigger)
        {
            col.isTrigger = true;
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            usePromptVisual.GetReferencedComponent<UsePromptUIHandler>().StartPrompt(transform, inputName, Priority);
        }
    }

    private void Update()
    {
        if (inside && !GameState.loading)
            player.GetReferencedComponent<UseHandler>().RequestUse(this);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
            inside = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            usePromptVisual.GetReferencedComponent<UsePromptUIHandler>().StopPrompt();
            inside = false;
        }
    }

    public void Use()
    {
        useEvent.Invoke();
    }

    private void OnDestroy()
    {
        usePromptVisual.GetReferencedComponent<UsePromptUIHandler>().StopPrompt();
        inside = false;
    }

    private void OnDisable()
    {
        usePromptVisual.GetReferencedComponent<UsePromptUIHandler>().StopPrompt();
        inside = false;
    }
}
