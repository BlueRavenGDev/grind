﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using GameObjectHolders;
using System.Collections.Generic;

public class EquippedConsumablesUIHandler : MonoBehaviour
{
    public ItemVisualUIHandler left, center, right;
    public Text consuambleName;

    public GameObjectReference player;
    private Inventory inventory;

    public float fadeTime = 2;

    private bool initialized;

    private Coroutine fade;

    private void Awake()
    {
        SetUp();
    }

    public void SetUp()
    {
        if (!initialized)
        {
            inventory = player.GetReferencedComponent<Inventory>();
            inventory.consumableChangedEvent += CurrentConsumableIndexChanged;

            left.SetUpDefaults(null);
            right.SetUpDefaults(null);
            center.SetUpDefaults(null);

            left.gameObject.SetActive(false);
            right.gameObject.SetActive(false);

            CurrentConsumableIndexChanged();

            initialized = true;
        }
    }

    private void OnDestroy()
    {
        inventory.consumableChangedEvent -= CurrentConsumableIndexChanged;
    }

    public void CurrentConsumableIndexChanged()
    {
        if (inventory.consumables.Count < 0)
            return;

        left.gameObject.SetActive(true);
        right.gameObject.SetActive(true);
        consuambleName.gameObject.SetActive(true);

        left.transform.localScale = new Vector3(1, 1, 1);
        right.transform.localScale = new Vector3(1, 1, 1);

        consuambleName.color = new Color(consuambleName.color.r, consuambleName.color.g, consuambleName.color.b, 1);

        int centerIndex = inventory.currentConsumableIndex;
        int leftIndex = inventory.currentConsumableIndex - 1 < 0 ? 
            inventory.consumables.Count - 1 : 
            inventory.currentConsumableIndex - 1;

        int rightIndex = inventory.currentConsumableIndex + 1 > inventory.consumables.Count - 1 ?
            0 : 
            inventory.currentConsumableIndex + 1;

        List<ItemInstance> consumables = inventory.consumables;

        for (int i = 0; i < consumables.Count; i++)
        {
            ItemInstance item = inventory.consumables[i];

            if (item)
            {
                if (i == centerIndex)
                {
                    center.SetItemVisualItem(item.itemStack, inventory, null);
                    consuambleName.text = item.attachedItem.iname;
                }

                if (i == leftIndex)
                    left.SetItemVisualItem(item.itemStack, inventory, null);

                if (i == rightIndex)
                    right.SetItemVisualItem(item.itemStack, inventory, null);
            }
        }

        if (fade != null) StopCoroutine(fade);
        fade = StartCoroutine(WaitThenFadeSides());
    }

    private IEnumerator WaitThenFadeSides()
    {
        yield return new WaitForSeconds(fadeTime);

        float decreaseby = 0.1f;
        while (left.transform.localScale.x > 0)
        {
            left.transform.localScale = left.transform.localScale - new Vector3(decreaseby, decreaseby, decreaseby);
            right.transform.localScale = right.transform.localScale - new Vector3(decreaseby, decreaseby, decreaseby);

            consuambleName.color = new Color(consuambleName.color.r, consuambleName.color.g, consuambleName.color.b, consuambleName.color.a - decreaseby);
            yield return new WaitForEndOfFrame();
        }

        left.gameObject.SetActive(false);
        right.gameObject.SetActive(false);

        consuambleName.gameObject.SetActive(false);
    }
}
