﻿using GameObjectHolders;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceVisualUIHandler : MonoBehaviour
{
    public string resourceName;
    public GameObjectReference resourceReference;
    private Resource resource;

    public int maxWidth;

    private void Awake()
    {
        
    }

    private void Start()
    {
        resource = resourceReference.GetReferencedComponent<ResourcesManager>().GetResource(resourceName);
        resource.resourceUsedEvent.AddListener(UpdateWidth);
        resource.resourceRegenEvent.AddListener(UpdateWidth);

        UpdateWidth();
    }

    private void UpdateWidth()
    {
        RectTransform rTransform = GetComponent<RectTransform>();
        Vector2 size = rTransform.sizeDelta;

        size.x = maxWidth * resource.currentPercent;

        rTransform.sizeDelta = size;
    }
}
