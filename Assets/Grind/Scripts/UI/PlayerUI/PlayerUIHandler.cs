﻿using UnityEngine;
using System.Collections;

public class PlayerUIHandler : MonoBehaviour
{
    public GameObject uiHolder;

    private void Update()
    {
        if (MenuSelectorUIHandler.menuOpen)
        {
            uiHolder.gameObject.SetActive(false);
        }
        else uiHolder.gameObject.SetActive(true);
    }
}
