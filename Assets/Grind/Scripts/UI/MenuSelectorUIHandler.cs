﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using GameObjectHolders;
using System;

public class MenuSelectorUIHandler : MonoBehaviour
{
    public GameObject segments;

    public List<MenuUIHandler> menuParentObjects;
    public MenuUIHandler currentActiveMenu;

    public GameObjectReference gameState;

    [Header("Dynamic Menu"), Tooltip("When calling ChangeToMenuFromSelectorOffSetting, we will change to the menu at this index in dynamicMenus.")]
    public int dynamicMenuIndex;
    public List<MenuUIHandler> dynamicMenus;
    public DynamicMenuSetting dynamicMenuSetting;

    //public Func<int> decideDynamicMenuIndexEvent;

    public static bool menuOpen, subMenuOpen;
    public static MenuSelectorUIHandler menuThatIsOpen;
    public static MenuUIHandler subMenuThatIsOpen; //TODO this

    /// <summary>
    /// Change to a menu from one of the menu selector's selectors.
    /// </summary>
    /// <param name="toMenu">The menu to change to.</param>
    public void ChangeToMenuFromSelector(MenuUIHandler toMenu)
    {
        if (!menuParentObjects.Contains(toMenu))
        {
            Debug.LogWarning("Tried to change to a menu that is not in the list.");
            return;
        }

        segments.SetActive(false);

        toMenu.gameObject.SetActive(true);
        toMenu.ChangeToMenu(currentActiveMenu);

        currentActiveMenu = toMenu;

        subMenuOpen = true;
        subMenuThatIsOpen = toMenu;
    }

    public void ChangeToSelectorFromSelector(MenuSelectorUIHandler toMenuSelector)
    {
        ExitMenuSelector();

        toMenuSelector.OpenMenuSelector();
    }
    
    /// <summary>
    /// Change from one menu to another.
    /// </summary>
    /// <param name="toMenu">The menu to change to.</param>
    public void ChangeToMenuFromMenu(MenuUIHandler toMenu)
    {
        if (!menuParentObjects.Contains(toMenu))
        {
            Debug.LogWarning("Tried to change to a menu that is not in the list.");
            return;
        }

        segments.SetActive(false);

        toMenu.gameObject.SetActive(true);
        toMenu.ChangeToMenu(currentActiveMenu);
        currentActiveMenu.ChangeFromMenu(toMenu);
        currentActiveMenu.gameObject.SetActive(false);

        currentActiveMenu = toMenu;

        subMenuThatIsOpen = toMenu;
        subMenuOpen = true;
    }

    public void ChangeToMenuFromSelectorIfHasQuest(MenuUIHandler toMenu)
    {
        if (gameState.GetReferencedComponent<GameState>().currentQuestInfo)
        {
            ChangeToMenuFromSelector(toMenu);
        }
    }

    public void ChangeToDynamicMenuFromSelector()
    {
        //if (decideDynamicMenuIndexEvent != null) dynamicMenuIndex = decideDynamicMenuIndexEvent.Invoke();
        if (dynamicMenuSetting) dynamicMenuIndex = dynamicMenuSetting.DecideDynamicMenu();

        if (dynamicMenuIndex < dynamicMenus.Count)
        {
            ChangeToMenuFromSelector(dynamicMenus[dynamicMenuIndex]);
        }
    }

    /// <summary>
    /// Returns from a menu to the menu selector.
    /// </summary>
    /// <param name="fromMenu">The menu we're returning from.</param>
    public void ReturnFromMenu(MenuUIHandler fromMenu)
    {
        segments.SetActive(true);

        fromMenu.gameObject.SetActive(false);

        fromMenu.ReturnToMenu(this);

        currentActiveMenu = null;

        subMenuThatIsOpen = null;
        subMenuOpen = false;
    }

    public static void ForceReturnFromMenuOrSelector()
    {
        if (menuOpen)
        {
            if (subMenuOpen)
                subMenuThatIsOpen.ReturnToMenu(menuThatIsOpen);
            menuThatIsOpen.ExitMenuSelector();
        }
    }

    /// <summary>
    /// Exits from the menu selector completely.
    /// </summary>
    public void ExitMenuSelector()
    {
        if (currentActiveMenu)
        {   //we have an active menu. return from it before closing.
            ReturnFromMenu(currentActiveMenu);
        }

        segments.SetActive(false);

        menuOpen = false;
        menuThatIsOpen = null;

        subMenuOpen = false;
        subMenuThatIsOpen = null;
    }

    /// <summary>
    /// Opens from closed to open.
    /// </summary>
    public void OpenMenuSelector()
    {
        if (!menuOpen && !GameState.loading)
        {
            menuOpen = true;
            menuThatIsOpen = this;

            segments.SetActive(true);
        }
    }

    /// <summary>
    /// Toggles the state of the menu from open to closed or closed to open.
    /// </summary>
    public void ToggleMenuSelector()
    {
        if (menuOpen)
        {
            if (!subMenuOpen && AmIOpen())
            {   //If we are the one that is open, then we can close ourselves.
                //If we're not the menu that's open, don't do anything.
                ExitMenuSelector();
            }
        }
        else
        {
            OpenMenuSelector();
        }
    }

    public bool AmIOpen()
    {
        return menuOpen && menuThatIsOpen == this;
    }
}
