﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PagedUI : MonoBehaviour
{
    public int maxPages;
    protected int currentPage;  //it should be noted that current page is zero indexed, whereas maxPages is not.

    public Text legendPage;

    public MenuUIHandler menuUI;

    protected virtual void Awake()
    {
        menuUI.changeToMenuEvent += x => { ChangePage(0); };
        menuUI.changeToMenuEvent += MenuOpened;
        menuUI.changeFromMenuEvent += MenuClosed;
        menuUI.changeFromMenuToSelectorEvent += MenuClosed;
    }

    protected virtual void Start()
    {
        ChangePage(0);
    }

    public virtual void ChangePage(int toPage)
    {
        if (toPage >= 0 && toPage < maxPages)
        {
            currentPage = toPage;

            if (legendPage) legendPage.text = (currentPage + 1) + "/" + maxPages;
        }
    }

    public void ChangePageLeft()
    {
        ChangePage(currentPage - 1);
    }

    public void ChangePageRight()
    {
        ChangePage(currentPage + 1);
    }

    public virtual void MenuOpened(MenuUIHandler oldMenu)
    {

    }

    public virtual void MenuClosed(MenuUIHandler newMenu)
    {

    }

    private void MenuClosed()
    {
        MenuClosed(null);
    }
}
