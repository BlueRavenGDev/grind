﻿using UnityEngine;
using System.Collections;
using GameObjectHolders;

public class DynamicMenuSettingRefine : DynamicMenuSetting
{
    public GameObjectReference player;
    private RefiningInventory inventory;

    private void Start()
    {
        inventory = player.GetReferencedComponent<RefiningInventory>();
    }

    public override int DecideDynamicMenu()
    {
        if (inventory.materialsRefined)
        {
            return 1;
        }

        return 0;
    }
}
