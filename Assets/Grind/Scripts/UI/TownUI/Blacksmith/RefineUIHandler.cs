﻿using GameObjectHolders;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RefineUIHandler : MonoBehaviour
{
    public GameObjectReference itemBox;
    private Inventory ibInventory;

    public GameObjectReference player;
    private RefiningInventory inventory;

    public GameObjectReference sceneState;
    private SceneState _sceneState;

    private ElementPopulatorUIHandler elementPopulator;

    public ItemInfoUIHandler itemInfo;

    public MenuUIHandler menuUI;

    public Button claimButton;

    private bool initialized;

    private void Awake()
    {
    }

    private void Start()
    {
        SetUp();
    }

    public void SetUp()
    {
        if (!initialized)
        {
            _sceneState = sceneState.GetReferencedComponent<SceneState>();

            ibInventory = itemBox.GetReferencedComponent<InventoryManager>().GetInventory("Items");
            inventory = player.GetReferencedComponent<InventoryManager>().GetInventory("Refining").GetComponent<RefiningInventory>();

            elementPopulator = GetComponent<ElementPopulatorUIHandler>();

            elementPopulator.elementCreatedEvent += x => { x.GetComponent<ItemVisualUIHandler>().SetUpDefaults(itemInfo); };

            elementPopulator.SetUpElements();

            menuUI.changeToMenuEvent += x => { ShowRefinables(); };

            initialized = true;
        }
        ShowRefinables();
    }

    public void ShowRefinables()
    {
        if (inventory.materialsRefined)
            claimButton.gameObject.SetActive(true);
        else claimButton.gameObject.SetActive(false);

        List<ItemStack> refiningMaterials = inventory.refiningMaterials;

        for (int i = 0; i < refiningMaterials.Count; i++)
        {
            if (i < elementPopulator.elements.Count)
            {
                ItemStack stack = refiningMaterials[i];

                elementPopulator.elements[i].GetComponent<ItemVisualUIHandler>().SetItemVisualItem(stack, inventory, null, true);
            }
        }
    }

    public void ClaimAllMaterials()
    {
        inventory.ClaimRefinedMaterials(ibInventory);
    }
}
