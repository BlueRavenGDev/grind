﻿using UnityEngine;
using System.Collections;

public class DynamicMenuSettingCraftWeapon : DynamicMenuSetting
{
    public UpgradeListUIHandler recipeMenu;
    public MenuSelectorUIHandler menuUI;

    [ReadOnly]
    public RecipeTypeList recipeList;

    public override int DecideDynamicMenu()
    {
        recipeMenu.SetUp();
        recipeMenu.ShowUpgrades(recipeList);
        return 0;
    }

    public void SetRecipeAndUseDynamicMenuOption(RecipeTypeList recipeTypeList)
    {
        this.recipeList = recipeTypeList;
        menuUI.ChangeToDynamicMenuFromSelector();
    }
}
