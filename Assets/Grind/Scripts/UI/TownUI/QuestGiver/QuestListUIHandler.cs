﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class QuestListUIHandler : PagedUI
{
    public MenuSelectorUIHandler menuSelector;
    public QuestInfoUIHandler questInfoUI;

    private ElementPopulatorUIHandler elementPopulator;

    public List<QuestData> questList;

    protected override void Awake()
    {
        elementPopulator = GetComponent<ElementPopulatorUIHandler>();
        elementPopulator.elementCreatedEvent += x => { x.GetComponent<QuestSlotUIHandler>().SetUp(this); };
        elementPopulator.SetUpElements();

        maxPages = (questList.Count / elementPopulator.rows) + 1;

        base.Awake();
    }

    protected override void Start()
    {
        base.Start();
    }

    public override void ChangePage(int toPage)
    {
        base.ChangePage(toPage);

        foreach (GameObject element in elementPopulator.elements)
        {
            QuestSlotUIHandler questSlot = element.GetComponent<QuestSlotUIHandler>();
            questSlot.ResetSlot();
        }

        for (int i = 0; i < elementPopulator.elements.Count; i++)
        {
            QuestSlotUIHandler questSlot = elementPopulator.elements[i].GetComponent<QuestSlotUIHandler>();
            int index = (currentPage * elementPopulator.elements.Count) + i;

            if (index < questList.Count)
                questSlot.SetQuestSlotInfo(questList[index]);
        }
    }

    public void ClickQuest(QuestData questInfo)
    {
        questInfoUI.SetQuestInfo(questInfo);
        menuSelector.ChangeToMenuFromMenu(questInfoUI.GetComponent<MenuUIHandler>());
    }
}
