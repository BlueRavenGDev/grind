﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using GameObjectHolders;

public class QuestInfoUIHandler : MonoBehaviour
{   //TODO quest has been done indicator
    public Text questName;
    public Text questDescription;
    public Text questImportant;

    public Text questType;
    public Image questTypeIcon;

    public Text faintsAllowedCount;

    public Text reward;

    public Sprite questTypeHunt, questTypeItem, questTypeTimed;

    public GameObjectReference gameState;

    [HideInInspector]
    public QuestData currentQuestInfo;

    public void SetUp()
    {
        ResetInfo();
    }

    public void SetQuestInfo(QuestData questInfo)
    {
        if (questInfo)
        {
            currentQuestInfo = questInfo;

            questName.gameObject.SetActive(true);
            questDescription.gameObject.SetActive(true);

            if (questInfo.isImportantQuest || questInfo.isStoryQuest)
                questImportant.gameObject.SetActive(true);

            questType.gameObject.SetActive(true);
            questTypeIcon.gameObject.SetActive(true);

            faintsAllowedCount.gameObject.SetActive(true);

            reward.gameObject.SetActive(true);

            questName.text = questInfo.questName;
            questDescription.text = questInfo.questDescription;

            if (questInfo.isStoryQuest)
                questImportant.color = Color.red;
            else questImportant.color = Color.white;

            questType.text = GetQuestTypeText(questInfo.questType);

            if (questInfo.questType == QuestData.QuestType.HuntAllMonsters || questInfo.questType == QuestData.QuestType.HuntAnyMonster)
            {
                questTypeIcon.sprite = questTypeHunt;
            }
            else if (questInfo.questType == QuestData.QuestType.FindAllItems || questInfo.questType == QuestData.QuestType.FindAnyItem)
            {
                questTypeIcon.sprite = questTypeItem;
            }
            else if (questInfo.questType == QuestData.QuestType.Timed)
            {
                questTypeIcon.sprite = questTypeTimed;
            }

            faintsAllowedCount.text = questInfo.maxFaints.ToString();
            reward.text = questInfo.rewardGold + " G";
        }
    }

    private string GetQuestTypeText(QuestData.QuestType type)
    {
        switch (type)
        {
            case QuestData.QuestType.HuntAllMonsters:
                return "Hunt All Monsters";
            case QuestData.QuestType.HuntAnyMonster:
                return "Hunt Any Monsters";
            case QuestData.QuestType.FindAllItems:
                return "Find All Items";
            case QuestData.QuestType.FindAnyItem:
                return "Find Any Item";
            case QuestData.QuestType.Timed:
                return "Timed";
            default:
                return "ERROR";
        }
    }

    public void ResetInfo()
    {
        questName.gameObject.SetActive(false);
        questDescription.gameObject.SetActive(false);
        questImportant.gameObject.SetActive(false);

        questType.gameObject.SetActive(false);
        questTypeIcon.gameObject.SetActive(false);

        faintsAllowedCount.gameObject.SetActive(false);

        reward.gameObject.SetActive(false);
    }

    public void Confirm()
    {
        gameState.GetReferencedComponent<GameState>().currentQuestInfo = currentQuestInfo;
    }

    public void OpenFromMenu()
    {
        SetUp();
        SetQuestInfo(gameState.GetReferencedComponent<GameState>().currentQuestInfo);
    }
}
