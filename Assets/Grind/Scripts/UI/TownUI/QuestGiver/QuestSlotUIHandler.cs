﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class QuestSlotUIHandler : MonoBehaviour
{   //TODO quest has been done indicator
    public Image star;
    public Text difficulty;
    public Image questType;
    public Text important;

    public Text questName;

    public Sprite huntType, itemType, timedType;

    private QuestListUIHandler questList;
    private QuestData quest;

    public void SetUp(QuestListUIHandler questList)
    {
        this.questList = questList;
        ResetSlot();
    }

    public void SetQuestSlotInfo(QuestData quest)
    {
        this.quest = quest;

        star.gameObject.SetActive(true);
        difficulty.gameObject.SetActive(true);
        questType.gameObject.SetActive(true);

        questName.gameObject.SetActive(true);

        star.color = QuestData.GetColorFromQuestDifficulty(quest.questDifficulty);
        difficulty.text = quest.questDifficulty.ToString();

        questName.text = quest.questName;

        if (quest.questType == QuestData.QuestType.HuntAllMonsters || quest.questType == QuestData.QuestType.HuntAnyMonster)
        {
            questType.sprite = huntType;
        }
        else if (quest.questType == QuestData.QuestType.FindAllItems || quest.questType == QuestData.QuestType.FindAnyItem)
        {
            questType.sprite = itemType;
        }
        else if (quest.questType == QuestData.QuestType.Timed)
        {
            questType.sprite = timedType;
        }

        if (quest.isImportantQuest || quest.isStoryQuest)
        {
            important.gameObject.SetActive(true);
        }
        else important.gameObject.SetActive(false);

        if (quest.isStoryQuest)
            important.color = Color.red;
        else important.color = Color.white;
    }

    public void ResetSlot()
    {
        quest = null;
        star.gameObject.SetActive(false);
        difficulty.gameObject.SetActive(false);
        questType.gameObject.SetActive(false);
        important.gameObject.SetActive(false);

        questName.gameObject.SetActive(false);
    }

    public void ClickQuest()
    {
        if (quest)
            questList.ClickQuest(quest);
    }
}
