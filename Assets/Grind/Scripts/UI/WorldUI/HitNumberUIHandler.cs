﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HitNumberUIHandler : MonoBehaviour
{
    public Text text;

    public float moveUp = 0.25f;
    public float duration;

    public float fadeOutStart = 1;

    public float fadeOutTime = 1;

    private float startTime;

    public Color friendlyHitType = Color.green;
    public Color hostileHitType = Color.red;

    public AnimationCurve scaleOverTime;

    public void SetDamage(int damage, DamageHitType hitType)
    {
        text.text = damage.ToString();
        text.color = GetColorOffHitType(hitType);
        startTime = Time.time;
    }

    private Color GetColorOffHitType(DamageHitType hitType)
    {
        switch (hitType)
        {
            case DamageHitType.Friendly:
                return friendlyHitType;
            case DamageHitType.Hostile:
                return hostileHitType;
            default:
                return Color.black;
        }
    }

    private void Update()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y + moveUp, transform.position.z);

        if (Time.time > startTime + fadeOutStart)
        {
            float currentTime = Time.time - (startTime + fadeOutStart);

            float percent = currentTime / fadeOutTime;
            float inversePercent = Mathf.Abs(percent - 1);

            text.color = new Color(text.color.r, text.color.g, text.color.b, inversePercent);
            text.transform.localScale = new Vector3(scaleOverTime.Evaluate(percent), scaleOverTime.Evaluate(percent), 1);
        }

        if (Time.time > startTime + duration)
        {
            Destroy(gameObject);
        }
    }
}
