﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using CustomInputManager;
using System.Collections.Generic;
using System;

public class UsePromptUIHandler : MonoBehaviour
{
    public Text text;
    public Image background;

    public Inputs input;

    public Vector2 offset = new Vector2(0, -128);

    private bool open;

    int currentPriority;

    private void Awake()
    {
    }

    public void StartPrompt(Transform transform, string inputName, int priority)
    {
        if (priority > currentPriority)
            currentPriority = priority;
        else return;

        if (!open && input.currentInputs.ContainsKey(inputName))
        {
            background.gameObject.SetActive(true);
            text.gameObject.SetActive(true);
            text.text = input.currentInputs[inputName].ToString();

            this.transform.position = transform.position + (Vector3)offset;

            open = true;
        }
    }

    public void StopPrompt()
    {
        open = false;

        background.gameObject.SetActive(false);
        text.gameObject.SetActive(false);

        currentPriority = 0;
    }
}
