﻿using UnityEngine;
using System.Collections;
using System;

public class MenuUIHandler : MonoBehaviour
{
    public Action<MenuUIHandler> changeToMenuEvent;     //called when we change to this menu. 0 is the menu we're changing from.
    public Action<MenuUIHandler> changeFromMenuEvent;   //called when we change from this menu to another. 0 is the menu we're changing to.
    public Action changeFromMenuToSelectorEvent;        //called when we change from this menu back to the menu selector.

    public void ChangeToMenu(MenuUIHandler fromMenu)
    {
        if (changeToMenuEvent != null) changeToMenuEvent.Invoke(fromMenu);
    }

    public void ChangeFromMenu(MenuUIHandler toMenu)
    {
        if (changeFromMenuEvent != null) changeFromMenuEvent.Invoke(toMenu);
    }

    public void ReturnToMenu(MenuSelectorUIHandler mainMenu)
    {
        if (changeFromMenuToSelectorEvent != null) changeFromMenuToSelectorEvent.Invoke();
    }
}
