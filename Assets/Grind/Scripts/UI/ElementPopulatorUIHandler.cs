﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ElementPopulatorUIHandler : MonoBehaviour
{
    public enum ElementPopulationType
    {
        Columns,
        Rows,
        ColumnsAndRows
    }

    public ElementPopulationType type;

    public int rows = 7;
    public int columns = 0;

    public int marginX = -5;
    public int marginY = -5;

    public float startXOffset;
    public float startYOffset;

    public List<GameObject> elements;

    [SerializeField]
    private GameObject elementPrefab;

    public Action<GameObject> elementCreatedEvent;          //run when a new element is instantiated. 0 is the created element.
    public Action<List<GameObject>> elementsCreatedEvent;   //run when all elements have been instantiated. 0 is the list of all created elements.

    [Tooltip("Automatically set up when Start for this gameobject is called.")]
    public bool autoSetup;

    private void Start()
    {
        if (autoSetup)
            SetUpElements();    
    }

    public void SetUpElements()
    {
        if (type == ElementPopulationType.ColumnsAndRows)
        {
            for (int y = 0; y < rows; y++)
            {
                for (int x = 0; x < columns; x++)
                {
                    GameObject element = Instantiate(elementPrefab);
                    element.gameObject.SetActive(true);
                    elements.Add(element);

                    RectTransform transform = element.GetComponent<RectTransform>();

                    Vector2 pos = transform.localPosition;

                    pos.x = startXOffset + (transform.sizeDelta.x * x) + (marginX * x);
                    pos.y = Screen.height + (startYOffset - ((transform.sizeDelta.y * y) + (marginY * y)));

                    transform.localPosition = pos;

                    element.transform.SetParent(gameObject.transform);

                    if (elementCreatedEvent != null) elementCreatedEvent.Invoke(element);
                }
            }
        }
        else if (type == ElementPopulationType.Rows)
        {
            for (int i = 0; i < rows; i++)
            {
                GameObject element = Instantiate(elementPrefab);
                element.gameObject.SetActive(true);
                elements.Add(element);

                RectTransform transform = element.GetComponent<RectTransform>();

                Vector2 pos = transform.localPosition;

                pos.x = startXOffset;
                pos.y = Screen.height + (startYOffset - ((transform.sizeDelta.y * i) + (marginY * i)));

                transform.localPosition = pos;

                element.transform.SetParent(gameObject.transform);

                if (elementCreatedEvent != null) elementCreatedEvent.Invoke(element);
            }
        }
        else if (type == ElementPopulationType.Columns)
        {
            for (int i = 0; i < columns; i++)
            {
                GameObject element = Instantiate(elementPrefab);
                element.gameObject.SetActive(true);
                elements.Add(element);

                RectTransform transform = element.GetComponent<RectTransform>();

                Vector2 pos = transform.localPosition;

                pos.x = startXOffset + (transform.sizeDelta.x * i) + (marginX * i);
                pos.y = Screen.height + startYOffset;

                transform.localPosition = pos;

                element.transform.SetParent(gameObject.transform);

                if (elementCreatedEvent != null) elementCreatedEvent.Invoke(element);
            }
        }

        if (elementsCreatedEvent != null) elementsCreatedEvent.Invoke(elements);
    }
}
