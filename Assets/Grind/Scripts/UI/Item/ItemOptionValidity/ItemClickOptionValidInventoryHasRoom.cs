﻿using UnityEngine;
using System.Collections;
using GameObjectHolders;

public class ItemClickOptionValidInventoryHasRoom : ItemClickOptionValid
{
    public GameObjectReference inventoryReference;

    public override bool OptionIsValid(ItemInstance item)
    {
        if (inventoryReference.GetGameObject())
        {
            Inventory inventory = inventoryReference.GetReferencedComponent<Inventory>();

            if (inventory.HasSpace(item.itemStack) && !inventory.CheckListsDisallow(item.attachedItem))
            {
                return true;
            }
            return false;
        }

        return false;
    }
}
