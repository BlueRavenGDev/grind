﻿using UnityEngine;
using System.Collections;

public class ItemClickOptionValidRefinable : ItemClickOptionValid
{
    public override bool OptionIsValid(ItemInstance item)
    {
        if (item.attachedItem is ItemRefinable)
            return true;
        return false;
    }
}
