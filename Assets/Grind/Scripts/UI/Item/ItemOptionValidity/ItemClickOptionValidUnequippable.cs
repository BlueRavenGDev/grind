﻿using UnityEngine;
using System.Collections;

public class ItemClickOptionValidUnequippable : ItemClickOptionValid
{
    public override bool OptionIsValid(ItemInstance item)
    {
        return item.IsEquippable && item.Equipped;
    }
}
