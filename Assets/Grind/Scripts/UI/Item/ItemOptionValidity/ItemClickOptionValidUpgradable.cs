﻿using UnityEngine;
using System.Collections;

public class ItemClickOptionValidUpgradable : ItemClickOptionValid
{
    public override bool OptionIsValid(ItemInstance item)
    {
        return item.IsUpgradable;
    }
}
