﻿using UnityEngine;
using System.Collections;

public class ItemClickOptionValidSellable : ItemClickOptionValid
{
    public override bool OptionIsValid(ItemInstance item)
    {
        return item.attachedItem.canSell;
    }
}
