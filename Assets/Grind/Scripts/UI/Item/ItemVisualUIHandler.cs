﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class ItemVisualUIHandler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Image icon;
    public Text stackCount;

    [HideInInspector]
    public ItemStack itemStack;

    private ItemInfoUIHandler infoPanel;

    private int countInInventory = 0;
    private Inventory inventory;
    private Equipment equipment;

    public bool disableCount;

    public void SetUpDefaults(ItemInfoUIHandler infoPanel)
    {
        this.infoPanel = infoPanel;

        icon.gameObject.SetActive(false);

        if (!disableCount)
            stackCount.gameObject.SetActive(false);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="itemStack"></param>
    /// <param name="inventory"></param>
    /// <param name="equipment">Can be null.</param>
    /// <param name="useStackSizeInsteadofInventoryCount"></param>
    public void SetItemVisualItem(ItemStack itemStack, Inventory inventory, Equipment equipment, bool useStackSizeInsteadofInventoryCount = false)
    {
        if (itemStack.attachedItem)
        {
            this.itemStack = itemStack;

            icon.gameObject.SetActive(true);

            if (!disableCount)
                stackCount.gameObject.SetActive(true);
            else if (stackCount) stackCount.gameObject.SetActive(false);

            icon.sprite = itemStack.attachedItem.icon;
            if (itemStack.attachedItem.colorOffRarity)
                icon.color = Item.GetRarityColor(itemStack.attachedItem.rarity);
            else icon.color = itemStack.attachedItem.spriteColor;

            int count = inventory.FindItemCount(itemStack.attachedItem);

            if (useStackSizeInsteadofInventoryCount)
                count = itemStack.stackSize;

            if (!disableCount)
            {
                countInInventory = count;

                if (count > 0)
                {
                    stackCount.text = count.ToString();
                    stackCount.color = Color.white;
                }
                else
                {
                    stackCount.text = count.ToString();
                    stackCount.color = Color.red;
                }
            }

            this.inventory = inventory;
            this.equipment = equipment;
        }
        else
        {
            SlotReset();
        }
    }

    /// <summary>
    /// Sets the item visual using only the given itemstack.
    /// </summary>
    /// <param name="itemstack"></param>
    public void SetItemVisualItem(ItemStack itemStack)
    {
        if (itemStack.attachedItem)
        {
            this.itemStack = itemStack;

            icon.gameObject.SetActive(true);

            if (!disableCount)
                stackCount.gameObject.SetActive(true);
            else if (stackCount) stackCount.gameObject.SetActive(false);

            icon.sprite = itemStack.attachedItem.icon;
            if (itemStack.attachedItem.colorOffRarity)
                icon.color = Item.GetRarityColor(itemStack.attachedItem.rarity);
            else icon.color = itemStack.attachedItem.spriteColor;
            
            int count = itemStack.stackSize;

            if (!disableCount)
            {
                countInInventory = count;

                if (count > 0)
                {
                    stackCount.text = count.ToString();
                    stackCount.color = Color.white;
                }
                else
                {
                    stackCount.text = count.ToString();
                    stackCount.color = Color.red;
                }
            }
        }
        else
        {
            SlotReset();
        }
    }

    private void UpdateSlot()
    {

    }

    public void SlotReset()
    {
        icon.gameObject.SetActive(false);

        if (!disableCount)
            stackCount.gameObject.SetActive(false);

        countInInventory = 0;

        this.itemStack = new ItemStack();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (itemStack.attachedItem && infoPanel)
        {
            infoPanel.StartHover(itemStack.attachedItem, inventory, equipment, transform.position);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (itemStack.attachedItem && infoPanel)
        {
            infoPanel.ResetInfo();
        }
    }
}
