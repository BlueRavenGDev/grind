﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameObjectHolders;

public class ReturnItemsUIHandler : MonoBehaviour
{
    public enum SendToInventory
    {
        ItemBox,
        Player
    }

    public ItemInfoUIHandler itemInfo;
    private ElementPopulatorUIHandler elementPopulator;

    public SendToInventory sendToInventory;

    public GameObjectReference itemBox;
    private Inventory itemBoxInventory;
    public GameObjectReference player;
    private Inventory playerInventory;

    private bool initialized;

    public bool Finished { get; private set; }
    public void SetUp()
    {
        if (!initialized)
        {
            if (itemBox.GetGameObject())
            {
                itemBoxInventory = itemBox.GetReferencedComponent<InventoryManager>().GetInventory("Items");
            }
            if (player.GetGameObject())
            {
                playerInventory = player.GetReferencedComponent<InventoryManager>().GetInventory("Items");
            }

            elementPopulator = GetComponent<ElementPopulatorUIHandler>();
            elementPopulator.elementCreatedEvent += ElementCreated;
            elementPopulator.SetUpElements();

            initialized = true;
        }
    }

    public void SetReturnItems(List<ItemStack> items)
    {
        for (int i = 0; i < elementPopulator.elements.Count; i++)
        {
            ItemVisualUIHandler item = elementPopulator.elements[i].GetComponent<ItemVisualUIHandler>();

            if (i < items.Count - 1)
            {
                item.SetItemVisualItem(items[i]);
            }
            else return;
        }
    }

    public void TakeAllItemsOption(SendToInventory sendToInventory)
    {

    }

    public void TakeItem(int index)
    {

    }

    public void SellAllItems()
    {

    }

    public void CheckAllRemoved()
    {

    }

    private void ElementCreated(GameObject element)
    {
        element.GetComponent<ItemVisualUIHandler>().SetUpDefaults(itemInfo);
    }
}
