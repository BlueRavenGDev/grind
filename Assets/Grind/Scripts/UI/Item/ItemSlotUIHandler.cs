﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

using System.Collections;

public class ItemSlotUIHandler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public ItemInstance itemInstanceReference;

    public Image icon;
    public Text stackSize;
    public Text equipped;

    [Tooltip("The index of this itemslot. Correlates to the player's inventory item indexes.")]
    public int index;

    public InventoryUIHandler inventory;
    private ItemInfoUIHandler infoPanel;

    private bool initialized;

    private int count;

    private void OnDestroy()
    {
        inventory.inventory.itemUpdatedEvent -= ItemUpdated;
        count--;
    }

    private void OnDisable()
    {
        inventory.inventory.itemUpdatedEvent -= ItemUpdated;
        count--;
    }

    private void OnEnable()
    {
        if (initialized)
        {
            inventory.inventory.itemUpdatedEvent += ItemUpdated;
            count++;

            Debug.Assert(count <= 1);
        }
    }

    public void SetUpDefaults(InventoryUIHandler inventory, ItemInfoUIHandler infoPanel)
    {
        if (!initialized)
        {
            this.inventory = inventory;
            this.infoPanel = infoPanel;

            icon.gameObject.SetActive(false);
            stackSize.gameObject.SetActive(false);

            inventory.inventory.itemUpdatedEvent += ItemUpdated;
            count++;

            initialized = true;

            Debug.Assert(count <= 1);
        }
    }

    /// <summary>
    /// Upadate the itemslot when the page changes.
    /// </summary>
    /// <param name="index">The new index, equal to page * pagesize + itemslot index</param>
    public void PageChanged(int index)
    {
        if (index != -1)
            this.index = index;
        ItemUpdated(index);
    }

    public void SlotReset()
    {
        icon.gameObject.SetActive(false);
        stackSize.gameObject.SetActive(false);
        equipped.gameObject.SetActive(false);

        itemInstanceReference = null;
    }

    private void ItemUpdated(int itemIndex)
    {
        if (initialized && itemIndex == index)
        {   //the item index is the same as ours - the item slot at our index has updated
            ItemInstance item = inventory.inventory.Items[itemIndex];

            if (item)
            {
                itemInstanceReference = item;

                if (item.attachedItem.icon)
                {   //our item has an icon, so show it.
                    icon.gameObject.SetActive(true);
                    if (item.attachedItem.colorOffRarity)
                        icon.color = Item.GetRarityColor(item.attachedItem.rarity);
                    else icon.color = item.attachedItem.spriteColor;
                    icon.sprite = item.attachedItem.icon;
                }
                else icon.gameObject.SetActive(false);

                if (item.stackSize > 0)
                {   //we have more than zero items, so show the stack size.
                    stackSize.gameObject.SetActive(true);
                    stackSize.text = item.stackSize.ToString();

                    if (item.stackSize == item.attachedItem.maxStack)
                    {   //we're max stack - set the color to red
                        stackSize.color = Color.red;
                    }
                    else stackSize.color = Color.white;
                }
                else stackSize.gameObject.SetActive(false);

                if (item.IsEquippable && item.Equipped)
                {   //this is a piece of armor, and it is equipped.
                    equipped.gameObject.SetActive(true);
                }
                else equipped.gameObject.SetActive(false);
            }
            else SlotReset();
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (itemInstanceReference)
        {
            infoPanel.StartHover(itemInstanceReference.attachedItem, inventory.inventory, inventory.equipment, transform.position);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (itemInstanceReference)
        {
            infoPanel.ResetInfo();
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        inventory.ClickItem(index);
    }
}
