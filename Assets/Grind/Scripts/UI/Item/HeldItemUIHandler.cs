﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HeldItemUIHandler : MonoBehaviour
{
    public GameObject background;
    public Image icon;
    public Text stackSize;
    public Text equipped;

    private ItemInstance itemInstance;

    private bool heldItem;
    public Vector2 offset = new Vector2(25, -25);

    public void SetUp()
    {
        SlotReset();
    }

    public void StartHoldItem(ItemInstance item)
    {
        if (item)
        {
            heldItem = true;

            background.gameObject.SetActive(true);

            itemInstance = item;

            if (item.attachedItem.icon)
            {   //our item has an icon, so show it.
                icon.gameObject.SetActive(true);
                if (item.attachedItem.colorOffRarity)
                    icon.color = Item.GetRarityColor(item.attachedItem.rarity);
                else icon.color = item.attachedItem.spriteColor;
                icon.sprite = item.attachedItem.icon;
            }
            else icon.gameObject.SetActive(false);

            if (item.stackSize > 0)
            {   //we have more than zero items, so show the stack size.
                stackSize.gameObject.SetActive(true);
                stackSize.text = item.stackSize.ToString();

                if (item.stackSize == item.attachedItem.maxStack)
                {   //we're max stack - set the color to red
                    stackSize.color = Color.red;
                }
                else stackSize.color = Color.white;
            }
            else stackSize.gameObject.SetActive(false);

            if (item.IsArmor && item.Equipped)
            {   //this is a piece of armor, and it is equipped.
                equipped.gameObject.SetActive(true);
            }
            else equipped.gameObject.SetActive(false);
        }
        else SlotReset();
    }

    private void Update()
    {
        if (heldItem)
        {
            transform.position = (Vector3)offset + Input.mousePosition;
        }
    }

    public void SlotReset()
    {
        heldItem = false;

        background.gameObject.SetActive(false);
        icon.gameObject.SetActive(false);
        stackSize.gameObject.SetActive(false);
        equipped.gameObject.SetActive(false);

        itemInstance = null;
    }
}
