﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class DropAmtUIHandler : MonoBehaviour, IPointerEnterHandler
{
    public Button dropButton;

    public InputField input;

    public ItemClickOptionsUIHandler itemOptions;
    public ItemClickAction clickAction;

    private void Start()
    {
        input.onValueChanged.AddListener(CheckInput);
    }

    public void ResetUI()
    {
        input.text = "0";
        input.gameObject.SetActive(false);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        input.gameObject.SetActive(true);
    }

    public void CheckInput(string inputText)
    {
        if (itemOptions && itemOptions.item)
        {
            int count = 0;
            int.TryParse(inputText, out count);

            if (count > itemOptions.item.stackSize)
            {
                count = itemOptions.item.stackSize;
            }

            if (count < 0)
            {
                count = 0;
            }

            input.text = count.ToString();
        }
    }

    public void FinishInput()
    {
        if (input.text != "")
        {//itemOptions.DropOption(int.Parse(input.text), this);
            if (clickAction is ItemClickActionDrop)
                ((ItemClickActionDrop)clickAction).amt = int.Parse(input.text);

            if (clickAction is ItemClickActionSell)
                ((ItemClickActionSell)clickAction).amt = int.Parse(input.text);

            itemOptions.ClickAction(dropButton);
        }
    }
}
