﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using GameObjectHolders;

public class InventoryUIHandler : MonoBehaviour
{
    public int itemCount;   //how many items per page to load

    public float startXOffset = 10, startYOffset = 10;
    public float marginX = 10, marginY = 10;        //distance between each item slot
    public int columns = 10;
    public int rows = 4;

    public GameObject itemslotUIPrefab;
    private List<ItemSlotUIHandler> itemslots;

    [Tooltip("How many itemslots can be fit on one page.")]
    public const int pageSize = 72;

    public GameObjectReference itemBox;
    [HideInInspector]
    public Inventory itemBoxInventory;
    [HideInInspector]
    public EquipmentInventory itemBoxEquipmentInventory;

    public GameObjectReference player;
    [ReadOnly]
    public Inventory inventory;
    [HideInInspector]
    public Equipment equipment;
    [HideInInspector]
    public Gold gold;

    public bool useItemBoxInventory;
    public bool useItemBoxEquipmentInventory;

    public ItemInfoUIHandler infoPanel;

    public ItemClickOptionsUIHandler itemClickOptions;

    public HeldItemUIHandler heldItemHandler;

    public MenuUIHandler menuUI;

    public Text goldCount;

    public Text legendPage;

    private int currentPage;

    public bool heldItem;
    public int heldItemIndex;
    public int heldItemPage;

    public bool canEquipItems;

    private void Awake()
    {
        if (itemBox.GetGameObject())
        {
            itemBoxInventory = itemBox.GetReferencedComponent<Inventory>();
            itemBoxEquipmentInventory = itemBox.GetReferencedComponent<EquipmentInventory>();

        }
        else
        {
            Debug.LogWarning("Item box was not found. The reference could not be set, or we may just have loaded a hunt scene manually.");
            useItemBoxEquipmentInventory = false;
            useItemBoxInventory = false;
        }
        inventory = player.GetReferencedComponent<Inventory>();
        equipment = player.GetReferencedComponent<Equipment>();
        gold = player.GetReferencedComponent<Gold>();
        gold.goldChangedEvent += GoldChanged;
        GoldChanged(gold.gold);

        if (useItemBoxInventory)
            inventory = itemBoxInventory;
        if (useItemBoxEquipmentInventory)
            inventory = itemBoxEquipmentInventory;

        itemslots = new List<ItemSlotUIHandler>();
        CreateItemslots();

        for (int i = 0; i < itemslots.Count; i++)
        {
            itemslots[i].SetUpDefaults(this, infoPanel);
        }

        ChangePage(0);

        infoPanel.SetUp();
        heldItemHandler.SetUp();
        itemClickOptions.SetUp(inventory, this, infoPanel);

        menuUI.changeToMenuEvent += OnMenuOpened;
        menuUI.changeFromMenuEvent += OnMenuClosed;
        menuUI.changeFromMenuToSelectorEvent += OnMenuClosed;
    }

    private void GoldChanged(int amt)
    {
        goldCount.text = "Gold: " + amt.ToString() + "g";
    }

    private void OnDisable()
    {
        heldItem = false;
    }

    private void OnDestroy()
    {
        if (gold)
        {
            gold.goldChangedEvent -= GoldChanged;
        }
    }

    private void CreateItemslots()
    {
        for (int y = 0; y < rows; y++)
        {
            for (int x = 0; x < columns; x++)
            {
                ItemSlotUIHandler itemslot = Instantiate(itemslotUIPrefab).GetComponent<ItemSlotUIHandler>();
                itemslot.gameObject.SetActive(true);
                itemslots.Add(itemslot);

                RectTransform transform = itemslot.GetComponent<RectTransform>();

                Vector2 pos = transform.localPosition;

                pos.x = startXOffset + (transform.sizeDelta.x * x) + (marginX * x);
                pos.y = Screen.height + (startYOffset - ((transform.sizeDelta.y * y) + (marginY * y)));

                transform.localPosition = pos;

                itemslot.transform.SetParent(gameObject.transform);
            }
        }
    }

    public void ChangePage(int page)
    {
        Debug.Log("Changed page to page " + page + " in inventory " + inventory.inventoryName);

        if (page < 0)
        {
            Debug.Log("Cannot set page to less than zero. (current: " + currentPage + ", tried to go to: " + page + ", max is: " + inventory.MaxPageCount + ")");
            return;
        }
        else if (page >= inventory.MaxPageCount)
        {
            Debug.Log("Cannot set page to greater than MaxPageCount. (current: " + currentPage + ", tried to go to: " + page + ", max is: " + inventory.MaxPageCount + ")");
            return;
        }

        int offset = page * pageSize;   //get the item offset

        foreach (ItemSlotUIHandler slot in itemslots)
        {   //reset all our slots so changing page visually works
            slot.SlotReset();
        }

        currentPage = page;

        for (int i = 0; i < itemslots.Count; i++)
        {
            itemslots[i].PageChanged((currentPage * pageSize) + i);
        }

        legendPage.text = (page + 1) + "/" + inventory.MaxPageCount;

        if (goldCount) goldCount.text = "Gold: " + gold.gold + "g";

    }

    public void ChangePageLeft()
    {
        ChangePage(currentPage - 1);
    }

    public void ChangePageRight()
    {
        ChangePage(currentPage + 1);
    }

    public void ClickItem(int index)
    {
        if (!heldItem)
        {
            if (inventory.Items[index])
            {
                itemClickOptions.StartClickItem(inventory.Items[index], GetItemslotByInventoryIndex(index).transform.position);
            }
        }
        else MoveItemOption(index);
    }

    public void MoveItemOption(int index)
    {
        if (!heldItem)
        {   //don't have anything held - start holding the clicked item
            StartHoldItem(index);
        }
        else
        {   //do have something held - attempt to move the item, and stop holding anything.
            EndHoldItem(index);
        }
    }

    public void EquipItemOption(int index)
    {
        TryEquipItem(index);
    }

    public void UnequipItemOption(int index)
    {
        UnequipItem(index);
    }

    private ItemSlotUIHandler GetItemslotByInventoryIndex(int index)
    {
        return itemslots[index - (currentPage * pageSize)];
    }

    private bool TryEquipItem(int index)
    {
        if (inventory.Items[index])
        {
            if (inventory.Items[index].IsArmor)
            {
                if (!inventory.Items[index].Equipped)
                {
                    equipment.EquipArmor(inventory.Items[index]);
                    Debug.Log("Equipped armor " + inventory.Items[index] + " at index " + index);
                    return true;
                }
            }

            if (inventory.Items[index].IsWeapon)
            {
                if (!inventory.Items[index].Equipped)
                {
                    equipment.EquipWeapon(inventory.Items[index]);
                    Debug.Log("Equipped weapon " + inventory.Items[index] + " at index " + index);
                    return true;
                }
            }
        }
        return false;
    }

    private void UnequipItem(int index)
    {
        equipment.Unequip(inventory, index);
    }

    /// <summary>
    /// Start holding an item.
    /// </summary>
    /// <param name="index">The index of the Item Slot we clicked on.</param>
    private void StartHoldItem(int index)
    {
        if (inventory.Items[index])
        {   //if we have an item in the slot, start holding it
            heldItem = true;
            heldItemIndex = index;
            heldItemPage = currentPage;

            heldItemHandler.StartHoldItem(inventory.Items[index]);
        }
    }

    /// <summary>
    /// Stop holding an item.
    /// </summary>
    /// <param name="index">The index of the Item Slot we clicked on.</param>
    private void EndHoldItem(int index)
    {
        heldItem = false;
        if (heldItemIndex != index)
        {
            TryMoveHeldItem(index);
        }
        heldItemHandler.SlotReset();
    }

    /// <summary>
    /// Tries to move the current held item to the given index.
    /// </summary>
    public void TryMoveHeldItem(int endIndex)
    {
        Debug.Log("moving item " + heldItemIndex + " to " + endIndex + 
            "\nItemslots: start " + (heldItemIndex - (pageSize * heldItemPage)) + " end " + (endIndex - (pageSize * currentPage)));

        inventory.MoveItem(heldItemIndex, endIndex);
        itemslots[heldItemIndex - (pageSize * heldItemPage)].PageChanged(-1);//.SetItemSlotItem(heldItemIndex, inventory.Items[heldItemIndex]);  
        itemslots[endIndex - (pageSize * currentPage)].PageChanged(-1);//.SetItemSlotItem(endIndex, inventory.Items[endIndex]);
    }

    private void OnMenuOpened(MenuUIHandler oldMenu)
    {
        ChangePage(0);
    }

    private void OnMenuClosed(MenuUIHandler newMenu)
    {
        EndHoldItem(heldItemIndex);
    }

    private void OnMenuClosed()
    {
        OnMenuClosed(null);
    }
}
