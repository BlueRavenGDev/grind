﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ItemInfoUIHandler : MonoBehaviour
{
    public Image background;
    public Text itemName;
    public Text itemDescription;
    public Text itemCount;
    public Text itemRarity;

    public ItemInfoArmorStatsUIHandler armorStats;
    public ItemInfoWeaponStatsUIHandler weaponStats;

    public Vector2 offset = new Vector2(25, -25);

    public bool canActive = true;

    public void SetUp()
    {
        ResetInfo();
    }

    public void StartHover(Item item, Inventory inventory, Equipment equipment, Vector2 pos)
    {
        if (canActive)
        {
            int countInInventory = 0;

            countInInventory = inventory.FindItemCount(item);

            background.gameObject.SetActive(true);
            itemName.gameObject.SetActive(true);
            itemDescription.gameObject.SetActive(true);
            if (countInInventory > 0)
                itemCount.gameObject.SetActive(true);
            itemRarity.gameObject.SetActive(true);
            if (item is ItemArmor)
                armorStats.gameObject.SetActive(true);
            if (item is ItemWeapon)
                weaponStats.gameObject.SetActive(true);

            itemName.text = item.iname;
            itemDescription.text = item.description;
            if (countInInventory > 0)
            {
                itemCount.text = countInInventory.ToString();
                if (countInInventory >= item.maxStack)
                    itemCount.color = Color.red;
                else itemCount.color = Color.white;
            }

            itemRarity.text = "Rarity " + item.rarity.ToString();
            itemRarity.color = Item.GetRarityColor(item.rarity);
            
            if (equipment)
            { 
                if (item is ItemArmor)
                {
                    ItemArmor currentArmor = equipment.GetEquippedOfType((item as ItemArmor).armorType);
                    armorStats.SetWeaponValues(item as ItemArmor, currentArmor);
                }
                if (item is ItemWeapon)
                    weaponStats.SetWeaponValues(item as ItemWeapon, equipment.weapon != null ? equipment.weapon.attachedItemWeapon : null);
            }

            transform.position = pos + offset;
        }
        //LimitPositionToScreenBounds();
    }

    public void ResetInfo()
    {
        background.gameObject.SetActive(false);
        itemName.gameObject.SetActive(false);
        itemDescription.gameObject.SetActive(false);
        itemCount.gameObject.SetActive(false);
        itemRarity.gameObject.SetActive(false);

        armorStats.gameObject.SetActive(false);
        weaponStats.gameObject.SetActive(false);
    }

    private void LimitPositionToScreenBounds()
    {
        Vector2 pos = transform.localPosition;

        Vector2 size = GetComponent<RectTransform>().sizeDelta;
        pos.x = Mathf.Clamp(pos.x, -Screen.width / 2, (Screen.width / 2) - size.x);
        pos.y = Mathf.Clamp(pos.y, -Screen.height / 2, (Screen.height / 2) - size.y);

        transform.localPosition = pos;
    }
}
