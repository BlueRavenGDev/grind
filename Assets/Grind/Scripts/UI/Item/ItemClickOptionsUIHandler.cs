﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using GameObjectHolders;

public class ItemClickOptionsUIHandler : MonoBehaviour
{
    public Image background;

    public List<Button> options;

    public ItemInstance item;

    [Tooltip("The index at which an equip option is displayed.")]
    public int equipIndex = -1;
    [Tooltip("The index at which the unequip option is displayed.")]
    public int unequipIndex = -1;

    [Tooltip("The index at which an upgrade option is displayed.")]
    public int upgradeIndex = -1;

    private Inventory inventory;
    private ItemInfoUIHandler itemInfo;
    private InventoryUIHandler inventoryUI;

    //public UpgradeListUIHandler upgradeListUI;
    //public RefineUIHandler refineUI;

    public Vector2 offset = new Vector2(50, -75);

    public void SetUp(Inventory inventory, InventoryUIHandler inventoryUI, ItemInfoUIHandler itemInfo)
    {
        this.inventory = inventory;
        this.inventoryUI = inventoryUI;
        this.itemInfo = itemInfo;

        ResetOptions();
    }

    public void StartClickItem(ItemInstance itemInstance, Vector2 position)
    {
        item = itemInstance;

        background.gameObject.SetActive(true);

        float height = -75;//magic offset number - don't ask

        for (int i = 0; i < options.Count; i++)
        {
            Button option = options[i];

            ItemClickOptionValid validChecker = option.GetComponent<ItemClickOptionValid>();

            //if there is no valid checker, continue on as normal. If there is one, then we check it to see if the item is valid to have the option.
            //If it is not, we do not display it (use continue)
            if (validChecker)
            {
                if (!validChecker.OptionIsValid(item))
                    continue;
            }

            option.gameObject.SetActive(true);
            height += 25;
            option.gameObject.transform.localPosition = new Vector3(0, -height);
        }

        transform.position = position + offset;
        background.GetComponent<RectTransform>().sizeDelta = new Vector2(50, height);

        itemInfo.canActive = false; //disable the item info temporarily
    }

    public void ClickAction(Button option)
    {
        ItemClickAction clickAction = option.GetComponent<ItemClickAction>();
        clickAction.ClickAction(item);

        AnyOption();
    }

    public void ResetOptions()
    {
        background.gameObject.SetActive(false);

        foreach (Button option in options)
            option.gameObject.SetActive(false);

        item = null;
    }

    public void MoveOption()
    {
        inventoryUI.MoveItemOption(item.inventoryIndex);
        AnyOption();
    }

    public void TransferOption(GameObjectHolder otherInventoryHolder)
    {
        otherInventoryHolder.GetComponent<Inventory>().TransferItemToInventory(item, item.parentInventory);
        AnyOption();
    }

    public void EquipOption()
    {
        inventoryUI.EquipItemOption(item.inventoryIndex);
        AnyOption();
    }

    public void UnequipOption()
    {
        inventoryUI.UnequipItemOption(item.inventoryIndex);
        AnyOption();
    }

    public void DropOption(int amt, DropAmtUIHandler drop)
    {
        if (amt > 0)
        {
            Debug.Log("Dropping item of type " + item.attachedItem.name + " with a stacksize of " + amt);
            inventory.DropItem(item.inventoryIndex, amt);
            AnyOption();
        }
        drop.ResetUI();
    }

    public void UpgradeOption()
    {
        //upgradeListUI.SetUp();
        //upgradeListUI.ShowUpgrades(item.itemStack);

        AnyOption();
    }

    public void RefineOption()
    {
        inventory.GetComponent<RefiningInventory>().SetItemToBeRefined(item);
        //refineUI.SetUp();

        AnyOption();
    }

    public void AnyOption()
    {
        inventory.CheckValidItemInstance(item);

        itemInfo.canActive = true;
        ResetOptions();
    }
}
