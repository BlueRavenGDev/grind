﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ItemInfoWeaponStatsUIHandler : MonoBehaviour
{
    public Text damage;
    public Text damageType;
    public Text elementDamage;
    
    public void SetWeaponValues(ItemWeapon weapon, ItemWeapon currentWeapon)
    {
        if (weapon == currentWeapon || !currentWeapon)
        {
            damage.text = weapon.damage.ToString();
            damageType.text = weapon.elementType.ToString();
            elementDamage.text = weapon.elementDamage.ToString();
        }
        else
        {
                damage.text = weapon.damage + GetDiff(weapon.damage, currentWeapon.damage);
                damageType.text = weapon.elementType + "(" + currentWeapon.elementType + ")";
                elementDamage.text = weapon.elementDamage + GetDiff(weapon.elementDamage, currentWeapon.elementDamage);
        }
    }

    private string GetDiff(int value, int oldValue)
    {
        return "(" + (value - oldValue).ToString() + ")";

    }
}
