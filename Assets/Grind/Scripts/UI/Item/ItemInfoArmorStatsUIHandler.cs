﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ItemInfoArmorStatsUIHandler : MonoBehaviour
{
    public Text defense;
    public Text fire, ice, elec, water;

    public void SetUp()
    {

    }

    public void SetWeaponValues(ItemArmor armor, ItemArmor currentArmor)
    {
        if (armor == currentArmor || !currentArmor)
        {
            defense.text = armor.defense.ToString();
            fire.text = armor.resistFire.ToString();
            ice.text = armor.resistIce.ToString();
            elec.text = armor.resistElec.ToString();
            water.text = armor.resistWater.ToString();
        }
        else
        {
            defense.text = armor.defense + GetDiff(armor.defense, currentArmor.defense);
            fire.text = armor.resistFire + GetDiff(armor.resistFire, currentArmor.resistFire);
            ice.text = armor.resistIce + GetDiff(armor.resistIce, currentArmor.resistIce);
            elec.text = armor.resistElec + GetDiff(armor.resistElec, currentArmor.resistElec);
            water.text = armor.resistWater + GetDiff(armor.resistWater, currentArmor.resistWater);
        }
    }

    private string GetDiff(int value, int oldValue)
    {
        return "(" + (value - oldValue).ToString() + ")";
    }
}
