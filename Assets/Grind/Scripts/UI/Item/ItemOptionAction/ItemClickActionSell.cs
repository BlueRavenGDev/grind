﻿using UnityEngine;
using System.Collections;
using GameObjectHolders;

public class ItemClickActionSell : ItemClickAction
{
    public int amt;
    public DropAmtUIHandler cascadeUIHandler;
    public GameObjectReference player;

    public override void ClickAction(ItemInstance item)
    {
        if (amt > 0)
        {
            Debug.Log("Selling item of type " + item.attachedItem.name + " with a stacksize of " + amt);
            item.parentInventory.SellItem(item, amt, player.GetReferencedComponent<Gold>());
        }

        cascadeUIHandler.ResetUI();
    }
}
