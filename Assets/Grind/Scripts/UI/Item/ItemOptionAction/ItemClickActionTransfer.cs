﻿using UnityEngine;
using System.Collections;
using GameObjectHolders;

public class ItemClickActionTransfer : ItemClickAction
{
    public GameObjectReference oldInventory;
    public GameObjectReference toInventory;

    public override void ClickAction(ItemInstance item)
    {
        toInventory.GetReferencedComponent<InventoryManager>().GetInventory("Items")
            .TransferItemToInventory(item, oldInventory.GetReferencedComponent<InventoryManager>().GetInventory("Items"));
    }
}
