﻿using UnityEngine;
using System.Collections;

public class ItemClickActionRefine : ItemClickAction
{
    public MenuSelectorUIHandler menuSelector;
    public MenuUIHandler toMenu;

    public GameObjectHolders.GameObjectReference player;

    public RefineUIHandler refineUI;

    public override void ClickAction(ItemInstance item)
    {
        menuSelector.ChangeToMenuFromMenu(toMenu);

        player.GetReferencedComponent<InventoryManager>().GetInventory("Refining").GetComponent<RefiningInventory>().SetItemToBeRefined(item);
        refineUI.SetUp();
    }
}
