﻿using UnityEngine;
using System.Collections;

public class ItemClickActionDrop : ItemClickAction
{
    public int amt;
    public DropAmtUIHandler cascadeUIHandler;
    public override void ClickAction(ItemInstance item)
    {
        if (amt > 0)
        {
            Debug.Log("Dropping item of type " + item.attachedItem.name + " with a stacksize of " + amt);
            item.parentInventory.DropItem(item.inventoryIndex, amt);
        }
        cascadeUIHandler.ResetUI();
    }
}
