﻿using UnityEngine;
using System.Collections;

public class ItemClickActionBlacksmithUpgrade : ItemClickAction
{
    public MenuSelectorUIHandler menuSelector;
    public MenuUIHandler toMenu;

    public UpgradeListUIHandler upgradeListUI;

    public override void ClickAction(ItemInstance item)
    {
        menuSelector.ChangeToMenuFromMenu(toMenu);

        upgradeListUI.SetUp();

        if (item.IsArmor)
            upgradeListUI.ShowUpgrades((item.attachedItem as ItemArmor).upgradeRecipes);
        if (item.IsWeapon)
            upgradeListUI.ShowUpgrades((item.attachedItem as ItemWeapon).upgradeRecipes);
    }
}
