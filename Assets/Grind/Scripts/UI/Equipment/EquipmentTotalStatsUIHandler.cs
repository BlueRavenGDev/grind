﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EquipmentTotalStatsUIHandler : MonoBehaviour
{
    public Text damage;
    public Text elementType;
    public Text elementDamage;

    public Text defense;
    public Text fire, ice, elec, water;

    public void UpdateStats(Equipment equipment)
    {
        if (equipment.weapon)
        {
            damage.text = equipment.weapon.attachedItemWeapon.damage.ToString();
            elementType.text = equipment.weapon.attachedItemWeapon.elementType.ToString();
            elementDamage.text = equipment.weapon.attachedItemWeapon.elementDamage.ToString();
        }

        else elementType.text = "None";
        defense.text = equipment.SumDefense().ToString();
        fire.text = equipment.SumDefenseOfType(Resistance.DamageType.Fire).ToString();
        ice.text = equipment.SumDefenseOfType(Resistance.DamageType.Ice).ToString();
        elec.text = equipment.SumDefenseOfType(Resistance.DamageType.Elec).ToString();
        water.text = equipment.SumDefenseOfType(Resistance.DamageType.Water).ToString();
    }
}
