﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class EquipmentItemUIHandler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Image icon;

    private ItemInfoUIHandler infoPanel;
    private Equipment equipment;

    private Item item;

    public void SetUp(ItemInfoUIHandler infoPanel, Equipment equipment)
    {
        this.infoPanel = infoPanel;

        SlotReset();
    }

    public void SetItem(Item item)
    {
        if (item)
        {
            this.item = item;

            if (item.icon)
            {   //our item has an icon, so show it.
                icon.gameObject.SetActive(true);
                if (item.colorOffRarity)
                    icon.color = Item.GetRarityColor(item.rarity);
                else icon.color = item.spriteColor;
                icon.sprite = item.icon;
            }
            else icon.gameObject.SetActive(false);
        }
        else SlotReset();
    }

    public void SlotReset()
    {
        icon.gameObject.SetActive(false);
        item = null;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (item)
        {   //we don't need to see diff stuff, so exclude inventory.
            infoPanel.StartHover(item, null, equipment, transform.position);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (item)
        {
            infoPanel.ResetInfo();
        }
    }
}
