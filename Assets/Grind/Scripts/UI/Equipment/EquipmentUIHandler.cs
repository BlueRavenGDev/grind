﻿using UnityEngine;
using System.Collections;
using GameObjectHolders;

public class EquipmentUIHandler : MonoBehaviour
{
    public ItemVisualUIHandler weapon;
    public ItemVisualUIHandler head, arms, chest, legs, feet;

    public GameObjectReference player;
    private Inventory inventory;
    private Equipment equipment;

    public ItemInfoUIHandler infoPanel;
    public EquipmentTotalStatsUIHandler equipmentStats;

    private void Awake()
    {
        inventory = player.GetReferencedComponent<Inventory>();
        equipment = player.GetReferencedComponent<Equipment>();

        equipment.equipmentUpdatedEvent += EquipmentUpdated;

        EquipmentUpdated(null);
    }

    private void Start()
    {
        weapon.SetUpDefaults(infoPanel);
        head.SetUpDefaults(infoPanel);
        arms.SetUpDefaults(infoPanel);
        chest.SetUpDefaults(infoPanel);
        legs.SetUpDefaults(infoPanel);
        feet.SetUpDefaults(infoPanel);
        infoPanel.SetUp();

        SetEquipmentSlots();
        equipmentStats.UpdateStats(equipment);
    }

    public void SetEquipmentSlots()
    {
        if (equipment.weapon)
            weapon.SetItemVisualItem(equipment.weapon.itemStack, inventory, equipment, true);
        else weapon.SlotReset();

        if (equipment.armorHead)
            head.SetItemVisualItem(equipment.armorHead.itemStack, inventory, equipment, true);
        else head.SlotReset();
        if (equipment.armorArms)
            arms.SetItemVisualItem(equipment.armorArms.itemStack, inventory, equipment, true);
        else arms.SlotReset();
        if (equipment.armorChest)
            chest.SetItemVisualItem(equipment.armorChest.itemStack, inventory, equipment, true);
        else chest.SlotReset();
        if (equipment.armorLegs)
            legs.SetItemVisualItem(equipment.armorLegs.itemStack, inventory, equipment, true);
        else legs.SlotReset();
        if (equipment.armorFeet)
            feet.SetItemVisualItem(equipment.armorFeet.itemStack, inventory, equipment, true);
        else feet.SlotReset();
    }

    public void EquipmentUpdated(ItemInstance item)
    {
        SetEquipmentSlots();
        equipmentStats.UpdateStats(equipment);
    }
}
