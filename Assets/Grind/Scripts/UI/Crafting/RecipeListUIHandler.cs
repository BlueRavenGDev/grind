﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using GameObjectHolders;

public class RecipeListUIHandler : MonoBehaviour
{
    public List<Recipe> recipes;

    public GameObject recipePrefab;

    public ItemInfoUIHandler infoPanel;

    public GameObjectReference inventory;
    private Inventory _inventory;

    public int pageSize = 7;
    public float margin = 5;

    public int currentPage;

    private List<RecipeSlotUIHandler> recipeSlots;

    public Text legendPage;

    private void Awake()
    {
        _inventory = inventory.GetReferencedComponent<Inventory>();
        _inventory.itemCraftedEvent += OnItemCrafted;
        recipeSlots = new List<RecipeSlotUIHandler>();
    }

    private void Start()
    {
        CreateRecipeSlots();

        for (int i = 0; i < recipeSlots.Count; i++) 
        {
            recipeSlots[i].SetUpDefaults(infoPanel);
        }

        ChangePage(0);

        infoPanel.SetUp();
    }

    public void CreateRecipeSlots()
    {
        for (int i = 0; i < pageSize; i++)
        {
            RecipeSlotUIHandler recipeslot = Instantiate(recipePrefab).GetComponent<RecipeSlotUIHandler>();
            recipeslot.gameObject.SetActive(true);
            recipeSlots.Add(recipeslot);

            RectTransform rectTransform = recipeslot.GetComponent<RectTransform>();

            Vector2 pos = recipeslot.transform.localPosition;

            pos.y = Screen.height + ((i * margin) - (i * rectTransform.sizeDelta.y));

            recipeslot.transform.localPosition = pos;

            recipeslot.transform.SetParent(transform);
        }
    }

    public void ChangePage(int page)
    {
        int offset = page * pageSize;

        foreach (RecipeSlotUIHandler recipeSlot in recipeSlots)
        {
            recipeSlot.SlotReset();
        }

        for (int i = offset; i < Mathf.Min(offset + pageSize, recipes.Count); i++)
        {
            recipeSlots[i - offset].SetRecipeSlotRecipe(i, recipes[i], _inventory);
        }

        currentPage = page;

        legendPage.text = (page + 1) + "/" + ((int)(recipes.Count / pageSize) + 1);
    }

    public void ChangePageLeft()
    {
        ChangePage(currentPage - 1);
    }

    public void ChangePageRight()
    {
        ChangePage(currentPage + 1);
    }

    public void OnItemCrafted(int index)
    {
        /*for (int i = 0; i < pageSize; i++)
        {
            recipeSlots[i].SetRecipeSlotRecipe((currentPage * pageSize) + i, recipes[i], _inventory);
        }*/
        ChangePage(currentPage);
    }
}