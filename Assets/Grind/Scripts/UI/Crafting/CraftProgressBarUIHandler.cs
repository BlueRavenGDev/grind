﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class CraftProgressBarUIHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Image progressImage;
    private float progressMaxWidth;

    public int startClickProgress = 120;  //how many frames it takes to craft the item at the start.
    public int progressDecrement = 10;

    public int currentMaxProgress = 120;
    public int clickProgress;

    public int minClickProgress = 30;

    private RecipeSlotUIHandler recipeSlot;

    private Inventory inventory;
    private Recipe recipe;

    private bool held;

    public void SetUp(RecipeSlotUIHandler recipeSlot, Recipe recipe, Inventory inventory)
    {
        this.recipeSlot = recipeSlot;
        this.recipe = recipe;
        this.inventory = inventory;

        progressMaxWidth = progressImage.GetComponent<RectTransform>().sizeDelta.x;
    }

    private void Update()
    {
        if (held)
        {
            Debug.Log("Held, progress: " + clickProgress);
            clickProgress++;

            if (clickProgress > currentMaxProgress)
            {
                currentMaxProgress -= progressDecrement;
                if (currentMaxProgress < minClickProgress)
                    currentMaxProgress = minClickProgress;

                clickProgress = 0;

                //if (inventory.TryCraftItem(recipe))
                {
                    //recipeSlot.OnRecipeCrafted(recipe, inventory);
                }
            }
        }

        Vector2 size = progressImage.GetComponent<RectTransform>().sizeDelta;

        size.x = progressMaxWidth * (float)((float)clickProgress / (float)currentMaxProgress);

        progressImage.GetComponent<RectTransform>().sizeDelta = size;
    }

    public void OnPointerDown(PointerEventData eventData)
    {   //while pointer is held down, increment value - fill up meter
        held = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        held = false;

        clickProgress = 0;
        currentMaxProgress = startClickProgress;
    }
}
