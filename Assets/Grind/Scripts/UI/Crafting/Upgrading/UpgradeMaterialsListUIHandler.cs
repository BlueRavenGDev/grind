﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(ElementPopulatorUIHandler))]
public class UpgradeMaterialsListUIHandler : MonoBehaviour
{
    public ItemVisualUIHandler itemVisualPrefab;

    public ItemInfoUIHandler infoPanel;

    public ItemVisualUIHandler outputItem;

    public Text goldCostText;
    public Text goldAmtText;

    private ElementPopulatorUIHandler elementPopulator;

    private bool initialized;

    public bool hideGold;

    private Gold gold;

    public void SetUp(Gold gold)
    {
        if (!initialized)
        {
            if (gold && !hideGold)
            {
                this.gold = gold;
                goldAmtText.gameObject.SetActive(true);
                gold.goldChangedEvent += GoldChanged;
                GoldChanged(gold.gold);
            }
            else
            {
                if (goldAmtText)
                    goldAmtText.gameObject.SetActive(false);
            }

            elementPopulator = GetComponent<ElementPopulatorUIHandler>();
            elementPopulator.elementCreatedEvent += ElementCreated;
            elementPopulator.startXOffset += Screen.width / 2;
            elementPopulator.SetUpElements();

            outputItem.SetUpDefaults(infoPanel);

            initialized = true;
        }
    }

    private void GoldChanged(int amt)
    {
        goldAmtText.text = "Gold: " + amt.ToString() + "g";
    }

    private void ElementCreated(GameObject gameObject)
    {
        ItemVisualUIHandler itemVisual = gameObject.GetComponent<ItemVisualUIHandler>();

        itemVisual.SetUpDefaults(infoPanel);
    }

    public void SetRecipe(Recipe recipe, Inventory inventory, Equipment equipment)
    {
        if (recipe)
        {
            if (recipe.costsGold)
            {
                goldCostText.gameObject.SetActive(true);
                goldCostText.text = "Cost: " + recipe.goldCost + "g";
            }
            else goldCostText.gameObject.SetActive(true);

            for (int i = 0; i < elementPopulator.elements.Count; i++)
            {
                ItemVisualUIHandler material = elementPopulator.elements[i].GetComponent<ItemVisualUIHandler>();

                if (i < recipe.inputItems.Count)
                {
                    material.SetItemVisualItem(recipe.inputItems[i], inventory, equipment, true);
                }
            }

            outputItem.SetItemVisualItem(recipe.outputItem, inventory, equipment, true);
        }
    }

    private void OnDestroy()
    {
        if (gold)
        {
            gold.goldChangedEvent -= GoldChanged;
        }
    }
}
