﻿using UnityEngine;
using System.Collections;

public class UpgradeSlotUIHandler : MonoBehaviour
{
    public ItemVisualUIHandler upgradingItemVisual;
    public ItemVisualUIHandler outputItemVisual;

    [HideInInspector]
    public UpgradeListUIHandler upgradeList;

    private Recipe upgradeRecipe;

    public void SetUpDefaults(ItemInfoUIHandler itemInfo, UpgradeListUIHandler upgradeList)
    {
        upgradingItemVisual.SetUpDefaults(itemInfo);
        outputItemVisual.SetUpDefaults(itemInfo);

        this.upgradeList = upgradeList;
    }

    public void SetUpgrade(ItemStack upgradingItem, Recipe recipe, Inventory inventory, Equipment equipment)
    {
        upgradingItemVisual.SetItemVisualItem(upgradingItem, inventory, equipment);
        outputItemVisual.SetItemVisualItem(recipe.outputItem, inventory, equipment);
        upgradeRecipe = recipe;
    }

    public void ResetSlot()
    {
        upgradingItemVisual.SlotReset();
        outputItemVisual.SlotReset();

        upgradeRecipe = null;
    }

    public void ClickedUpgrade()
    {
        upgradeList.ShowRecipeMaterials(upgradeRecipe);
    }
}
