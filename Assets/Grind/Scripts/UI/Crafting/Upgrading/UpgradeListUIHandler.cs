﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameObjectHolders;

public class UpgradeListUIHandler : PagedUI
{
    public enum CraftOutputInventory
    {
        ItemBox,
        ItemBoxEquipment,
        Player
    }

    [Header("Upgrade List")]
    public UpgradeSlotUIHandler upgradePrefab;

    public ItemInfoUIHandler infoPanel;
    public UpgradeMaterialsListUIHandler recipeMaterialsUI;

    private List<UpgradeSlotUIHandler> upgradeSlots;

    public GameObjectReference itemBox;
    private Inventory itemBoxInventory;
    private Inventory itemBoxEquipmentInventory;
    private InventoryManager itemBoxInvManager;

    public GameObjectReference player;
    private Inventory playerInventory;
    private Equipment equipment;
    private Gold gold;

    private Inventory outputInventory;

    [Tooltip("Craft using items from the item box if true, player's inventory if false.")]
    public bool craftFromItemBox;

    public CraftOutputInventory craftOutputInventory = CraftOutputInventory.Player;

    [Header("Recipe")]
    public List<Recipe> recipes;
    public RecipeTypeList recipeTypeList;

    public Recipe.RecipeType recipeType;

    [Header("Element Creation")]
    public int upgradeSlotCount = 7;
    public float margin = 5;
    public float offset = 5;

    private bool initialized;

    private Recipe currentClickedRecipe;

    private void OnEnable()
    {
        SetUp();

        ChangePage(0);    
    }

    protected override void Start()
    {
        //StartCoroutine(DEBUGWaitThenStart());
        SetUp();

        recipeMaterialsUI.SetUp(gold);
    }

    public void SetUp()
    {
        if (!initialized)
        {
            upgradeSlots = new List<UpgradeSlotUIHandler>();

            if (itemBox.GetGameObject())
            {
                itemBoxInvManager = itemBox.GetReferencedComponent<InventoryManager>();
                itemBoxInventory = itemBoxInvManager.GetInventory("Items");
                itemBoxEquipmentInventory = itemBoxInvManager.GetInventory("Equipment");
            }
            else
            {
                Debug.LogWarning("Item box was not found. The reference could not be set, or we may just have loaded a hunt scene manually.");
                craftFromItemBox = false;
            }
            playerInventory = player.GetReferencedComponent<InventoryManager>().GetInventory("Items");
            equipment = player.GetReferencedComponent<Equipment>();
            gold = player.GetReferencedComponent<Gold>();

            if (craftOutputInventory == CraftOutputInventory.Player)
                outputInventory = playerInventory;
            else if (craftOutputInventory == CraftOutputInventory.ItemBox)
                outputInventory = itemBoxInventory;
            else if (craftOutputInventory == CraftOutputInventory.ItemBoxEquipment)
                outputInventory = itemBoxEquipmentInventory;

            if (recipeTypeList && recipeTypeList.recipeType == recipeType)
                recipes = recipeTypeList.recipes;

            CreateUpgradeSlots();

            initialized = true;
        }
    }

    public override void ChangePage(int toPage)
    {
        base.ChangePage(toPage);

        ShowUpgrades(recipes);
    }

    public void ShowUpgrades(RecipeTypeList recipesList)
    {
        ShowUpgrades(recipesList.recipes);
    }

    public void ShowUpgrades(List<Recipe> recipes)
    {
        this.recipes = recipes;

        maxPages = (recipes.Count / upgradeSlotCount) + 1;

        foreach (UpgradeSlotUIHandler upgradeSlot in upgradeSlots)
        {
            upgradeSlot.ResetSlot();
        }

        for (int i = 0; i < upgradeSlotCount; i++)
        {
            int index = (currentPage * upgradeSlotCount) + i;

            if (index < recipes.Count)
            {
                Recipe recipe = recipes[index];

                if (CheckValidRecipe(recipe))
                {
                    SetUpgradeSlot(i, recipe);
                }
            }
        }
    }

    private void CreateUpgradeSlots()
    {
        for (int i = 0; i < upgradeSlotCount; i++)
            CreateUpgradeSlot(i);
    }

    private void CreateUpgradeSlot(int index)
    {
        UpgradeSlotUIHandler upgrade = Instantiate(upgradePrefab);
        upgrade.gameObject.SetActive(true);

        upgrade.SetUpDefaults(infoPanel, this);

        //set our position correctly
        RectTransform rectTransform = upgrade.GetComponent<RectTransform>();

        Vector2 pos = upgrade.transform.localPosition;
        pos.y = offset + Screen.height + ((index * margin) - (index * rectTransform.sizeDelta.y));
        upgrade.transform.localPosition = pos;

        upgrade.transform.SetParent(transform);

        upgradeSlots.Add(upgrade);
    }

    private void SetUpgradeSlot(int index, Recipe recipe)
    {
        if (recipe.inputItems.Count > 0)
            upgradeSlots[index].SetUpgrade(recipe.inputItems[0], recipe, playerInventory, equipment);
    }

    private bool CheckValidRecipe(Recipe recipe)
    {
        if (recipe)
        {
            if (recipe.recipeType != recipeType)
                Debug.LogWarning("Recipe type for the given recipe not the same as the recipe type for the Recipe UI Handler! Recipe Type: " + recipe.recipeType + " UI Type: " + recipeType);
            return recipe.recipeType == recipeType;
        }

        return false;
    }

    public void ShowRecipeMaterials(Recipe recipe)
    {
        currentClickedRecipe = recipe;
        recipeMaterialsUI.SetRecipe(recipe, playerInventory, equipment);
    }

    public void ClickMakeRecipe()
    {
        if (currentClickedRecipe)
        {
            if (!craftFromItemBox)
                playerInventory.TryCraftItem(currentClickedRecipe, gold, outputInventory);
            else
            {
                itemBoxInvManager.TryJointCraftItem(currentClickedRecipe, gold, outputInventory);
            }
            //after this, we should return to the crafting menu.
        }
    }
}
