﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class RecipeSlotUIHandler : MonoBehaviour
{
    public Text recipeIndex;

    public ItemVisualUIHandler outputItem;
    public ItemVisualUIHandler inputItem1, inputItem2, inputItem3, inputItem4;
    public CraftProgressBarUIHandler progressBar;

    public void SetUpDefaults(ItemInfoUIHandler infoPanel)
    {
        outputItem.SetUpDefaults(infoPanel);
        inputItem1.SetUpDefaults(infoPanel);
        inputItem2.SetUpDefaults(infoPanel);
        inputItem3.SetUpDefaults(infoPanel);
        inputItem4.SetUpDefaults(infoPanel);

        InputOutputSetstate(false);
        recipeIndex.gameObject.SetActive(false);
        progressBar.gameObject.SetActive(false);
    }

    public void SetRecipeSlotRecipe(int index, Recipe recipe, Inventory inventory)
    {
        if (recipe)
        {
            InputOutputSetstate(true);
            recipeIndex.gameObject.SetActive(true);
            progressBar.gameObject.SetActive(true);

            outputItem.SetItemVisualItem(recipe.outputItem, inventory, null);
            if (recipe.inputItems.Count >= 1)
                inputItem1.SetItemVisualItem(recipe.inputItems[0], inventory, null);
            if (recipe.inputItems.Count >= 2)
                inputItem2.SetItemVisualItem(recipe.inputItems[1], inventory, null);
            if (recipe.inputItems.Count >= 3)
                inputItem3.SetItemVisualItem(recipe.inputItems[2], inventory, null);
            if (recipe.inputItems.Count >= 4)
                inputItem4.SetItemVisualItem(recipe.inputItems[3], inventory, null);

            progressBar.SetUp(this, recipe, inventory);
        }
    }

    public void SlotReset()
    {
        outputItem.SlotReset();
        inputItem1.SlotReset();
        inputItem2.SlotReset();
        inputItem3.SlotReset();
        inputItem4.SlotReset();

        InputOutputSetstate(false);
        recipeIndex.gameObject.SetActive(false);
    }

    private void InputOutputSetstate(bool state)
    {
        outputItem.gameObject.SetActive(state);
        inputItem1.gameObject.SetActive(state);
        inputItem2.gameObject.SetActive(state);
        inputItem3.gameObject.SetActive(state);
        inputItem4.gameObject.SetActive(state);
    }
}
