﻿using UnityEngine;

public class GraphicIgnoreRaycast : MonoBehaviour, ICanvasRaycastFilter
{
    [Tooltip("If true, raycasts will IGNORE. if false, raycasts will hit.")]
    public bool active = true;

    public bool IsRaycastLocationValid(Vector2 screenPoint, Camera eventCamera)
    {
        if (active)
            return false;
        return true;
    }
}