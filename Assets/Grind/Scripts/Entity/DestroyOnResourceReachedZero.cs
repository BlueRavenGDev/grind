﻿using UnityEngine;
using System.Collections;

public class DestroyOnResourceReachedZero : MonoBehaviour
{
    private Resource _resource;
    public Resource resource;
    public Resource Resource { get { return _resource != null ? _resource : resource; } }

    private void Awake()
    {
        _resource = GetComponent<Resource>();
        Resource.resourceExhaustedEvent.AddListener(OnResourceExhausted);
    }

    private void OnResourceExhausted()
    {
        Destroy(gameObject);
    }
}
