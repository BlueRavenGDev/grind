﻿using UnityEngine;
using System.Collections;
using GameObjectHolders;

public class CameraHandler : MonoBehaviour
{
    public GameObjectReference defaultTarget;

    public GameObjectReference currentTarget;

    public float dampTime = 0.15f;
    private Vector3 velocity = Vector3.zero;

    private void Start()
    {
        defaultTarget = currentTarget;
    }

    private void FixedUpdate()
    {
        if (currentTarget != null && currentTarget.GetGameObject())
        {
            Camera camera = GetComponent<Camera>();
            Vector3 point = camera.WorldToViewportPoint(currentTarget.GetGameObject().transform.position);
            Vector3 delta = currentTarget.GetGameObject().transform.position - camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z));
            Vector3 destination = transform.position + delta;
            transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
        }
    }
}
