﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ResourcesManager : MonoBehaviour
{
    public List<Resource> resourceReferences;

    private Dictionary<string, Resource> resources;

    public bool findAuto;

    private void Awake()
    {
        resources = new Dictionary<string, Resource>();

        if (findAuto)
        {
            foreach (Resource resource in GetComponents<Resource>().ToList())
            {
                resources.Add(resource.resourceName, resource);
            }
        }
        else
        {
            foreach (Resource resource in resourceReferences)
            {
                resources.Add(resource.resourceName, resource);
            }
        }
    }

    public Resource GetResource(string name)
    {
        return resources.ContainsKey(name) ? resources[name] : null;
    }

    public void ResetResources()
    {
        foreach (Resource resource in resources.Values)
        {
            resource.ResetResource();
        }
    }
}
