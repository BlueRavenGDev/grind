﻿using UnityEngine;
using System.Collections;
using System;

public class BetterCollision : MonoBehaviour
{
    public float rayBoxHeight = .45f;
    public float rayBoxWidth = .45f;

    //public float rayDistFromCenter = 0.45f;

    [Tooltip("how far from the center the rays extend.")]
    //public float rayLength = 0.55f;
    public float rayLengthX = 1.1f;
    public float rayLengthY = 1.1f;

    public float raySetDist = 0.5f;

    public Rigidbody2D rigidbody2d;

    public Action<Vector2> collideLeftEvent;    //vector2 is the point of collision
    public Action<Vector2> collideRightEvent;
    public Action<Vector2> collideUpEvent;
    public Action<Vector2> collideDownEvent;
    public Action collideAnyEvent;              //collide with any. No point of collision given; if you want more precision, use another event.
    public Action collideNoneEvent;             //colliding with nothing

    public int layerWorldIndex = 10;

    private void FixedUpdate()
    {
        Vector2 velocity = rigidbody2d.velocity;
        BetterCollisions(ref velocity);
        rigidbody2d.velocity = velocity;
    }

    /// <summary>
    /// We're using tilemap colliders for world collisions, which are notoriously terrible. Use a box collider and you'll randomly get stuck,
    /// Use a circle/capsule collider and you'll randomly get bumped up into the air.
    /// So instead, we're creating our own collisions, using 2d physics rays.
    /// There's still a 2d box collider inside, with a size of .95, just to catch anything the rays don't.
    /// </summary>
    private void BetterCollisions(ref Vector2 velocity)
    {
        Physics2D.queriesHitTriggers = false;
        Physics2D.queriesStartInColliders = false;

        bool anycol = false;

        bool bot = CollisionsBottom(ref velocity);
        bool left = CollisionsLeft(ref velocity);
        bool right = CollisionsRight(ref velocity);
        bool up = CollisionsUp(ref velocity);

        anycol = bot || left || right || up;

        if (anycol)
        {
            if (collideAnyEvent != null) collideAnyEvent.Invoke();
        }
        else
        {
            if (collideNoneEvent != null) collideNoneEvent.Invoke();
        }
    }

    private bool CollisionsBottom(ref Vector2 velocity)
    {
        RaycastHit2D hit1 = Physics2D.Raycast((Vector2)transform.position - new Vector2(rayBoxWidth, 0), Vector2.down, rayLengthY, 1 << layerWorldIndex);
        RaycastHit2D hit2 = Physics2D.Raycast((Vector2)transform.position + new Vector2(rayBoxWidth, 0), Vector2.down, rayLengthY, 1 << layerWorldIndex);

        bool anyhit = false;
        Vector2 hitpoint = Vector2.zero;

        if (hit1)
        {
            hitpoint = new Vector2(transform.position.x, hit1.point.y + raySetDist);
            anyhit = true;
        }

        if (hit2)
        {
            hitpoint = new Vector2(transform.position.x, hit2.point.y + raySetDist);
            anyhit = true;
        }

        if (anyhit)
        {
            if (velocity.y < 0)
            {
                transform.position = new Vector3(hitpoint.x, hitpoint.y, transform.position.z);

                velocity.y = 0;

                if (collideDownEvent != null) collideDownEvent.Invoke(hitpoint);

                return true;
            }
        }
        return false;
    }

    private bool CollisionsLeft(ref Vector2 velocity)
    {
        RaycastHit2D hit1 = Physics2D.Raycast((Vector2)transform.position - new Vector2(0, rayBoxHeight), Vector2.left, rayLengthX, 1 << layerWorldIndex);
        RaycastHit2D hit2 = Physics2D.Raycast((Vector2)transform.position + new Vector2(0, rayBoxHeight), Vector2.left, rayLengthX, 1 << layerWorldIndex);

        bool anyhit = false;
        Vector2 hitpoint = Vector2.zero;

        if (hit1)
        {
            hitpoint = new Vector2(hit1.point.x + raySetDist, transform.position.y);
            anyhit = true;
        }

        if (hit2)
        {
            hitpoint = new Vector2(hit2.point.x + raySetDist, transform.position.y);
            anyhit = true;
        }

        if (anyhit)
        {
            if (velocity.x < 0)
            {
                transform.position = new Vector3(hitpoint.x, hitpoint.y, transform.position.z);

                velocity.x = 0;

                if (collideLeftEvent != null) collideLeftEvent.Invoke(hitpoint);
                return true;
            }
        }
        return false;
    }

    private bool CollisionsRight(ref Vector2 velocity)
    {
        RaycastHit2D hit1 = Physics2D.Raycast((Vector2)transform.position - new Vector2(0, rayBoxHeight), Vector2.right, rayLengthX, 1 << layerWorldIndex);
        RaycastHit2D hit2 = Physics2D.Raycast((Vector2)transform.position + new Vector2(0, rayBoxHeight), Vector2.right, rayLengthX, 1 << layerWorldIndex);

        bool anyhit = false;
        Vector2 hitpoint = Vector2.zero;

        if (hit1)
        {
            hitpoint = new Vector2(hit1.point.x - raySetDist, transform.position.y);
            anyhit = true;
        }

        if (hit2)
        {
            hitpoint = new Vector2(hit2.point.x - raySetDist, transform.position.y);
            anyhit = true;
        }

        if (anyhit)
        {
            if (velocity.x > 0)
            {
                transform.position = new Vector3(hitpoint.x, hitpoint.y, transform.position.z);

                velocity.x = 0;

                if (collideRightEvent != null) collideRightEvent.Invoke(hitpoint);
                return true;
            }
        }
        return false;
    }

    private bool CollisionsUp(ref Vector2 velocity)
    {
        RaycastHit2D hit1 = Physics2D.Raycast((Vector2)transform.position - new Vector2(rayBoxWidth, 0), Vector2.up, rayLengthY, 1 << layerWorldIndex);
        RaycastHit2D hit2 = Physics2D.Raycast((Vector2)transform.position + new Vector2(rayBoxWidth, 0), Vector2.up, rayLengthY, 1 << layerWorldIndex);

        bool anyhit = false;
        Vector2 hitpoint = Vector2.zero;

        if (hit1)
        {
            hitpoint = new Vector2(transform.position.x, hit1.point.y - raySetDist);
            anyhit = true;
        }

        if (hit2)
        {
            hitpoint = new Vector2(transform.position.x, hit2.point.y - raySetDist);
            anyhit = true;
        }

        if (anyhit)
        {
            if (velocity.y > 0)
            {
                transform.position = new Vector3(hitpoint.x, hitpoint.y, transform.position.z);

                velocity.y = 0;

                if (collideUpEvent != null) collideUpEvent.Invoke(hitpoint);
                return true;
            }
        }
        return false;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;

        //down
        Gizmos.DrawRay((Vector2)transform.position - new Vector2(rayBoxWidth, 0), Vector3.down * rayLengthY);
        Gizmos.DrawRay((Vector2)transform.position + new Vector2(rayBoxWidth, 0), Vector3.down * rayLengthY);

        //left
        Gizmos.DrawRay((Vector2)transform.position - new Vector2(0, rayBoxHeight), Vector3.left * rayLengthX);
        Gizmos.DrawRay((Vector2)transform.position + new Vector2(0, rayBoxHeight), Vector3.left * rayLengthX);

        //right                                                   
        Gizmos.DrawRay((Vector2)transform.position - new Vector2(0, rayBoxHeight), Vector3.right * rayLengthX);
        Gizmos.DrawRay((Vector2)transform.position + new Vector2(0, rayBoxHeight), Vector3.right * rayLengthX);

        //up
        Gizmos.DrawRay((Vector2)transform.position - new Vector2(rayBoxWidth, 0), Vector3.up * rayLengthY);
        Gizmos.DrawRay((Vector2)transform.position + new Vector2(rayBoxWidth, 0), Vector3.up * rayLengthY);
    }
}
