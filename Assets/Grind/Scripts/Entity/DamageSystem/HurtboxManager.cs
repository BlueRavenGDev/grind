﻿using UnityEngine;
using System.Collections;
using GameObjectHolders;

/// <summary>
/// Controls overall damage resistances and the like.
/// </summary>
public class HurtboxManager : MonoBehaviour
{
    public Resource healthResource;

    public GameObjectReference hitNumberManager;

    [Range(0, 1), Tooltip("How much physical damage is taken. Lower % = Higher damage.")]
    public float overallDefense = 1;
    [Range(0, 1)]
    public float overallFire = 1;
    [Range(0, 1)]
    public float overallIce = 1;
    [Range(0, 1)]
    public float overallElec = 1;
    [Range(0, 1)]
    public float overallWater = 1;

    public int CalculateElementalDamage(int elementalDamage, Resistance.DamageType elementDamageType)
    {
        switch (elementDamageType)
        {
            case Resistance.DamageType.None:
                return 0;   //take no elemental damage
            case Resistance.DamageType.Fire:
                return (int)(CalculateTotalElementalDamage(elementalDamage, overallFire));
            case Resistance.DamageType.Ice:
                return (int)(CalculateTotalElementalDamage(elementalDamage, overallIce));
            case Resistance.DamageType.Elec:
                return (int)(CalculateTotalElementalDamage(elementalDamage, overallElec));
            case Resistance.DamageType.Water:
                return (int)(CalculateTotalElementalDamage(elementalDamage, overallWater));
            default:
                return 0;
        }
    }
    
    private float CalculateTotalElementalDamage(float elementalDamage, float overallModifier)
    {//give this to another function so I don't have to keep pasting stuff
        return elementalDamage * overallModifier;
    }

    public int CalculatePhysicalDamage(int physicalDamage, float weight, float effectiveness = 1)
    {
        return (int)(physicalDamage * weight * effectiveness);
    }

    public int CalculateTotalDamage(int totalPhysicalDamage, int totalElementalDamage)
    {
        return (int)((totalPhysicalDamage + totalElementalDamage) * overallDefense);
    }
}
