﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider2D), typeof(Rigidbody2D))]
public class Hurtbox : MonoBehaviour
{
    public DamageHitType damageHitType;

    public HurtboxManager hurtboxManager;

    public bool CanTakeDamage()
    {
        return true;
    }

    public bool TakeDamage(int damage, float weight, Resistance.DamageType elementDamageType, int elementDamage)
    {
        if (!CanTakeDamage())
            return false;

        int totalPhysical = hurtboxManager.CalculatePhysicalDamage(damage, weight);
        int totalElemental = hurtboxManager.CalculateElementalDamage(elementDamage, elementDamageType);
        int totalDamage = hurtboxManager.CalculateTotalDamage(totalPhysical, totalElemental);

        hurtboxManager.healthResource.TryUseResource(totalDamage);

        hurtboxManager.hitNumberManager.GetReferencedComponent<HitNumberManager>().CreateHitNumber(totalDamage, transform.position, damageHitType);

        Debug.Log("Hitbox took " + totalDamage + " damage. (" + totalPhysical + " phys, " + totalElemental + " element)");
        return true;
    }
}
