﻿using UnityEngine;
using System.Collections;

public enum DamageHitType
{
    Friendly,
    Hostile
}

[RequireComponent(typeof(Collider2D))]//, typeof(Rigidbody2D))]
public class Hitbox : MonoBehaviour
{
    public DamageHitType damageHitType;

    public bool CanHit { get; private set; }

    public int damage;
    public int elementDamage;
    public Resistance.DamageType elementDamageType;

    public float weight = 1;
    [Tooltip("How many times this hitbox can hit during an active period.")]
    public float weightHits = 1;

    [Tooltip("Time between hits.")]
    public float weightHitsCooldown = 0.25f;

    private Coroutine weightHitsCooldownC;
    private bool weightHitsOnCooldown;

    /// <summary>
    /// Sets the hitbox able to hit hurtboxes.
    /// If the hitbox is already active, this will not run anything.
    /// Call <see cref="StopSetActive"/> before calling this.
    /// </summary>
    /// <param name="damage">The amount of raw physical damage to deal.</param>
    /// <param name="weight">The 'weight' or attack multiplier of the attack.</param>
    /// <param name="weightHits">How many times this hitbox can hit during an active period.</param>
    /// <param name="elementDamageType">The damage type of the element</param>
    /// <param name="elementDamage">The amount of raw elemental damage to deal.</param>
    public void StartSetActive(int damage, float weight, float weightHits, Resistance.DamageType elementDamageType, int elementDamage)
    {
        if (!CanHit)
        {
            CanHit = true;
            this.damage = damage;
            this.weight = weight;
            this.weightHits = weightHits;
            this.elementDamage = elementDamage;
            this.elementDamageType = elementDamageType;
        }
    }

    /// <summary>
    /// Stops the hitbox from being active. This does not cause errors if called more than once before restarting.
    /// </summary>
    public void StopSetActive()
    {
        CanHit = false;
        if (weightHitsCooldownC != null) StopCoroutine(weightHitsCooldownC);
        weightHitsOnCooldown = false;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (CanHit)
        {
            Hurtbox hurtbox = collision.GetComponent<Hurtbox>();

            if (hurtbox)
            {
                if (!weightHitsOnCooldown && HitTypeCanHit(hurtbox))
                {
                    DamageHurtbox(hurtbox);
                }
            }
        }
    }

    private void DamageHurtbox(Hurtbox hurtbox)
    {
        if (hurtbox.TakeDamage(damage, weight, elementDamageType, elementDamage))
        {
            weightHits--;

            Debug.Log("Damaged hurtbox. hits remaining: " + weightHits);

            if (weightHits <= 0)
            {
                StopSetActive();
            }
            else
            {
                weightHitsCooldownC = StartCoroutine(WeightHitsCooldown());
            }
        }
    }

    private bool HitTypeCanHit(Hurtbox hurtbox)
    {
        bool playerHittingOther = damageHitType == DamageHitType.Friendly && hurtbox.damageHitType == DamageHitType.Hostile;
        bool otherHittingPlayer = damageHitType == DamageHitType.Hostile && hurtbox.damageHitType == DamageHitType.Friendly;

        return playerHittingOther || otherHittingPlayer;
    }

    private IEnumerator WeightHitsCooldown()
    {
        weightHitsOnCooldown = true;
        yield return new WaitForSeconds(weightHitsCooldown);
        weightHitsOnCooldown = false;
    }
}
