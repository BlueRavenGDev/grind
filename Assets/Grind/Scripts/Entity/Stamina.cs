﻿using UnityEngine;
using System.Collections;
using System;

public class Stamina : MonoBehaviour
{
    public int maxStamina;

    public float regenRate;

    public int currentStamina;

    public float exhaustedDuration;
    public Action startExhaustedEvent;
    public Action stopExhaustedEvent;

    public bool exhausted = false;

    public void UseStamina(int amt)
    {
        if (currentStamina - amt < 0)
        {
            StartExhausted();
        }
    }

    private void StartExhausted()
    {
        StartCoroutine(ExhaustedCountdown());
        if (startExhaustedEvent != null) startExhaustedEvent.Invoke();
    }

    private void StopExhausted()
    {
        currentStamina = maxStamina;
        if (stopExhaustedEvent != null) stopExhaustedEvent.Invoke();
    }

    private IEnumerator ExhaustedCountdown()
    {
        exhausted = true;
        yield return new WaitForSeconds(exhaustedDuration);
        exhausted = false;

        StopExhausted();
    }

    private IEnumerator RegenStamina()
    {
        yield return new WaitForSeconds(regenRate);
        currentStamina += 1;

        if (currentStamina > maxStamina)
            currentStamina = maxStamina;

        StartCoroutine(RegenStamina());
    }
}
