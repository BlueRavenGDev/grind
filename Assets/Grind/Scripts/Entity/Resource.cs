﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

[RequireComponent(typeof(ResourcesManager))]
public class Resource : MonoBehaviour
{
    public string resourceName;

    public int max;
    [Tooltip("The maximum value that this resource will regenerate to.")]
    public int regenMax;
    [Tooltip("When ResourcesManager.ResetResources() is called, this is the value regenMax is reset to.")]
    public int regenMaxReset;
    public int min;

    public int current;

    public float currentPercent { get { return (float)((float)current / (float)max); } }

    [Tooltip("Delay between each tick.")]
    public float tickTime;

    [Tooltip("How much this resource regains per second.")]
    public int regenPerTick;

    [Tooltip("If we reach the min, disable regain for this many seconds.")]
    public float resourceExhaustedDelay;

    private bool canUseResource = true;
    [HideInInspector]
    public bool resourceExhausted;
    public bool CanUseResource { get { return canUseResource && !resourceExhausted; } }

    private bool canRegenResource = true;
    [HideInInspector]
    public bool regenDelayed;
    public bool CanRegenResource { get { return canRegenResource && !regenDelayed && !resourceExhausted; } }

    [Tooltip("Delay before regen starts after using resource.")]
    public float useRegenDelay;

    public UnityEvent resourceUsedEvent;
    public UnityEvent resourceExhaustedEvent;
    public UnityEvent resourceRegenEvent;

    private float regenStartTime;

    private Coroutine exhaustedCoroutine;
    private Coroutine delayCoroutine;

    public void IncreaseRegenMax(int regenMax)
    {
        if (regenMax > max)
            this.regenMax = max;
        else
            this.regenMax = regenMax;
    }

    public bool TryUseResource(int amt)
    {   //should there be a separate function for adding? can just use negative numbers with this...
        bool canuse = CanUseResources();

        if (!canuse)
            return false;

        bool delay = true;
        if (current - amt < min)
        {
            delay = false;
            if (exhaustedCoroutine != null)
                StopCoroutine(exhaustedCoroutine);

            exhaustedCoroutine = StartCoroutine(ExhaustedDelay());

            current = min;
        }
        else if (current - amt > max) //in case amt is negative
            current = max;
        else
        {
            current -= amt;
        }

        if (delay)
        {
            if (delayCoroutine != null)
                StopCoroutine(delayCoroutine);
            delayCoroutine = StartCoroutine(UseDelay());
        }

        if (resourceUsedEvent != null) resourceUsedEvent.Invoke();

        return true;
    }

    public bool CanUseResources()
    {
        if (CanUseResource)
        {
            return true;
        }

        return false;
    }

    private IEnumerator UseDelay()
    {
        regenDelayed = true;
        yield return new WaitForSeconds(useRegenDelay);
        regenDelayed = false;
        StartRegen();
    }

    private IEnumerator ExhaustedDelay()
    {
        if (resourceExhaustedEvent != null) resourceExhaustedEvent.Invoke();

        resourceExhausted = true;
        yield return new WaitForSeconds(resourceExhaustedDelay);
        resourceExhausted = false;
        StartRegen();
    }

    private void Update()
    {
        if (CanRegenResource)
        {
            if (Time.time > regenStartTime + tickTime)
            {
                current += regenPerTick;

                if (current > regenMax)
                    current = regenMax;

                StartRegen();

                if (resourceRegenEvent != null) resourceRegenEvent.Invoke();
            }
        }
    }

    private void StartRegen()
    {
        regenStartTime = Time.time;
    }

    private IEnumerator RegenUpdate()
    {
        float startTime = Time.time;

        while (startTime < startTime + 1)   //run this while one second has not passed
        {
            if (!resourceExhausted && !regenDelayed)
            {

                if (current >= regenMax)
                    current = regenMax;
                else
                {
                    current += regenPerTick;

                    if (resourceRegenEvent != null) resourceRegenEvent.Invoke();
                }
            }
            yield return new WaitForEndOfFrame();
        }

        StartCoroutine(RegenUpdate());
    }

    public void ResetResource()
    {
        regenMax = regenMaxReset;
        current = regenMax;
    }
}
