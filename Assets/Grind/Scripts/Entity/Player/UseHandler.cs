﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using CustomInputManager;

public class UseHandler : MonoBehaviour
{
    public string useInput = "Use";
    public Inputs input;

    private List<UseRequest> useRequests = new List<UseRequest>();

    public void RequestUse(IUseRequester useRequester)
    {
        if (input.IsInputDown(useInput))
        {
            useRequests.Add(new UseRequest(useRequester.Priority, useRequester.Use));
        }
    }

    private UseRequest FindUse()
    {
        UseRequest bestRequest = new UseRequest();
        bestRequest.priority = int.MinValue;

        foreach (UseRequest request in useRequests)
        {
            if (request.priority > bestRequest.priority)
                bestRequest = request;
        }

        return bestRequest;
    }

    private void LateUpdate()
    {
        UseRequest bestRequest = FindUse();

        if (bestRequest.action != null) bestRequest.action.Invoke();

        ResetAllRequests();
    }

    private void ResetAllRequests()
    {
        useRequests = new List<UseRequest>();
    }
}

public struct UseRequest
{
    public int priority;
    public Action action;

    public UseRequest(int priority, Action action)
    {
        this.priority = priority;
        this.action = action;
    }
}
