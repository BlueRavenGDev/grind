﻿using UnityEngine;
using System.Collections;
using GameObjectHolders;

public interface IUseRequester
{
    GameObjectReference UseHandler { get; set; }

    int Priority { get; set; }
    void Use();
}
