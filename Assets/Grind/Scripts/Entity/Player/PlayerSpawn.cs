﻿using UnityEngine;
using System.Collections;

public class PlayerSpawn : MonoBehaviour
{
    [Tooltip("If the scene is set to not spawn the player randomly, they will always spawn on this spawn.")]
    public bool isDefaultSpawn;
}
