﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public enum MotionEditMode
{
    None = 0,
    EditStartNode = 1 << 0,
    EditEndNode = 1 << 1,
    EditAllNodes = EditStartNode | EditEndNode,
    EditStartCurve = 1 << 2,
    EditEndCurve = 1 << 3,
    EditAllCurves = EditStartCurve | EditEndCurve,
    EditAll = ~0
}

public class CustomMotionController : MonoBehaviour
{
    public List<CustomMotion> motions;

    private Dictionary<string, CustomMotion> motionsLookup;

    public int idleMotionIndex;
    [ReadOnly]

    public CustomMotion playingMotion;
    public int playingMotionNodeIndex;
    public MotionNode playingMotionNode;

    public GameObject puppetObject;
    public bool startPlayingIdle;

    [Tooltip("Is a NON-IDLE node playing?")]
    public bool playing;
    public bool playingIdle;

    [Tooltip("Are we waiting to return to idle?")]
    public bool returnToIdleWaiting;

    public Action playIdleEvent;
    public Action<CustomMotion> playMotionEvent;
    public Action<CustomMotion, MotionNode> playMotionNodeEvent;

    #region editor
    public List<MotionNode> e_currentMotionNodes;

    public MotionNode e_currentSelectedNode;
    public int e_currentSelectedIndex;

    public CustomMotion e_currentMotion;

    public MotionEditMode e_currentEditMode;

    public bool e_drawAllPaths;
    public bool e_drawOtherPaths;
    public bool e_drawOldPaths;

    #endregion

    private float startTime;

    private bool initialized;

    private void Start()
    {
        SetUp();
    }

    private void SetUp()
    {
        if (!initialized)
        {
            initialized = true;

            motionsLookup = new Dictionary<string, CustomMotion>();
            foreach (CustomMotion motion in motions)
            {
                motionsLookup.Add(motion.motionName, motion);
            }

            if (startPlayingIdle)
            {
                StartPlayMotion("Idle");
            }
        }
    }

    private void Update()
    {
        if (playingMotionNode.active)
        {
            PlayWeaponNode();
        }
    }
    
    public void StartPlayMotion(string motionName, bool force = false)
    {
        SetUp();
        if (!motionsLookup.ContainsKey(motionName))
        {
            Debug.LogError("Tried to play a weapon motion that was at a non-existant key. Key: " + motionName);
            return;
        }

        if (motionName == "Idle")
        {
            StartPlayIdle(force);
        }
        else
        {
            if (!playing || returnToIdleWaiting)
            {
                playingIdle = false;
                playing = true;

                returnToIdleWaiting = false;

                startTime = Time.time;

                playingMotion = motionsLookup[motionName];
                if (playMotionEvent != null) playMotionEvent.Invoke(playingMotion);

                StartPlayMotionNode(0);
                playingMotionNode.active = true;

                e_currentSelectedNode = playingMotionNode;
            }
        }
    }

    public void StartPlayIdle(bool force = false)
    {
        SetUp();

        if (!playingIdle || force)
        {
            playing = false;
            playingIdle = true;

            startTime = Time.time;

            returnToIdleWaiting = false;

            playingMotion = motionsLookup["Idle"];
            if (playIdleEvent != null) playIdleEvent.Invoke();
            if (playMotionEvent != null) playMotionEvent.Invoke(playingMotion);

            StartPlayMotionNode(0);
            playingMotionNode.active = true;

            e_currentMotion = playingMotion;
        }
    }

    private void StartPlayMotionNode(int index)
    {
        returnToIdleWaiting = false;

        startTime = Time.time;

        playingMotionNode = playingMotion.nodes[index];
        playingMotionNodeIndex = index;
        if (playMotionNodeEvent != null) playMotionNodeEvent.Invoke(playingMotion, playingMotionNode);

        playingMotionNode.active = true;

        e_currentSelectedNode = playingMotionNode;
    }

    private void PlayWeaponNode()
    {
        playingMotionNode.active = true;

        float maxTime = Mathf.Max(playingMotionNode.delayPosition + playingMotionNode.timePosition,
            playingMotionNode.timeRotation + playingMotionNode.timeRotation,
            playingMotionNode.timePlayerMoveDirection + playingMotionNode.timePlayerMoveDirection);

        if (Time.time > startTime + maxTime)
        {
            returnToIdleWaiting = true;

            if (Time.time > startTime + maxTime + playingMotionNode.delayReturn)
            {
                playingMotionNode.active = false;

                StartPlayNextNode();
            }
            return;
        }

        float currentTime = Time.time - startTime;

        if (currentTime > playingMotionNode.delayPosition)
        {
            float delayTime = currentTime - playingMotionNode.delayPosition;
            float delayEndTime = maxTime - playingMotionNode.delayPosition;
            Vector2 position = puppetObject.transform.localPosition;
            position = CubeBezier3(playingMotionNode.startPosition, playingMotionNode.endPosition, 
                playingMotionNode.startPosition + playingMotionNode.startPositionTan, playingMotionNode.endPosition + playingMotionNode.endPositionTan, 
                delayTime / delayEndTime);
            puppetObject.transform.localPosition = position;
        }

        if (currentTime > playingMotionNode.delayRotation)
        {
            float delayTime = currentTime - playingMotionNode.delayRotation;
            float delayEndTime = maxTime - playingMotionNode.delayRotation;

            Vector3 rotation = puppetObject.transform.localEulerAngles;
            rotation = Vector3.Lerp(new Vector3(rotation.x, rotation.y, playingMotionNode.startRotation), new Vector3(rotation.x, rotation.y, playingMotionNode.endRotation), delayTime / delayEndTime);
            puppetObject.transform.localEulerAngles = rotation;
        }
    }

    private void StartPlayNextNode()
    {
        playingMotionNodeIndex++;

        if (playingMotionNodeIndex > playingMotion.nodes.Count - 1)
        {
            StartPlayMotion("Idle", true);
        }
        else
        {
            StartPlayMotionNode(playingMotionNodeIndex);
        }
    }

    public static Vector3 CubeBezier3(Vector3 start, Vector3 end, Vector3 startTan, Vector3 endTan, float t)
    {
        Vector2 startToTan = Vector2.Lerp(start, startTan, t);
        Vector2 tanToTan = Vector2.Lerp(startTan, endTan, t);
        Vector2 endToTan = Vector2.Lerp(endTan, end, t);

        Vector2 stttt = Vector2.Lerp(startToTan, tanToTan, t);  //the BEST names
        Vector2 etttt = Vector2.Lerp(tanToTan, endToTan, t);

        return Vector3.Lerp(stttt, etttt, t);
    }
}
