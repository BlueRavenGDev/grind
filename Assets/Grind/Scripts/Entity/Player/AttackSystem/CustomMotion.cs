﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CustomMotion : MonoBehaviour
{
    public string motionName;
    public List<MotionNode> nodes = new List<MotionNode>();
}

[Serializable]
public struct MotionNode
{
    [ReadOnly]
    public bool active;

    [Tooltip("Delay before we start moving to the given position.")]
    public float delayPosition;
    [Tooltip("Delay before we start rotating towards the given rotation.")]
    public float delayRotation;
    [Tooltip("Delay before we start moving the player in the given direction.")]
    public float delayPlayerMoveDirection;

    [Tooltip("The time it takes to reach the given position.")]
    public float timePosition;
    [Tooltip("The time it takes to reach the given rotation.")]
    public float timeRotation;
    [Tooltip("The amount of time the player will move in the given direction.")]
    public float timePlayerMoveDirection;

    public Vector2 endPosition;
    public float endRotation;
    public Vector2 startPositionTan;

    public Vector2 startPosition;
    public float startRotation;
    public Vector2 endPositionTan;

    public float moveDirection;
    public float moveSpeed;

    [Tooltip("Is this motion node damage dealing?")]
    public bool damageDealing;

    [Tooltip("Delay before returning back to the idle animation.")]
    public float delayReturn;
}
