﻿using UnityEngine;
using System.Collections;
using System;
using CustomInputManager;
using System.Collections.Generic;
using NodeEditorFramework;

public class WeaponController : MonoBehaviour
{
    public CustomMotionController motionController;

    public Inputs input;

    public WeaponAttackCanvas weaponAttack;
    private WeaponAttackOptionsRootNode node;

    public Equipment equipment;
    public GameObject weaponHolder;
    private bool hasWeapon;
    private Dictionary<string, WeaponMotionWeight> motionWeights;

    public PlayerController playerController;

    [ReadOnly]
    public int comboLength;

    public Hitbox hitbox;

    private void Start()
    {
        equipment.weaponUpdatedEvent += OnWeaponUpdated;
        OnWeaponUpdated(null);

        motionController.playIdleEvent += OnPlayIdleMotion;
        motionController.playMotionEvent += OnPlayMotion;
        motionController.playMotionNodeEvent += OnPlayMotionNode;

        SetIdle();
    }

    private void Update()
    {
        if (hasWeapon && playerController.Controllable)
        {
            if (motionController.returnToIdleWaiting || !motionController.playing)
            {
                TryPlayNextAttack();
            }
        }
    }

    private void OnWeaponUpdated(ItemInstance weapon)
    {
        motionWeights = new Dictionary<string, WeaponMotionWeight>();

        if (weapon && weapon.Equipped)
        {
            motionWeights = weapon.attachedItemWeapon.moveset.GetMotionWeightsDictionary();

            hasWeapon = true;
            weaponHolder.SetActive(true);
        }
        else
        {
            hasWeapon = false;
            weaponHolder.SetActive(false);
        }
    }

    /// <summary>
    /// Tries to play the next attack in the attack chain.
    /// If there is no next attack, it will instead set it to idle.
    /// </summary>
    private void TryPlayNextAttack()
    {
        WeaponLeadButtons leadButtons = GetCurrentLeadButtons();

        string motion = GetNextMotionToPlay(leadButtons);
        if (motion != "")
        {
            PlayNextAttack(motion);
        }
        else SetIdle();
    }

    private void PlayNextAttack(string motion)
    {
        motionController.StartPlayMotion(motion);
    }

    private void OnPlayMotion(CustomMotion motion)
    {
        
    }

    private void OnPlayMotionNode(CustomMotion motion, MotionNode node)
    {
        hitbox.StopSetActive();

        if (node.damageDealing)
        {
            float weight = 1;
            float hits = 1;
            if (motionWeights.ContainsKey(motion.motionName))
            {
                weight = motionWeights[motion.motionName].weight;
                hits = motionWeights[motion.motionName].weightHits;
            }

            hitbox.StartSetActive(equipment.weapon.attachedItemWeapon.damage, weight, hits, equipment.weapon.attachedItemWeapon.elementType, equipment.weapon.attachedItemWeapon.elementDamage);
        }
    }

    private string GetNextMotionToPlay(WeaponLeadButtons leadButtons)
    {
        comboLength++;

        WeaponAttackNode attackNode = weaponAttack.GetNextNode(node, leadButtons);

        if (attackNode)
        {   //we have an attack node; try to start its given motion.
            node = attackNode.GetNextOptions();
            if (!node)
                SetIdle();

            return attackNode.motion;
        }

        return "";
    }

    private void OnPlayIdleMotion()
    {
        if (!motionController.playing)
        {
            node = weaponAttack.GetRootNode();
        }
    }

    private void SetIdle()
    {
        if (!motionController.playing)
        {
            comboLength = 0;
            node = weaponAttack.GetRootNode();

            hitbox.StopSetActive();

            motionController.StartPlayIdle();
        }
    }

    private WeaponLeadButtons GetCurrentLeadButtons()
    {
        WeaponLeadButtons leadButton = WeaponLeadButtons.None;

        if (input.IsInput("AttackMod"))
        {
            leadButton = WeaponLeadButtons.AttackMod;

            if (input.IsInputDown("Attack1"))
            {
                leadButton = WeaponLeadButtons.Attack1AndAttackMod;
            }
            if (input.IsInputDown("Attack2"))
            {
                leadButton = WeaponLeadButtons.Attack2AndAttackMod;
            }
        }
        else
        {
            if (input.IsInputDown("Attack1"))
            {
                leadButton = WeaponLeadButtons.Attack1;
            }
            if (input.IsInputDown("Attack2"))
            {
                leadButton = WeaponLeadButtons.Attack2;
            }
        }

        return leadButton;
    }
}


