﻿using UnityEngine;
using System.Collections;
using CustomInputManager;
using GameObjectHolders;
using System.Collections.Generic;

/// <summary>
/// Controls the player's inputs and movement
/// </summary>
public class PlayerController : MonoBehaviour
{
    public Inputs input;

    public GameObjectReference menu;

    public bool controllable = true;
    public bool Controllable { get { return controllable && !MenuSelectorUIHandler.menuOpen; } set { controllable = value; } }

    private bool interact, run, useItem;
    private bool left, right;
    private bool leftArrow, rightArrow;

    public ResourcesManager resourcesManager;

    public GameObjectReference gameState;

    [Header("Physics")]
    public Rigidbody2D rigidbody2d;
    public bool freezeVelocity = false;

    public float moveSpeed = 1;
    public float runSpeed = 1.2f;
    public float jumpSpeed = 6;

    public float maxMoveSpeed = 12;
    public float maxRunSpeed = 20;

    public float friction = 1;

    private Vector2 velocity;

    public bool onGround = false;

    [Header("Stamina")]
    public int staminaRun;

    [Header("Inventory")]
    public Inventory inventory;

    [Header("Test")]
    public GameObjectReference hitNumberManagerHolder;

    private void Start()
    {
        BetterCollision bc = GetComponent<BetterCollision>();

        bc.collideDownEvent += CollideDown;
        bc.collideNoneEvent += CollideNothing;
    }

    private void Update()
    {
        Inputs();
    }
    
    private void Inputs()
    {
        if (input.IsInputDown("Menu"))
        {
            menu.GetReferencedComponent<MenuSelectorUIHandler>().ToggleMenuSelector();
        }

        if (Controllable)
        {
            velocity = rigidbody2d.velocity;

            left = false;
            right = false;
            interact = false;
            run = false;

            useItem = false;
            leftArrow = false;
            rightArrow = false;

            if (input.IsInput("Left"))
            {
                left = true;
            }

            if (input.IsInput("Right"))
            {
                right = true;
            }

            if (input.IsInputDown("Jump"))
            {
                if (onGround)
                {
                    velocity.y = jumpSpeed;
                }
                //gameState.GetReferencedComponent<GameState>().LoadGameFromFile();
            }

            if (input.IsInputDown("Use"))
            {
                //gameState.GetReferencedComponent<GameState>().SaveGameToFile();
                //hitNumberManagerHolder.GetReferencedComponent<HitNumberManager>().CreateHitNumber(1, transform.position);

                interact = true;
            }

            if (input.IsInputDown("UseItem"))
            {
                inventory.UseCurrentConsumable();
                useItem = true;
            }

            if (input.IsInputDown("ArrowLeft"))
            {
                inventory.ScrollCurrentConsumableLeft();
                leftArrow = true;
            }

            if (input.IsInputDown("ArrowRight"))
            {
                inventory.ScrollCurrentConsumableRight();
                rightArrow = true;
            }

            if (input.IsInput("Run") && onGround)
            {
                if (resourcesManager.GetResource("Stamina").CanUseResource)
                {
                    if (left || right)
                    {
                        resourcesManager.GetResource("Stamina").TryUseResource(staminaRun);
                        run = true;
                    }
                }
            }

            UpdateVelocity();
        }
    }

    private void FixedUpdate()
    {
        FixedInputs();
    }

    /// <summary>
    /// fixed inputs is inputs that relate to movement - wasd, jump, dash, etc.
    /// </summary>
    private void FixedInputs()
    {
        float fMaxMoveSpeed = maxMoveSpeed;

        if (Controllable)
        {
            velocity = rigidbody2d.velocity;

            float fMoveSpeed = moveSpeed;

            if (run)
            {
                fMoveSpeed = runSpeed;
                fMaxMoveSpeed = maxRunSpeed;
            }

            if (left)
            {
                velocity.x -= fMoveSpeed;
            }

            if (right)
            {
                velocity.x += fMoveSpeed;
            }

            if (input.IsInputDown("Up"))
            {   //up and down are not used for movement
                //inventory.DropNewItem(test);
            }

            if (input.IsInput("Down"))
            {

            }
        }

        velocity.x = Mathf.Clamp(velocity.x, -fMaxMoveSpeed, fMaxMoveSpeed);

        velocity.x *= friction;

        UpdateVelocity();   
    }

    private void UpdateVelocity()
    {
        if (!freezeVelocity)
            rigidbody2d.velocity = velocity;
    }
    
    private void CollideDown(Vector2 hitPoint)
    {
        onGround = true;
    }

    private void CollideNothing()
    {
        onGround = false;
    }
}
