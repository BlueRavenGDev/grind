﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class FinalNavNode : NavNode
{
    [Header("Final Node")]
    public List<NavNode> nodes;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        foreach (NavNode node in nodes.ToList())
        {
            if (node)
            {
                Vector2 midpoint = (node.transform.position + transform.position) / 2;
                Gizmos.DrawLine(transform.position, midpoint);

                Vector2 perp = ((Vector2)transform.position - midpoint);
                perp = (new Vector2(perp.y, -perp.x).normalized * 0.125f) + midpoint;

                Gizmos.DrawLine(midpoint, perp);
            }
            else connectedNodes.Remove(node);
        }
    }
}
