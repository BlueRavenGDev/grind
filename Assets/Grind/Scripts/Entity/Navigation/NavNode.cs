﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(Collider2D))]
public class NavNode : MonoBehaviour
{
    public int index;

    public List<NavNode> connectedNodes;

    public bool final;

    public FinalNavNode finalLeft;
    public NavNode leftNode;
    public NavNode rightNode;
    public FinalNavNode finalRight;

    private void Awake()
    {
        GetComponent<Collider2D>().isTrigger = true;    
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        NavController controller = collider.GetComponent<NavController>();

        if (controller)
        {
            controller.EnterNode(index);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = final ? Color.red : Color.green;

        Gizmos.DrawCube(transform.position, new Vector3(0.25f, 0.25f, 0));

        if (leftNode)
        {   //draw a line to halfway between nodes
            Vector2 midpoint = (leftNode.transform.position + transform.position) / 2;
            Gizmos.DrawLine(transform.position, midpoint);
        }
        if (rightNode)
        {   //draw a line to halfway between nodes
            Vector2 midpoint = (rightNode.transform.position + transform.position) / 2;
            Gizmos.DrawLine(transform.position, midpoint);
        }

        /*foreach (NavNode node in connectedNodes.ToList())
        {
            if (node)
            {
                Vector2 midpoint = (node.transform.position + transform.position) / 2;
                Gizmos.DrawLine(transform.position, midpoint);

                Vector2 perp = ((Vector2)transform.position - midpoint);
                perp = (new Vector2(perp.y, -perp.x).normalized * 0.125f) + midpoint;

                Gizmos.DrawLine(midpoint, perp);
            }
            else connectedNodes.Remove(node);
        }*/
    }
}
