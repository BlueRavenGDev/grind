﻿using UnityEngine;
using System.Collections;

/// <summary>
/// NavController controls what nodes to travel to.
/// It should be noted that this does not control how they travel to the node.
/// </summary>
public class NavController : MonoBehaviour
{
    [Tooltip("The node we're currently navigating to.")]
    public int navigatingNode;

    [Tooltip("The node we were last at, before we started navigating.")]
    public int previousNode;

    [Tooltip("The node we're currently at. Is the same as previousNode while navigating, " +
        "and the same as navigatingNode when we've reached the destination.")]
    public int currentNode;

    public SceneState sceneState;

    public void StartNavToNode(int nodeIndex)
    {
        previousNode = currentNode;
        navigatingNode = nodeIndex;
    }

    public void EnterNode(int nodeIndex)
    {
        if (nodeIndex == navigatingNode)
        {
            ReachedNode();
        }
    }

    private void ReachedNode()
    {
        currentNode = navigatingNode;
    }
}
