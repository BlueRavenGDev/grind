﻿using UnityEngine;
using System.Collections;
using GameObjectHolders;
using CustomInputManager;

[RequireComponent(typeof(Collider2D))]
public class OpenNpcMenu : MonoBehaviour, IUseRequester
{
    public MenuSelectorUIHandler menu;
    public string inputName = "Use";

    public GameObjectReference usePromptVisual;
    public GameObjectReference player;

    [SerializeField]
    private int priority;

    public int Priority
    {
        get { return priority; }
        set { priority = value; }
    }

    public GameObjectReference UseHandler
    {
        get { return player; }
        set { player = value; }
    }

    private bool inside;

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            usePromptVisual.GetReferencedComponent<UsePromptUIHandler>().StartPrompt(transform, inputName, Priority);
        }
    }

    private void Update()
    {
        if (inside)
        {
            player.GetReferencedComponent<UseHandler>().RequestUse(this);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            inside = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            usePromptVisual.GetReferencedComponent<UsePromptUIHandler>().StopPrompt();
            inside = false;
        }
    }

    public void Use()
    {
        menu.OpenMenuSelector();
    }

    private void OnDestroy()
    {
        usePromptVisual.GetReferencedComponent<UsePromptUIHandler>().StopPrompt();
        inside = false;
    }

    private void OnDisable()
    {
        usePromptVisual.GetReferencedComponent<UsePromptUIHandler>().StopPrompt();
        inside = false;
    }
}
