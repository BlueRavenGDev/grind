﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// inventory type soley used to hold refining materials.
/// </summary>
public class RefiningInventory : Inventory
{
    [Header("Refining")]
    public List<ItemStack> refiningMaterials;
    public bool materialsRefined = false;

    public void RefineMaterials()
    {
        if (!materialsRefined)
        {
            for (int i = 0; i < refiningMaterials.Count; i++)
            {
                if (refiningMaterials[i].attachedItem is ItemRefinable && (refiningMaterials[i].attachedItem as ItemRefinable).refinedItem)
                {
                    refiningMaterials[i] = new ItemStack()
                    {
                        attachedItem = (refiningMaterials[i].attachedItem as ItemRefinable).refinedItem,
                        stackSize = refiningMaterials[i].stackSize
                    };
                }
            }
            materialsRefined = true;
        }
    }

    public void SetItemToBeRefined(ItemInstance item)
    {
        //TransferItemToInventory(item, item.parentInventory);
        refiningMaterials.Add(item.itemStack);
        item.parentInventory.RemoveItem(item.inventoryIndex, item.stackSize);   //Since this item can be in another inventory, we call the parent inventory's remove function
    }

    public void ClaimRefinedMaterials(Inventory toInventory)
    {
        if (materialsRefined)
        {
            foreach (ItemStack stack in refiningMaterials)
            {
                if (materialsRefined)
                {
                    toInventory.AddNewItem(stack);
                }
            }

            materialsRefined = false;
            refiningMaterials = new List<ItemStack>();
        }
    }
}
