﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class Inventory : MonoBehaviour
{
    public string inventoryName;

    public enum ListType
    {
        Blacklist,
        Whitelist
    }

    public ListType listType;
    public List<string> whiteListTypes;
    public List<string> blackListTypes;
    
    public List<ItemStack> inspectorItems;

    public ItemInstance[] Items { get; private set; }

    public Dictionary<Item, List<int>> itemLookup = new Dictionary<Item, List<int>>();

    public int inventorySizeMax = 288;
    public int MaxPageCount { get { return inventorySizeMax / InventoryUIHandler.pageSize; } }

    private int emptyIndex;

    public Action<int> itemUpdatedEvent;    //event called when an item is updated in any way. 0 is updated item index.
    public Action<int> itemCreatedEvent;    //event called when an item is created. 0 is the index at which it was placed.
    public Action<int> itemRemovedEvent;    //event called when an item is removed. 0 is the index from which the item was removed.
    public Action<int, int> itemMovedEvent; //event called when an item is moved to another index. 0 is old index, 1 is new index.
    public Action<int> itemCraftedEvent;    //event called when an item is crafted. 0 is the index of the newly crafted item.

    public Action consumableChangedEvent;   //event called when the current consumable index is changed.

    public ItemLookup lookup;

    [Tooltip("When turned on, you cannot have 2 stacks of the same item in the inventory.")]
    public bool limitToOneStack;

    [Tooltip("Allows items to be stored in the Items array.")]
    public bool allowItemsInInventory = true;

    [Header("Drop")]
    public ItemDrop dropPrefab;

    [Header("Consumable Stuff")]
    [ReadOnly]
    public List<ItemInstance> consumables;
    [Tooltip("The index of the equipped consumable in the list equippedConsumable. THIS IS NOT THE INDEX OF THE ITEM; THIS IS THE INDEX OF THE INDEX OF THE ITEM.")]
    [ReadOnly]
    public int currentConsumableIndex;

    private void Awake()
    {
        Items = new ItemInstance[inventorySizeMax];
    }

    private void Start()
    {
        itemCreatedEvent += ConsumableAdded;
        itemRemovedEvent += ConsumableRemoved;

        for (int i = 0; i < inspectorItems.Count; i++)
        {
            AddNewItem(inspectorItems[i]);
        }
    }

    /// <summary>
    /// Adds a new item to the inventory.
    /// </summary>
    /// <param name="itemStack"></param>
    /// <returns>Returns the index at which this item was added. If there was overflow (there was already an item of this type in the inventory, 
    /// and adding the stacksize to it put it over the limit, thus forcing another item to be created in a different slot) it will return the newly created slot. </returns>
    public virtual int AddNewItem(ItemStack itemStack)
    {   //TODO optimize
        if (!allowItemsInInventory)
        {
            RejectItemInventoryFull(itemStack);
            return -1;
        }

        if (CheckListsDisallow(itemStack.attachedItem))
        {
            RejectItemListDisallow(itemStack);
            return -1;
        }

        if (itemLookup.ContainsKey(itemStack.attachedItem))
        {   //we have an item of this type in the inventory
            int index = itemLookup[itemStack.attachedItem].Last();

            ItemInstance itemAtIndex = Items[index];

            if (itemAtIndex.stackSize + itemStack.stackSize > itemAtIndex.attachedItem.maxStack)
            {   //we go over the max
                //get leftovers
                int leftover = (itemAtIndex.stackSize + itemStack.stackSize) - itemAtIndex.attachedItem.maxStack;

                //reject the item - the stack is full

                itemAtIndex.stackSize = itemAtIndex.attachedItem.maxStack;

                int findex = RejectItemStackFull(new ItemStack() { attachedItem = itemAtIndex.attachedItem, stackSize = leftover });
                if (itemCreatedEvent != null) itemCreatedEvent.Invoke(findex);
                if (itemUpdatedEvent != null) itemUpdatedEvent.Invoke(index);

                return findex;
            }
            else
            {   //we can fit the new itemstack under maxstack
                itemAtIndex.stackSize += itemStack.stackSize;
                Debug.Log("Added " + itemStack.stackSize + " " + itemAtIndex.attachedItem.iname + "s at index " + index + " in inventory " + inventoryName);

                if (itemCreatedEvent != null) itemCreatedEvent.Invoke(index);
                if (itemUpdatedEvent != null) itemUpdatedEvent.Invoke(index);

                return index;
            }
        }
        else
        {   //we do not have an item of this type in the inventory

            if (emptyIndex != -1)
            {
                return CreateItemInstance(itemStack);
            }
            else RejectItemInventoryFull(itemStack);
        }

        return -1;
    }

    /// <summary>
    /// Check to see if the given item type is in either <see cref="whiteListTypes"/> or <see cref="blackListTypes"/>, according to <see cref="listType"/>.
    /// </summary>
    /// <param name="item"></param>
    /// <returns>If true, the list does not allow. if false, the list does allow.</returns>
    public virtual bool CheckListsDisallow(Item item)
    {
        if (listType == ListType.Blacklist)
        {   //allow any item types but the given
            if (blackListTypes.Contains(item.GetType().Name))
                return true;
            else return false;
        }
        else if (listType == ListType.Whitelist)
        {   //allow no item types but the given
            if (whiteListTypes.Contains(item.GetType().Name))
                return false;
            else return true;
        }

        return false;
    }

    protected virtual void RejectItemListDisallow(ItemStack itemStack)
    {
        Debug.Log(GetType().Name + " rejected item. The " + listType + " does not allow items of type " + itemStack.attachedItem.GetType().Name);

        rejectedItem = itemStack;
        rejectType = InventoryRejectType.ListDisallows;
        rejected = true;
    }

    protected virtual int RejectItemStackFull(ItemStack itemStack)
    {
        Debug.Log("Rejected item, stack was full.\n" + itemStack.stackSize + " over max.");

        if (emptyIndex != -1 && !limitToOneStack)
        {
            return CreateItemInstance(itemStack);
        }
        else RejectItemSingleLimitStackFull(itemStack);    //inventory is full, reject fully

        rejectedItem = itemStack;
        rejectType = InventoryRejectType.StackFull;
        rejected = true;
        return -1;
    }

    protected virtual void RejectItemInventoryFull(ItemStack itemStack)
    {
        Debug.Log("Rejected item, inventory was full.");
        rejectedItem = itemStack;
        rejectType = InventoryRejectType.InventoryFull;
        rejected = true;
    }

    protected virtual void RejectItemSingleLimitStackFull(ItemStack itemStack)
    {
        Debug.Log("Rejected item, single limit stack was full.");
        rejectedItem = itemStack;
        rejectType = InventoryRejectType.SingleLimitStackFull;
        rejected = true;
    }

    /// <summary>
    /// Creates an item instance.
    /// </summary>
    /// <returns>Returns the inventory item index that the instance was created at.</returns>
    private int CreateItemInstance(ItemStack itemStack)
    {
        int createdSlot = emptyIndex;

        Debug.Log("Created a new item instance at " + emptyIndex + ". contains " + itemStack.stackSize + " " + itemStack.attachedItem.name + "s in inventory " + inventoryName);
        //if the item stack was full, we want to create another item instance for a new stack.
        Items[emptyIndex] = new GameObject("ItemInstance_" + itemStack.attachedItem.name).AddComponent<ItemInstance>();
        Items[emptyIndex].itemStack = itemStack;
        Items[emptyIndex].stackSize = Mathf.Max(1, Mathf.Min(itemStack.stackSize, itemStack.attachedItem.maxStack));   //use max so we can't go below 1   

        Items[emptyIndex].parentInventory = this;
        Items[emptyIndex].inventoryIndex = emptyIndex;

        Items[emptyIndex].transform.parent = transform;

        if (itemLookup.ContainsKey(itemStack.attachedItem))
            itemLookup[itemStack.attachedItem].Add(emptyIndex);
        else itemLookup.Add(itemStack.attachedItem, new List<int> { emptyIndex });

        if (itemCreatedEvent != null) itemCreatedEvent.Invoke(emptyIndex);
        if (itemUpdatedEvent != null) itemUpdatedEvent.Invoke(emptyIndex);

        FindEmptyItemslot();

        return createdSlot;
    }

    public void CheckValidItemInstance(ItemInstance item)
    {
        Debug.Log(IsValidItemInstance(item).ToString());
    }

    public bool HasSpace(ItemStack itemStack)
    {
        if (!limitToOneStack)
        {
            return emptyIndex != -1;    //the only case in which the inventory is full is when it could not find another empty index after
                                        //finding an item. It will default to in this case.
        }
        else
        {
            Debug.Log(name + " " + inventoryName);

            if (itemLookup.ContainsKey(itemStack.attachedItem))
            {   //it should be noted that this assumes there is only one item instance already in here. If there's more than one, it'll result in glitchy behavior.
                if (Items[itemLookup[itemStack.attachedItem].First()].stackSize < itemStack.attachedItem.maxStack)
                    return true;
                return false;
            }
            else return emptyIndex != -1;
        }
    }

    /// <summary>
    /// Is this item a valid instance - i.e. it exists in the inventory,
    /// its inventoryIndex matches up, and
    /// </summary>
    /// <param name="item">The item to check out</param>
    private ItemInstance.ItemInstanceInvalidType IsValidItemInstance(ItemInstance item)
    {
        if (item.attachedItem)
        {
            if (itemLookup.ContainsKey(item.attachedItem))
            {
                if (itemLookup[item.attachedItem].Count > 0)
                {
                    if (itemLookup[item.attachedItem].Contains(item.inventoryIndex))
                    {   //our indices for this item has its inventory index. It's valid.
                        if (item.stackSize <= 0)
                            return ItemInstance.ItemInstanceInvalidType.InvalidCount;
                        return ItemInstance.ItemInstanceInvalidType.Valid;
                    }
                    else
                    {
                        //our item has a valid index and exists in the inventory but its index is not in the lookup.
                        if (Items[item.inventoryIndex])
                            return ItemInstance.ItemInstanceInvalidType.IndexNotContainedInLookup;
                    }
                }
                else
                {
                    //our item has a valid index and exists in the inventory but its index is not in the lookup.
                    if (Items[item.inventoryIndex])
                        return ItemInstance.ItemInstanceInvalidType.IndexNotContainedInLookup;
                }
            }
            else
            {
                //our item has a valid index and exists in the inventory but it's not in the lookup.
                if (Items[item.inventoryIndex])
                    return ItemInstance.ItemInstanceInvalidType.NotContainedInLookup;
            }
        }
        else
        {
            return ItemInstance.ItemInstanceInvalidType.NoAttachedItem;
        }

        return ItemInstance.ItemInstanceInvalidType.Other;
    }

    #region consumables
    private void ConsumableAdded(int index)
    {
        if (index <= -1)
            return;

        if (Items[index].IsConsumable)
        {
            consumables.Add(Items[index]);

            if (consumableChangedEvent != null) consumableChangedEvent.Invoke();
        }
    }

    private void ConsumableRemoved(int index)
    {
        if (!Items[index] || (Items[index].IsConsumable && Items[index].stackSize <= 0))
        {
            consumables.Remove(Items[index]);

            if (consumableChangedEvent != null) consumableChangedEvent.Invoke();
        }
    }

    public void UseCurrentConsumable()
    {
        ItemInstance currentConsumable = consumables[currentConsumableIndex];

        currentConsumable.attachedItemConsumable.consumeAction.Consume();

        RemoveItem(currentConsumable.inventoryIndex, 1);

        if (currentConsumable.stackSize <= 0)
        {
            consumables.RemoveAt(currentConsumableIndex);
            ScrollCurrentConsumableLeft();
        }

        if (itemUpdatedEvent != null) itemUpdatedEvent.Invoke(currentConsumable.inventoryIndex);
        if (consumableChangedEvent != null) consumableChangedEvent.Invoke();
    }

    public void ScrollCurrentConsumableLeft()
    {
        currentConsumableIndex--;

        if (currentConsumableIndex < 0)
            currentConsumableIndex = consumables.Count - 1;

        if (consumableChangedEvent != null) consumableChangedEvent.Invoke();
    }

    public void ScrollCurrentConsumableRight()
    {
        currentConsumableIndex++;

        if (currentConsumableIndex > consumables.Count - 1)
            currentConsumableIndex = 0;

        if (consumableChangedEvent != null) consumableChangedEvent.Invoke();
    }
    #endregion

    #region Item Operations
    /// <summary>
    /// Transfers an item from another inventory to this one.
    /// </summary>
    public void TransferItemToInventory(ItemInstance item, Inventory oldInventory)
    {
        oldInventory.RemoveItem(item.inventoryIndex, item.stackSize);
        AddNewItem(item.itemStack);
    }

    /// <summary>
    /// Moves an item from <paramref name="startIndex"/> to <paramref name="endIndex"/>.
    /// If an item exists at the <paramref name="endIndex"/> already, the items are swapped.
    /// </summary>
    public void MoveItem(int startIndex, int endIndex)
    {
        if (Items[startIndex])
        {   //we have an item in the start index - we're not attempting to move nothing
            if (Items[endIndex])
            {   //we have an item in the end index - swap them
                //update indices in the itemLookup table, both at the start and end index.
                int startIndexIndex = itemLookup[Items[startIndex].attachedItem].IndexOf(startIndex);
                itemLookup[Items[startIndex].attachedItem][startIndexIndex] = endIndex;
                int endIndexIndex = itemLookup[Items[endIndex].attachedItem].IndexOf(endIndex);
                itemLookup[Items[endIndex].attachedItem][endIndexIndex] = startIndex;

                ItemInstance temp = Items[endIndex];
                Items[endIndex] = Items[startIndex];
                Items[endIndex].inventoryIndex = endIndex;

                Items[startIndex] = temp;
                Items[startIndex].inventoryIndex = startIndex;

                IsValidItemInstance(Items[startIndex]);
                IsValidItemInstance(Items[endIndex]);
            }
            else
            {   //no item in the end index
                //update indices in the itemLookup table, only at the start index (as there's no items at the end index)
                int startIndexIndex = itemLookup[Items[startIndex].attachedItem].IndexOf(startIndex);
                itemLookup[Items[startIndex].attachedItem][startIndexIndex] = endIndex;

                Items[endIndex] = Items[startIndex];
                Items[endIndex].inventoryIndex = endIndex;
                Items[startIndex] = null;

                IsValidItemInstance(Items[endIndex]);

                //the start index has had its item removed, so it's now empty. If it's an earlier index than our current empty index, we can set it to that.
                if (startIndex < emptyIndex)
                    emptyIndex = startIndex;
            }

            if (itemMovedEvent != null) itemMovedEvent.Invoke(startIndex, endIndex);
            if (itemUpdatedEvent != null) itemUpdatedEvent.Invoke(startIndex);
            if (itemUpdatedEvent != null) itemUpdatedEvent.Invoke(endIndex);
        }
    }

    public virtual void SellItem(ItemInstance item, int amount, Gold gold)
    {
        Debug.Log("Inventory " + inventoryName + " sold " + amount + "x item " + item.attachedItem.iname + " for " + (item.attachedItem.sellGold * amount) + " gold.");
        RemoveItem(item.inventoryIndex, amount);
        gold.GainGold(item.attachedItem.sellGold * amount);
    }

    /// <summary>
    /// Consumes the items in this inventory, as well as gold, to create an item in the given output inventory.
    /// </summary>
    public virtual bool TryCraftItem(Recipe recipe, Gold gold, Inventory outputInventory)
    {
        Debug.Log("inventory " + inventoryName + " Attempting to craft " + recipe.outputItem.attachedItem.iname + " using recipe " + recipe);

        if (recipe.costsGold)
        {
            if (!gold.CanUseGold(recipe.goldCost))
            {
                Debug.Log("Crafting failed. Player does not have enough gold. Required: " + recipe.goldCost + ", player's gold: " + gold.gold);
                return false;
            }
        }

        foreach (ItemStack recipeStack in recipe.inputItems)
        {
            int foundCount = FindItemCount(recipeStack.attachedItem);

            if (foundCount < recipeStack.stackSize)
            {   //we do not have enough of an item. return false.
                Debug.Log("Crafting failed, inventory " + inventoryName + " contains only " + foundCount + " " + recipeStack.attachedItem.iname + ", this recipe calls for " + recipeStack.stackSize + ".");
                return false;
            }
        }

        if (recipe.costsGold) gold.UseGold(recipe.goldCost);

        foreach (ItemStack recipeStack in recipe.inputItems)
        {
            RemoveItem(recipeStack);
        }

        int createdIndex = outputInventory.AddNewItem(recipe.outputItem);
        if (itemCraftedEvent != null) itemCraftedEvent.Invoke(createdIndex);

        Debug.Log("Crafting of " + recipe.outputItem.attachedItem.iname + " was sucessful.");

        return true;
    }

    public virtual void DropItem(int index, int count)
    {
        if (count > 0)
        {
            ItemDrop drop = Instantiate(dropPrefab);
            ItemStack fstack = Items[index].itemStack;
            fstack.stackSize = count;

            drop.SetItem(fstack);

            RemoveItem(index, count);
        }
    }

    /// <summary>
    /// Use this method to drop an item that does not currently exist in the inventory.
    /// </summary>
    public void DropNewItem(ItemStack itemStack, Vector3 position)
    {
        if (itemStack.stackSize > 0)
        {
            ItemDrop drop = Instantiate(dropPrefab);
            drop.SetItem(itemStack);

            drop.transform.position = position;
        }
    }

    /// <summary>
    /// Removes an item from the inventory. 
    /// </summary>
    public virtual void RemoveItem(Item item, int stackSize)
    {
        Debug.Log("Removing " + stackSize + " of " + item.iname + " from inventory.");

        List<int> indices = new List<int>();
        List<ItemInstance> foundItems = FindItems(item, out indices);

        if (foundItems != null)
        {
            foundItems = foundItems.OrderBy(x => { return x.itemStack.stackSize; }).ToList();   //order by size, smallest to largest

            int remainingStackSize = stackSize; //how many items we have left to remove
            
            //loop while we still have more to remove and we have slots to remove from.
            while (remainingStackSize > 0 && foundItems.Count > 0)
            {
                ItemInstance foundItem = foundItems[0];

                //get the count of how many we still need to remove.
                int remaining = remainingStackSize - foundItem.stackSize;

                //if it's below zero, we have NOT removed everything from one stack.
                if (remaining < 0)
                {   //we wouldn't deplete the entirety of this item stack; we have more than the required amount in the given stack.
                    //This loop will always end up running this.
                    foundItem.stackSize = foundItem.stackSize - remainingStackSize;

                    remainingStackSize = 0;

                    if (itemUpdatedEvent != null) itemUpdatedEvent.Invoke(foundItem.inventoryIndex);
                    if (itemRemovedEvent != null) itemRemovedEvent.Invoke(foundItem.inventoryIndex);

                    //Exit the while loop. We've removed the required amount.
                    break;
                }
                else
                {   //we would deplete the current item stack; we have less than the required amount in the given stack.

                    //Stack would be depleted to <= 0, which would be invalid - instead, just remove all references to it and delete the instance.
                    foundItems.RemoveAt(0);
                    int foundIndexIndex = itemLookup[item].FindIndex(x => { return x == foundItem.inventoryIndex; });
                    int foundIndex = itemLookup[item][foundIndexIndex];

                    PreRemoveAtIndex(foundIndex, foundItem);

                    Items[foundIndex] = null;

                    itemLookup[item].RemoveAt(foundIndexIndex);

                    if (itemLookup[item].Count == 0)    //we have nothing left - remove the item from the itemlookup as it's no longer in the inventory.
                    {
                        itemLookup.Remove(item);
                    }

                    if (foundItem)
                        Destroy(foundItem.gameObject);

                    remainingStackSize = remaining;

                    Debug.Assert(foundIndex >= 0);

                    if (foundIndex < emptyIndex)
                        emptyIndex = foundIndex;
                }
            }
            
        }
    }

    public void RemoveItem(ItemStack itemStack)
    {
        RemoveItem(itemStack.attachedItem, itemStack.stackSize);
    }

    /// <summary>
    /// Removes an item from a given index. If the amount stored in the given index is greater than the amount removed,
    /// That stack will be removed from the inventory. It will not attempt to look for other indices with the same item.
    /// </summary>
    /// <param name="index"></param>
    /// <param name="stackSize"></param>
    public virtual void RemoveItem(int index, int stackSize)
    {
        ItemInstance foundItem = Items[index];

        if (foundItem)
        {
            if (foundItem.stackSize > stackSize)
            {   //we have more than enough in the stack at the index to remove from. We'll have leftover; nothing more than
                //decrementing the stack size needs to be done.
                foundItem.stackSize -= stackSize;

                if (itemRemovedEvent != null) itemRemovedEvent.Invoke(foundItem.inventoryIndex);
                if (itemUpdatedEvent != null) itemUpdatedEvent.Invoke(foundItem.inventoryIndex);
            }
            else
            {   //we do NOT have enough in the given stack.
                Item item = foundItem.attachedItem;

                int foundIndexIndex = itemLookup[item].FindIndex(x => { return x == foundItem.inventoryIndex; });
                int foundIndex = itemLookup[item][foundIndexIndex];

                PreRemoveAtIndex(foundIndex, foundItem);

                Items[foundIndex] = null;

                itemLookup[item].RemoveAt(foundIndexIndex);

                if (itemLookup[item].Count == 0)    //we have nothing left - remove the item from the itemlookup as it's no longer in the inventory.
                {
                    itemLookup.Remove(item);
                }

                if (foundItem)  //if, for whatever reason, the gameobject with the instance remains, destroy it, as it's no longer useful.
                    Destroy(foundItem.gameObject);

                Debug.Assert(foundIndex >= 0, "Foundindex was less than zero. " + foundIndex);

                if (foundIndex < emptyIndex)
                    emptyIndex = foundIndex;

                if (itemRemovedEvent != null) itemRemovedEvent.Invoke(foundIndex);
                if (itemUpdatedEvent != null) itemUpdatedEvent.Invoke(foundIndex);
            }
        }
    }

    /// <summary>
    /// called before we remove an iteminstance from an index.
    /// </summary>
    protected virtual void PreRemoveAtIndex(int index, ItemInstance itemInstance)
    {

    }
    #endregion

    #region Find
    /// <summary>
    /// Finds an item instance of an item scriptableobject, if it exists. returns null if it does not.
    /// </summary>
    public ItemInstance FindItem(Item item)
    {
        if (itemLookup.ContainsKey(item))
            return Items[itemLookup[item].Last()];
        return null;
    }

    public ItemInstance FindItem(Item item, out int index)
    {
        if (itemLookup.ContainsKey(item))
        {
            index = itemLookup[item].Last();
            return Items[index];
        }

        index = -1;
        return null;
    }

    public List<ItemInstance> FindItems(Item item, out List<int> indices)
    {
        if (itemLookup.ContainsKey(item))
        {
            indices = itemLookup[item];
            List<ItemInstance> instances = new List<ItemInstance>();
            foreach (int index in indices)
            {
                instances.Add(Items[index]);
            }

            return instances;
        }

        indices = new List<int>();
        return null;
    }

    public int FindItemCount(Item item)
    {
        if (itemLookup.ContainsKey(item))
        {
            int count = 0;
            List<int> indices = itemLookup[item];

            foreach (int index in indices)
            {
                count += Items[index].stackSize;
            }

            return count;
        }

        //we there's nothing in the lookup table, so there must not be any.
        return 0;
    }

    /// <summary>
    /// Find an empty item slot, and return the index of it.
    /// This is an O(n) operation.
    /// </summary>
    /// <returns></returns>
    private int FindEmptyItemslot()
    {
        int count = 0;

        while (Items[emptyIndex] != null)
        {
            if (count > inventorySizeMax)
            {
                emptyIndex = -1;    //no empty indices
                break;
            }

            emptyIndex++;
            count++;
        }

        if (emptyIndex == -1)
            Debug.LogWarning("EmptyIndex is -1! The inventory may be full.");

        return emptyIndex;
    }
    #endregion

    public List<ItemStack> GetUniqueItemstacks()
    {
        Dictionary<Item, ItemStack> fdic = new Dictionary<Item, ItemStack>();

        foreach (ItemInstance instance in Items)
        {
            if (fdic.ContainsKey(instance.attachedItem))
            {   //our dictionary contains the key; add the stack sizes
                fdic[instance.attachedItem] = new ItemStack()
                {
                    attachedItem = instance.attachedItem,
                    stackSize = instance.stackSize + fdic[instance.attachedItem].stackSize
                };
            }
            else
            {   //our dictionary does not contain the key; add to it.
                fdic.Add(instance.attachedItem, instance.itemStack);
            }
        }

        return fdic.Values.ToList();
    }

    public void ClearItems()
    {   //TODO clear items better from inventory; clearing via creating new dictionary/array still leaves old references
        foreach (ItemInstance item in Items)
        {
            if (item)
            {
                Destroy(item.gameObject);
            }
        }

        Items = new ItemInstance[inventorySizeMax];
        itemLookup = new Dictionary<Item, List<int>>();
    }

    /// <summary>
    /// adds an item to this inventory from an unattached item instance.
    /// USE THIS FUNCTION CAREFULLY. It is supposed to be used for game saving/loading; it *will* overwrite the current items in the inventory,
    /// As well as current equipment. It will NOT save these overwritten items. They're deleted forever.
    /// </summary>
    public virtual void AddItemFromLoadedItemInstance(ItemInstance itemInstance)
    {
        Items[itemInstance.inventoryIndex] = itemInstance;
        
        if (itemLookup.ContainsKey(itemInstance.attachedItem))
        {
            //attach the item index
            itemLookup[itemInstance.attachedItem].Add(itemInstance.inventoryIndex);
        }
        else
        {
            itemLookup.Add(itemInstance.attachedItem, new List<int> { itemInstance.inventoryIndex });
        }

        if (itemInstance.transform.parent != transform)
            itemInstance.transform.parent = transform;
    }

    public List<SerializableItemInstance> ItemsToSerializableForm()
    {
        List<SerializableItemInstance> ser = new List<SerializableItemInstance>();
        
        foreach (ItemInstance item in Items)
        {   //thankfully, we don't need to care about the item's index, as that's already set inside it.
            if (item)
                ser.Add(item.ToSerializable());
        }

        return ser;
    }

    public void AddSerializedItems(List<SerializableItemInstance> serItems)
    {
        ClearItems();

        foreach (SerializableItemInstance serItem in serItems)
        {
            ItemInstance newInstance = serItem.ToItemInstance(lookup, transform);
            AddItemFromLoadedItemInstance(newInstance);
        }
    }

    #region Reject
    public enum InventoryRejectType
    {
        NotRejected,
        StackFull,
        SingleLimitStackFull,
        InventoryFull,
        ListDisallows
    }

    private bool rejected;
    [HideInInspector]
    public InventoryRejectType rejectType;
    [HideInInspector]
    public ItemStack rejectedItem;

    public void StartRejectCheck()
    {
        rejected = false;
        rejectType = InventoryRejectType.NotRejected;
        rejectedItem = new ItemStack();
    }

    public bool EndRejectCheck()
    {
        return rejected;
    }
    #endregion
}
