﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class InventoryManager : MonoBehaviour
{
    private Dictionary<string, Inventory> inventories;

    private void Awake()
    {
        inventories = new Dictionary<string, Inventory>();

        List<Inventory> i = GetComponents<Inventory>().ToList();
        foreach (Inventory inv in i)
        {
            inventories.Add(inv.inventoryName, inv);
        }
    }

    public Inventory GetInventory(string name)
    {
        return inventories.ContainsKey(name) ? inventories[name] : null;
    }

    public bool TryJointCraftItem(Recipe recipe, Gold gold, Inventory outputInventory)
    {
        if (recipe.costsGold)
        {
            if (!gold.CanUseGold(recipe.goldCost))
            {
                Debug.Log("Crafting failed. Player does not have enough gold. Required: " + recipe.goldCost + ", player's gold: " + gold.gold);
                return false;
            }
        }

        foreach (Inventory inventory in inventories.Values)
        {
            if (inventory.TryCraftItem(recipe, gold, outputInventory))
            {   //first, check to see if any one inventory can craft the item with its items alone.
                return true;
            }
        }

        List<Inventory> foundInventories = new List<Inventory>();

        foreach (ItemStack recipeStack in recipe.inputItems)
        {
            bool found = false;

            foreach (Inventory inventory in inventories.Values)
            {
                int foundCount = inventory.FindItemCount(recipeStack.attachedItem);

                if (foundCount >= recipeStack.stackSize)
                {
                    foundInventories.Add(inventory);
                    found = true;
                }
            }

            if (!found)
            {
                Debug.Log("Crafting failed, could not find any or enough of " + recipeStack.attachedItem.iname + ", this recipe calls for " + recipeStack.stackSize + ".");
                return false;
            }
        }

        //we get this far, we can craft the item.

        if (recipe.costsGold) gold.UseGold(recipe.goldCost);

        for (int i = 0; i < recipe.inputItems.Count; i++)
        {
            ItemStack recipeStack = recipe.inputItems[i];

            foundInventories[i].RemoveItem(recipeStack);
        }

        return true;
    }
}
