﻿using UnityEngine;
using System.Collections;
using System;

public class EquipmentInventory : Inventory
{
    [Header("Equipment")]
    [Tooltip("Is this the equipment currently held on the player? If so, then we can only store the currently equipped items.")]
    public bool isHeldEquipment;

    public Equipment equipment;

    public Action weaponUpdatedEvent;       //event called when a weapon is equipped or unequipped.
    public Action armorUpdatedEvent;        //event called when a piece of armor is equipped or unequipped.
    public Action equipmentUpdatedEvent;    //event called when an item is equipped or unequipped.
    
    public override void DropItem(int index, int count)
    {
        base.DropItem(index, count);

        if (count > 0)
        {
            if (Items[index].Equipped)
            {
                Items[index].Equipped = false;
                if (equipmentUpdatedEvent != null) equipmentUpdatedEvent.Invoke();
            }
        }
    }

    protected override void PreRemoveAtIndex(int index, ItemInstance itemInstance)
    {
        base.PreRemoveAtIndex(index, itemInstance);

        if (itemInstance)
        {
            if (itemInstance.IsEquippable && itemInstance.Equipped)
            {
                if (itemInstance.attachedItem.maxStack > 1)
                    Debug.LogWarning("Found an equippable item with a maxstack larger than one. THIS WILL RESULT IN ISSUES! CHANGE IT TO ONE MAX.");

                equipment.Unequip(this, index);
            }
        }
    }

    public override void AddItemFromLoadedItemInstance(ItemInstance itemInstance)
    {
        base.AddItemFromLoadedItemInstance(itemInstance);

        if (itemInstance.IsArmor && itemInstance.Equipped)
            equipment.EquipArmor(itemInstance);
        if (itemInstance.IsWeapon && itemInstance.Equipped)
            equipment.EquipWeapon(itemInstance);
    }
}
