﻿using UnityEngine;
using System.Collections;
using System;

public class Equipment : MonoBehaviour
{
    public enum EquipmentType
    {
        Weapon,
        Armor
    }
    #region Equipped Instances
    public ItemInstance weapon;
    public ItemInstance armorHead;
    public ItemInstance armorArms;
    public ItemInstance armorChest;
    public ItemInstance armorLegs;
    public ItemInstance armorFeet;
    #endregion

    public Action<ItemInstance>  weaponUpdatedEvent;        //event called when a weapon is equipped or unequipped.
    public Action<ItemInstance> armorUpdatedEvent;          //event called when a piece of armor is equipped or unequipped.
    public Action<ItemInstance> equipmentUpdatedEvent;      //event called when an item is equipped or unequipped.

    public ItemArmor GetEquippedOfType(ItemArmor.ArmorType type)
    {
        try
        {
            switch (type)
            {
                case ItemArmor.ArmorType.Head:
                    return armorHead.attachedItemArmor;
                case ItemArmor.ArmorType.Chest:
                    return armorChest.attachedItemArmor;
                case ItemArmor.ArmorType.Arms:
                    return armorArms.attachedItemArmor;
                case ItemArmor.ArmorType.Legs:
                    return armorLegs.attachedItemArmor;
                case ItemArmor.ArmorType.Feet:
                    return armorFeet.attachedItemArmor;
                default:
                    return null;
            }
        }
        catch
        {   //hate me.
            return null;
        }
    }

    /// <summary>
    /// Add an item in the inventory to an armor slot.
    /// </summary>
    /// <returns>The armor type that was equipped.</returns>
    public ItemArmor.ArmorType EquipArmor(ItemInstance armor)
    {
        switch (armor.ArmorType)
        {
            case ItemArmor.ArmorType.Head:
                {
                    if (armorHead)
                        armorHead.Equipped = false;
                    armorHead = armor;
                    armorHead.Equipped = true;

                    if (armorUpdatedEvent != null) armorUpdatedEvent.Invoke(armor);
                    if (equipmentUpdatedEvent != null) equipmentUpdatedEvent.Invoke(armor);

                    return ItemArmor.ArmorType.Head;
                }
            case ItemArmor.ArmorType.Arms:
                {
                    if (armorArms)
                        armorArms.Equipped = false;
                    armorArms = armor;
                    armorArms.Equipped = true;

                    if (armorUpdatedEvent != null) armorUpdatedEvent.Invoke(armor);
                    if (equipmentUpdatedEvent != null) equipmentUpdatedEvent.Invoke(armor);

                    return ItemArmor.ArmorType.Arms;
                }
            case ItemArmor.ArmorType.Chest:
                {
                    if (armorChest)
                        armorChest.Equipped = false;
                    armorChest = armor;
                    armorChest.Equipped = true;

                    if (armorUpdatedEvent != null) armorUpdatedEvent.Invoke(armor);
                    if (equipmentUpdatedEvent != null) equipmentUpdatedEvent.Invoke(armor);

                    return ItemArmor.ArmorType.Chest;
                }
            case ItemArmor.ArmorType.Legs:
                {
                    if (armorLegs)
                        armorLegs.Equipped = false;
                    armorLegs = armor;
                    armorLegs.Equipped = true;

                    if (armorUpdatedEvent != null) armorUpdatedEvent.Invoke(armor);
                    if (equipmentUpdatedEvent != null) equipmentUpdatedEvent.Invoke(armor);

                    return ItemArmor.ArmorType.Legs;
                }
            case ItemArmor.ArmorType.Feet:
                {
                    if (armorFeet)
                        armorFeet.Equipped = false;
                    armorFeet = armor;
                    armorFeet.Equipped = true;

                    if (armorUpdatedEvent != null) armorUpdatedEvent.Invoke(armor);
                    if (equipmentUpdatedEvent != null) equipmentUpdatedEvent.Invoke(armor);

                    return ItemArmor.ArmorType.Feet;
                }
            default:
                {
                    return ItemArmor.ArmorType.NONE;
                }
        }
    }

    public void EquipWeapon(ItemInstance weapon)
    {
        if (weapon)
        {
            if (this.weapon)
                this.weapon.Equipped = false;
            this.weapon = weapon;
            weapon.Equipped = true;

            if (weaponUpdatedEvent != null) weaponUpdatedEvent.Invoke(weapon);
            if (equipmentUpdatedEvent != null) equipmentUpdatedEvent.Invoke(weapon);
        }
    }

    public void Unequip(Inventory inventory, int index)
    {
        if (inventory.Items[index] && inventory.Items[index].attachedItem && inventory.Items[index].Equipped)
        {
            ItemInstance item = inventory.Items[index];

            if (item.IsArmor)
            {
                UnequipArmorOfType(item.attachedItemArmor.armorType);

                if (armorUpdatedEvent != null) armorUpdatedEvent.Invoke(item);
                if (equipmentUpdatedEvent != null) equipmentUpdatedEvent.Invoke(item);
            }
            else if (item.IsWeapon)
            {
                weapon.Equipped = false;
                weapon = null;

                if (weaponUpdatedEvent != null) weaponUpdatedEvent.Invoke(item);
                if (equipmentUpdatedEvent != null) equipmentUpdatedEvent.Invoke(item);
            }
            else Debug.Log("Encountered an equippable object that is neither an armor or weapon. PANIC");
        }
        else
        {
            Debug.Log("Tried to unequip an item that is not equippable.");
        }
    }

    private void UnequipArmorOfType(ItemArmor.ArmorType type)
    {
        switch (type)
        {
            case ItemArmor.ArmorType.Head:
                armorHead.Equipped = false;
                armorHead = null;
                break;
            case ItemArmor.ArmorType.Chest:
                armorChest.Equipped = false;
                armorChest = null;
                break;
            case ItemArmor.ArmorType.Arms:
                armorArms.Equipped = false;
                armorArms = null;
                break;
            case ItemArmor.ArmorType.Legs:
                armorLegs.Equipped = false;
                armorLegs = null;
                break;
            case ItemArmor.ArmorType.Feet:
                armorFeet.Equipped = false;
                armorFeet = null;
                break;
        }
    }

    public int SumDefense()
    {
        int sum = 0;

        if (armorHead)
            sum += armorHead.attachedItemArmor.defense;
        if (armorArms)
            sum += armorArms.attachedItemArmor.defense;
        if (armorChest)
            sum += armorChest.attachedItemArmor.defense;
        if (armorLegs)
            sum += armorLegs.attachedItemArmor.defense;
        if (armorFeet)
            sum += armorFeet.attachedItemArmor.defense;

        return sum;
    }

    public int SumDefenseOfType(Resistance.DamageType type)
    {
        switch (type)
        {
            case Resistance.DamageType.Fire:
                {
                    int sum = 0;
                    if (armorHead)
                        sum += armorHead.attachedItemArmor.resistFire;
                    if (armorChest)
                        sum += armorChest.attachedItemArmor.resistFire;
                    if (armorArms)
                        sum += armorArms.attachedItemArmor.resistFire;
                    if (armorLegs)
                        sum += armorLegs.attachedItemArmor.resistFire;
                    if (armorFeet)
                        sum += armorFeet.attachedItemArmor.resistFire;

                    return sum;
                }
            case Resistance.DamageType.Ice:
                {
                    int sum = 0;
                    if (armorHead)
                        sum += armorHead.attachedItemArmor.resistIce;
                    if (armorChest)
                        sum += armorChest.attachedItemArmor.resistIce;
                    if (armorArms)
                        sum += armorArms.attachedItemArmor.resistIce;
                    if (armorLegs)
                        sum += armorLegs.attachedItemArmor.resistIce;
                    if (armorFeet)
                        sum += armorFeet.attachedItemArmor.resistIce;

                    return sum;
                }
            case Resistance.DamageType.Elec:
                {
                    int sum = 0;
                    if (armorHead)
                        sum += armorHead.attachedItemArmor.resistElec;
                    if (armorChest)
                        sum += armorChest.attachedItemArmor.resistElec;
                    if (armorArms)
                        sum += armorArms.attachedItemArmor.resistElec;
                    if (armorLegs)
                        sum += armorLegs.attachedItemArmor.resistElec;
                    if (armorFeet)
                        sum += armorFeet.attachedItemArmor.resistElec;

                    return sum;
                }
            case Resistance.DamageType.Water:
                {
                    int sum = 0;
                    if (armorHead)
                        sum += armorHead.attachedItemArmor.resistWater;
                    if (armorChest)
                        sum += armorChest.attachedItemArmor.resistWater;
                    if (armorArms)
                        sum += armorArms.attachedItemArmor.resistWater;
                    if (armorLegs)
                        sum += armorLegs.attachedItemArmor.resistWater;
                    if (armorFeet)
                        sum += armorFeet.attachedItemArmor.resistWater;

                    return sum;
                }
            default: return 0;
        }
    }
}
