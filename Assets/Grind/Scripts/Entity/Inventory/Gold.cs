﻿using UnityEngine;
using System.Collections;
using System;

public class Gold : MonoBehaviour
{
    public int gold;
    public int maxGold = 999999999;

    public Action<int> goldChangedEvent;

    public bool CanUseGold(int cost)
    {
        return gold - cost >= 0;
    }

    public void UseGold(int cost)
    {
        gold -= cost;

        if (gold < 0)
            gold = 0;
        if (goldChangedEvent != null) goldChangedEvent.Invoke(gold);
    }

    public void GainGold(int amount)
    {
        gold += amount;

        if (gold > maxGold)
            gold = maxGold;
        if (goldChangedEvent != null) goldChangedEvent.Invoke(gold);
    }
}
