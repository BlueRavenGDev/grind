﻿using UnityEngine;

public class Resistance : MonoBehaviour
{
    public enum DamageType
    {
        None = 0,
        Fire = 1,
        Ice = 2,
        Elec = 4,
        Water = 8
    }

    public Equipment equipment;

    private void Awake()
    {
        equipment = GetComponent<Equipment>();
    }

    public int CalculateHealthOffResistances(int damage, float weight, DamageType dtype)
    {
        float fdamage = damage;

        ApplyDefense(ref fdamage, weight);
        ApplyDamageType(ref fdamage, dtype);

        return (int)fdamage;
    }

    private void ApplyDefense(ref float damage, float weight)
    {
        damage = (int)((damage - (equipment.SumDefense() * 0.25)) * weight);
    }

    private void ApplyDamageType(ref float damage, DamageType dtype)
    {
        damage = (int)(damage - (0.65 * equipment.SumDefenseOfType(dtype)));

        DoDamageTypeAdditionalEffects(dtype);
    }

    private void DoDamageTypeAdditionalEffects(DamageType dtype)
    {

    }
}