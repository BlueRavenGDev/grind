﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ArmorItem", menuName = "Grind/Items/New Armor Item", order = 202)]
public class ItemArmor : Item
{
    public enum ArmorType
    {   //slot types aren't something that needs to be added or changed very much, so don't make this a scriptableobject
        Head,
        Arms,
        Chest,
        Legs,
        Feet,
        NONE
    }

    [Header("Armor")]
    public int defense;
    public int resistFire;
    public int resistIce;
    public int resistElec;
    public int resistWater;

    public ArmorType armorType;

    public List<Recipe> upgradeRecipes;
}