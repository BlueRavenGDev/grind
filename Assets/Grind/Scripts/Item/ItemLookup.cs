﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "ItemLookup", menuName = "Grind/Items/Item Lookup")]
public class ItemLookup : ScriptableObject
{
    public List<Item> items;

    public Dictionary<string, Item> lookupTable;

    private void OnEnable()
    {
        ResetTable();
    }

    /// <summary>
    /// clears the table of items and adds everything from the list of items.
    /// </summary>
    public void ResetTable()
    {
        lookupTable = new Dictionary<string, Item>();

        foreach (Item item in items)
        {
            if (item)
            {
                if (!lookupTable.ContainsKey(item.name))
                {
                    lookupTable.Add(item.name, item);
                }
            }
        }
    }

    public void ResetList()
    {
        items = new List<Item>();
        ResetTable();
    }
}
