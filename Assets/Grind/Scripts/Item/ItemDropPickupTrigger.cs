﻿using UnityEngine;
using System.Collections;
using CustomInputManager;
using GameObjectHolders;

public class ItemDropPickupTrigger : MonoBehaviour, IUseRequester
{
    public string useInput = "Use";

    public ItemDrop itemDrop;
    public GameObjectReference usePromptVisual;
    public GameObjectReference player;

    [SerializeField]
    private int priority;

    public int Priority
    {
        get { return priority; }
        set { priority = value; }
    }

    private bool inside;

    public GameObjectReference UseHandler
    {
        get { return player; }
        set { player = value; }
    }

    private bool delayReuse;

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            usePromptVisual.GetReferencedComponent<UsePromptUIHandler>().StartPrompt(transform, useInput, Priority);
        }
    }

    private void Update()
    {
        if (inside && !delayReuse)
            player.GetReferencedComponent<UseHandler>().RequestUse(this);
    }

    public void Use()
    {
        itemDrop.PickUp(player.GetReferencedComponent<Inventory>());
        StartCoroutine(DelayReuse());
    }

    /// <summary>
    /// In the case that an item gets rejected, the dropped item will stay on the ground instead of being destroyed.
    /// This can lead to situations where you have a ton of items on the floor but one item is being rejected - 
    /// and due to the use priority system, this prevents us from picking up any other items on the ground.
    /// So we can get around this issue by simply preventing the offending item from being able to request a use for several seconds.
    /// </summary>
    /// <returns></returns>
    private IEnumerator DelayReuse()
    {
        delayReuse = true;
        yield return new WaitForSeconds(8);
        delayReuse = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            inside = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            usePromptVisual.GetReferencedComponent<UsePromptUIHandler>().StopPrompt();
            inside = false;
        }
    }

    private void OnDestroy()
    {
        usePromptVisual.GetReferencedComponent<UsePromptUIHandler>().StopPrompt();
        inside = false;
    }

    private void OnDisable()
    {
        usePromptVisual.GetReferencedComponent<UsePromptUIHandler>().StopPrompt();
        inside = false;
    }
}
