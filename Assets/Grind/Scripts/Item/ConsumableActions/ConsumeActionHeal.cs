﻿using UnityEngine;
using System.Collections;
using GameObjectHolders;

[CreateAssetMenu(fileName = "ConsumeActionHeal", menuName = "Grind/Consume Actions/Consume Action Heal", order = 20)]
public class ConsumeActionHeal : ConsumeAction
{
    public GameObjectReference player;

    public int healAmt;
    public int increaseMaxBy;

    public override void Consume()
    {
        Resource health = player.GetReferencedComponent<ResourcesManager>().GetResource("Health");
        health.IncreaseRegenMax(health.regenMax + increaseMaxBy);
        health.TryUseResource(-healAmt);
    }
}
