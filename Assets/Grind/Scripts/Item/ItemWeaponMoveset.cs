﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WeaponItem", menuName = "Grind/Items/New Weapon Moveset", order = 202)]
public class ItemWeaponMoveset : ScriptableObject
{
    public ItemWeapon.WeaponType weaponType;

    public List<WeaponMotionWeight> motionWeights;

    public Dictionary<string, WeaponMotionWeight> GetMotionWeightsDictionary()
    {
        Dictionary<string, WeaponMotionWeight> dict = new Dictionary<string, WeaponMotionWeight>();

        foreach (WeaponMotionWeight motionWeight in motionWeights)
        {
            if (!dict.ContainsKey(motionWeight.motion))
            {
                dict.Add(motionWeight.motion, motionWeight);
            }
        }

        return dict;
    }
}

[Serializable]
public struct WeaponMotionWeight
{
    public string motion;
    public float weight;
    public int weightHits;
}