﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "Grind/Items/New Generic Item", order = 202)]
public class Item : ScriptableObject
{
    public Sprite icon;
    [ColorUsage(false)]
    public Color spriteColor = Color.white;
    [Tooltip("If true, instead of using spriteColor to determine what color the sprite is, it will instead use the color of the rarity." +
        "See the static function GetRarityColor in Item.cs for these colors.")]
    public bool colorOffRarity;

    public int rarity;

    public string iname;
    [TextArea]
    public string description;

    public bool canSell = true;
    public int sellGold;

    public int maxStack = 99;

    public static Color GetRarityColor(int rarity)
    {   //rarity is 1-10
        switch (rarity)
        {
            case 1:
                return Color.white;
            case 2:
                return new Color32(248, 223, 190, 255);
            case 3:
                return new Color32(248, 255, 133, 255);
            case 4:
                return new Color32(184, 255, 132, 255);
            case 5:
                return new Color32(118, 184, 126, 255);
            case 6:
                return new Color32(108, 208, 155, 255);
            case 7:
                return new Color32(100, 173, 189, 255);
            case 8:
                return new Color32(104, 136, 215, 255);
            case 9:
                return new Color32(115, 85, 210, 255);
            case 10:
                return new Color32(173, 68, 204, 255);
            default:
                return Color.white;
        }
    }
}
