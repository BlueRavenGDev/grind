﻿using UnityEngine;

[CreateAssetMenu(fileName = "RefinableItem", menuName = "Grind/Items/New Refinable Item", order = 202)]
public class ItemRefinable : Item
{
    [Tooltip("When this item is refined, it turns into this.")]
    public Item refinedItem;
}