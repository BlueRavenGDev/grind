﻿using UnityEngine;
using System.Collections;
using GameObjectHolders;
using RangeDrawers;

public class CollectionPoint : MonoBehaviour
{
    public DropTable dropTable;

    public GameObjectReference player;
    private Inventory inventory;

    [IntRange(1, 90)]
    public IntRange collectionAmount = new IntRange(3, 6);

    [ReadOnly]
    public int fcollectionAmount;

    private void Start()
    {
        fcollectionAmount = collectionAmount;
        inventory = player.GetReferencedComponent<InventoryManager>().GetInventory("Items");
    }

    public void GetDroppedItem()
    {
        fcollectionAmount--;

        if (fcollectionAmount <= 0)
            Destroy(transform.parent.gameObject);
        inventory.DropNewItem(dropTable.GetDroppedItem(), transform.position);
    }
}
