﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using RangeDrawers;

[CreateAssetMenu(fileName = "DropTable", menuName = "Grind/Drop Table", order = 202)]
public class DropTable : ScriptableObject
{
    public int test;

    [Tooltip("The amount of items to drop. It's recommended to keep this at one.")]
    [IntRange(0, 100)]
    public IntRange numDrops = new IntRange(1, 1);

    [HideInInspector]
    [IntRange(0, 100)]
    public IntRange dropValue = new IntRange(0, 100);

    public List<DropChance> drops;

    public bool HasOverlaps()
    {
        return false;
    }

    public ItemStack GetDroppedItem()
    {
        int value = dropValue;

        foreach (DropChance drop in drops)
        {
            if (drop.dropChance.ValueLiesInRange(value))
            {   //drop this item
                return drop.droppedItem;
            }
        }

        ItemStack emptyStack = new ItemStack();
        return emptyStack;
    }
}

[Serializable]
public struct DropChance
{
    [IntRange(0, 100)]
    public IntRange dropChance;

    public RandomItemStack droppedItem;
}

[Serializable]
public struct RandomItemStack
{
    public Item attachedItem;

    [IntRange(0, 100)]
    public IntRange stackSize;

    public ItemStack ToItemStack()
    {
        return new ItemStack() { attachedItem = this.attachedItem, stackSize = this.stackSize };
    }

    public static implicit operator ItemStack(RandomItemStack randStack)
    {
        return randStack.ToItemStack();
    }
}

