﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ItemInstance : MonoBehaviour
{
    /// <summary>
    /// InvalidIndex: The inventoryIndex of this item instance does not contain this item.<para/>
    /// NotContainedInLookup: The index of this item is valid, but it is not contained in the lookup.<para/>
    /// IndexNotContainedInLookup: the index of this item is valid, but its index is not contained in the lookup.<para/>
    /// NoAttachedItem: itemStack.attachedItem is null.<para/>
    /// InvalidCount: the stackSize is less than or equal to zero, which should not ever be true.<para/>
    /// Other: something else I didn't think of <para/>
    /// </summary>
    public enum ItemInstanceInvalidType
    {
        Valid,
        InvalidIndex,       //the inventoryIndex of this item instance does not contain this item.
        NotContainedInLookup,   //the index of this item is valid, but it is not contained in the lookup.
        IndexNotContainedInLookup,
        NoAttachedItem,
        InvalidCount,
        Other
    }

    public ItemStack itemStack;

    public Item attachedItem { get { return itemStack.attachedItem; } set { itemStack.attachedItem = value; } }
    public int stackSize { get { return itemStack.stackSize; } set { itemStack.stackSize = value; } }

    [Tooltip("The inventory this item belongs to.")]
    public Inventory parentInventory;
    public int inventoryIndex;

    #region Armor stuff
    public ItemArmor attachedItemArmor { get { return IsArmor ? attachedItem as ItemArmor : null; } }

    public bool IsArmor { get { return itemStack.attachedItem is ItemArmor; } }
    
    public ItemArmor.ArmorType ArmorType { get { return IsArmor ? attachedItemArmor.armorType : ItemArmor.ArmorType.NONE; } }
    #endregion

    #region Weapon stuff
    public ItemWeapon attachedItemWeapon { get { return IsWeapon ? attachedItem as ItemWeapon : null; } }

    public bool IsWeapon { get { return itemStack.attachedItem is ItemWeapon; } }
    #endregion

    #region Equippable stuff - Armor/Weapons
    public bool IsEquippable { get { return IsWeapon || IsArmor; } }
    private bool equipped;
    public bool Equipped { get { return IsEquippable ? equipped : false; } set { equipped = (IsEquippable ? value : false); } }
    #endregion

    #region Consumable
    public bool IsConsumable { get { return itemStack.attachedItem is ItemConsumable; } }
    public ItemConsumable attachedItemConsumable { get { return IsConsumable ? attachedItem as ItemConsumable : null; } }
    #endregion

    public bool IsUpgradable
    {
        get
        {
            if (IsEquippable)
            {
                if (IsArmor)
                    if (attachedItemArmor.upgradeRecipes.Count > 0)
                        return true;
                if (IsWeapon)
                    if (attachedItemWeapon.upgradeRecipes.Count > 0)
                        return true;
            }

            return false;
        }
    }

    public SerializableItemInstance ToSerializable()
    {
        return new SerializableItemInstance(this);
    }
}

[Serializable]
public struct ItemStack
{
    public Item attachedItem;
    public int stackSize;
}

[Serializable]
public struct SerializableItemInstance
{
    public bool valid;

    public string attachedItemName;

    public int stackSize;
    public int inventoryIndex;
    public bool equipped;

    public SerializableItemInstance(string attachedItemName, int stackSize, int inventoryIndex, bool equipped)
    {
        valid = true;
        this.attachedItemName = attachedItemName;
        this.stackSize = stackSize;
        this.inventoryIndex = inventoryIndex;
        this.equipped = equipped;
    }

    public SerializableItemInstance(ItemInstance instance)
    {
        valid = true;
        attachedItemName = instance.attachedItem.name;
        stackSize = instance.stackSize;
        inventoryIndex = instance.inventoryIndex;
        equipped = instance.Equipped;
    }

    public ItemInstance ToItemInstance(ItemLookup itemLookup, Transform parent)
    {
        ItemInstance instance = new GameObject().AddComponent<ItemInstance>();

        if (itemLookup.lookupTable.ContainsKey(attachedItemName))
        {
            instance.itemStack = new ItemStack()
            {
                attachedItem = itemLookup.lookupTable[attachedItemName],
                stackSize = stackSize
            };

            instance.inventoryIndex = inventoryIndex;
            instance.Equipped = equipped;

            instance.name = "ItemInstance_" + instance.attachedItem.name;
            instance.transform.SetParent(parent);
            return instance;
        }

        return null;
    }
}
