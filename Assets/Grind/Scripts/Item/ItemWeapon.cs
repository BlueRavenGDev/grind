﻿using UnityEngine;
using System.Collections.Generic;
using System;

[CreateAssetMenu(fileName = "WeaponItem", menuName = "Grind/Items/New Weapon Item", order = 202)]
public class ItemWeapon : Item
{
    public enum WeaponType
    {
        Sword
    }

    [Header("Weapon")]
    public int damage;

    public Resistance.DamageType elementType;

    public int elementDamage;

    public List<Recipe> upgradeRecipes;

    public WeaponType weaponType;

    public ItemWeaponMoveset moveset;
}
