﻿using UnityEngine;
using System.Collections;
using GameObjectHolders;

[CreateAssetMenu(fileName = "ConsumableItem", menuName = "Grind/Items/New Consumable Item", order = 202)]
public class ItemConsumable : Item
{
    public ConsumeAction consumeAction;
}
