﻿using UnityEngine;
using System.Collections;

public class ItemDrop : MonoBehaviour
{
    public SpriteRenderer glowPillar;
    public ParticleSystem particles;

    public GameObject visual;   //visual is disabled until item is set

    public Rigidbody2D rigidbody2d;

    private ItemStack item;

    private void Awake()
    {
        visual.gameObject.SetActive(false);
    }

    private void FixedUpdate()
    {
        Vector2 velocity = rigidbody2d.velocity;
        velocity.x *= 0.90f;
        rigidbody2d.velocity = velocity;
    }

    public void SetItem(ItemStack item)
    {
        name = "ItemDrop_" + item.attachedItem.iname;

        visual.gameObject.SetActive(true);

        this.item = item;

        glowPillar.color = Item.GetRarityColor(item.attachedItem.rarity);

        rigidbody2d.velocity = new Vector2(Random.Range(-4, 4), Random.Range(0, 12));
    }

    public void PickUp(Inventory inventory)
    {
        inventory.StartRejectCheck();
        inventory.AddNewItem(item);
        if (inventory.EndRejectCheck())
        {   //our item has been rejected for one reason or another.
            item = inventory.rejectedItem;  //simply set our item to the rejected item, so we gain its stack size and such
        }
        else
        {
            visual.gameObject.SetActive(false);

            particles.Stop(false, ParticleSystemStopBehavior.StopEmitting);

            Destroy(gameObject);//, particles.GetComponent<ParticleSystem>().main.duration);
        }
    }
}
