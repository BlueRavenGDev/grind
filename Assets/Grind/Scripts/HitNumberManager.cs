﻿using UnityEngine;
using System.Collections;

public class HitNumberManager : MonoBehaviour
{
    public HitNumberUIHandler hitNumberPrefab;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="number"></param>
    /// <param name="position"></param>
    /// <param name="hitType">The type that the reciever is. Hostile = a hostile is being damaged by something. Friendly = the player is being damaged by something.</param>
    public void CreateHitNumber(int number, Vector2 position, DamageHitType hitType = DamageHitType.Hostile)
    {
        HitNumberUIHandler hitNumber = Instantiate(hitNumberPrefab, transform);
        hitNumber.gameObject.SetActive(true);
        hitNumber.SetDamage(number, hitType);

        hitNumber.transform.position = position;
    }
}
