﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(RecipeTypeList))]
public class RecipeTypeListDrawer : Editor
{
    public override void OnInspectorGUI()
    {
        RecipeTypeList recipeTypeList = (RecipeTypeList)target;

        EditorGUILayout.PropertyField(serializedObject.FindProperty("recipeType"));

        if (recipeTypeList.recipeType == Recipe.RecipeType.BlacksmithCraft)
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("equipmentType"));

            if (recipeTypeList.equipmentType == Equipment.EquipmentType.Armor)
                EditorGUILayout.PropertyField(serializedObject.FindProperty("armorType"));
            else if (recipeTypeList.equipmentType == Equipment.EquipmentType.Weapon)
                EditorGUILayout.PropertyField(serializedObject.FindProperty("weaponType"));
        }

        EditorGUILayout.PropertyField(serializedObject.FindProperty("recipes"), true);

        serializedObject.ApplyModifiedProperties();

        if (GUILayout.Button("Find Recipes of Type"))
        {
            FindRecipesOfRecipeType(recipeTypeList);

            serializedObject.ApplyModifiedProperties();
            EditorUtility.SetDirty(recipeTypeList);
            AssetDatabase.SaveAssets();
        }
    }

    public void FindRecipesOfRecipeType(RecipeTypeList recipeTypeList)
    {
        var guids = AssetDatabase.FindAssets("t:Recipe");

        recipeTypeList.recipes = new List<Recipe>();

        foreach (string guid in guids)
        {
            string path = AssetDatabase.GUIDToAssetPath(guid);

            var recipe = AssetDatabase.LoadAssetAtPath<Recipe>(path);

            if (recipe.recipeType == recipeTypeList.recipeType)
            {
                if (recipeTypeList.recipeType == Recipe.RecipeType.BlacksmithCraft)
                {
                    if (recipeTypeList.equipmentType == Equipment.EquipmentType.Armor && recipe.outputItem.attachedItem is ItemArmor)
                    {
                        ItemArmor armor = recipe.outputItem.attachedItem as ItemArmor;

                        if (armor.armorType == recipeTypeList.armorType)
                        {
                            recipeTypeList.recipes.Add(recipe);
                        }
                    }
                    else if (recipeTypeList.equipmentType == Equipment.EquipmentType.Weapon && recipe.outputItem.attachedItem is ItemWeapon)
                    {
                        ItemWeapon weapon = recipe.outputItem.attachedItem as ItemWeapon;

                        if (weapon.weaponType == recipeTypeList.weaponType)
                        {
                            recipeTypeList.recipes.Add(recipe);
                        }
                    }
                }
                else recipeTypeList.recipes.Add(recipe);
            }
        }
    }
}
