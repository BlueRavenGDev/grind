﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(CustomMotionController))]
public class CustomMotionControllerEditor : Editor
{
    private CustomMotionController motionController;

    private bool empty = false;

    private bool confirm;

    private bool setMotion;
    public SerializedProperty currentEditingMotion;
    public SerializedProperty motions;
    public SerializedProperty puppetObject;
    public SerializedProperty startPlayingIdle;
    public SerializedProperty playing;
    public SerializedProperty playingIdle;
    public SerializedProperty returnToIdleWaiting;

    public CustomMotion copyToMotion;

    private bool show = true;

    private void OnEnable()
    {
        motionController = (CustomMotionController)target;
        currentEditingMotion = serializedObject.FindProperty("currentEditingMotion");
        motions = serializedObject.FindProperty("motions");
        puppetObject = serializedObject.FindProperty("puppetObject");
        startPlayingIdle = serializedObject.FindProperty("startPlayingIdle");
        playing = serializedObject.FindProperty("playing");
        playingIdle = serializedObject.FindProperty("playingIdle");
        returnToIdleWaiting = serializedObject.FindProperty("returnToIdleWaiting");
    }

    public override void OnInspectorGUI()
    {
        #region Currently Editing Motion and Copy To Motion
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Currently Editing Motion");
        EditorGUILayout.LabelField("Copy To Motion");
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();

        EditorGUI.BeginChangeCheck();
        motionController.e_currentMotion = EditorGUILayout.ObjectField(new GUIContent(""), motionController.e_currentMotion, typeof(CustomMotion), true) as CustomMotion;
        if (EditorGUI.EndChangeCheck())
        {
            SelectNewMotion();
            UpdateStuff();
        }

        EditorGUI.BeginChangeCheck();
        copyToMotion = EditorGUILayout.ObjectField(new GUIContent("", "Copy The current editing motion to this mode, then change to it. Requires confirm to be true."), copyToMotion, typeof(CustomMotion), true) as CustomMotion;
        if (EditorGUI.EndChangeCheck())
        {
            CopyToMotion();
            UpdateStuff();
        }

        EditorGUILayout.EndHorizontal();
        #endregion

        if (!motionController.e_currentMotion)
        {
            GUILayout.Label("Enter A Motion To Edit");
            return;
        }

        motionController.e_currentMotion.motionName = EditorGUILayout.TextField("Motion Name", motionController.e_currentMotion.motionName);

        EditorGUI.BeginChangeCheck();

        GUILayout.Label("Motion Node Editor");

        if (!empty)
        {
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("<<"))
            {
                ChangeToIndex(0);
            }
            if (GUILayout.Button("<"))
            {
                ChangeLeft();
            }
            if (GUILayout.Button(">"))
            {
                ChangeRight();
            }
            if (GUILayout.Button(">>"))
            {
                ChangeToIndex(motionController.e_currentMotionNodes.Count - 1);
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Move Left"))
            {
                MoveLeft();
            }
            if (GUILayout.Button("Move Right"))
            {
                MoveRight();
            }
            GUILayout.EndHorizontal();

            GUILayout.Label("Index: " + motionController.e_currentSelectedIndex + " (max " + (motionController.e_currentMotionNodes.Count - 1) + ", count " + motionController.e_currentMotionNodes.Count + ")");

            show = EditorGUILayout.Toggle("Show", show);
            if (show)
            {
                GUILayout.BeginVertical("Box");
                #region position stuff
                EditorGUILayout.LabelField("Position:");

                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Set Start To Last End"))
                    AttachNodeStartToLastEnd();
                if (GUILayout.Button("Set End To Next Start"))
                    AttachNodeEndToNextStart();
                GUILayout.EndHorizontal();

                motionController.e_currentSelectedNode.delayPosition = EditorGUILayout.FloatField("Delay Position", motionController.e_currentSelectedNode.delayPosition);
                motionController.e_currentSelectedNode.timePosition = EditorGUILayout.FloatField("Duration Position", motionController.e_currentSelectedNode.timePosition);

                GUILayout.BeginHorizontal();
                motionController.e_currentSelectedNode.startPosition = EditorGUILayout.Vector2Field("Start Position", motionController.e_currentSelectedNode.startPosition);
                if (GUILayout.Button("Reset"))
                    motionController.e_currentSelectedNode.startPosition = Vector2.zero;
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                motionController.e_currentSelectedNode.endPosition = EditorGUILayout.Vector2Field("End Position", motionController.e_currentSelectedNode.endPosition);
                if (GUILayout.Button("Reset"))
                    motionController.e_currentSelectedNode.endPosition = Vector2.zero;
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                motionController.e_currentSelectedNode.startPositionTan = EditorGUILayout.Vector2Field("Start Tangent", motionController.e_currentSelectedNode.startPositionTan);
                if (GUILayout.Button("Reset"))
                    motionController.e_currentSelectedNode.startPositionTan = Vector2.zero;
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                motionController.e_currentSelectedNode.endPositionTan = EditorGUILayout.Vector2Field("End Tangent", motionController.e_currentSelectedNode.endPositionTan);
                if (GUILayout.Button("Reset"))
                    motionController.e_currentSelectedNode.endPositionTan = Vector2.zero;
                GUILayout.EndHorizontal();
                #endregion

                #region Rotation stuff
                EditorGUILayout.LabelField("Rotation:");

                motionController.e_currentSelectedNode.delayRotation = EditorGUILayout.FloatField("Delay Rotation", motionController.e_currentSelectedNode.delayRotation);
                motionController.e_currentSelectedNode.timeRotation = EditorGUILayout.FloatField("Duration Rotation", motionController.e_currentSelectedNode.timeRotation);
                motionController.e_currentSelectedNode.startRotation = EditorGUILayout.FloatField("Start Rotation", motionController.e_currentSelectedNode.startRotation);
                motionController.e_currentSelectedNode.endRotation = EditorGUILayout.FloatField("End Rotation", motionController.e_currentSelectedNode.endRotation);
                #endregion

                motionController.e_currentSelectedNode.delayReturn = EditorGUILayout.FloatField("Delay Return", motionController.e_currentSelectedNode.delayReturn);

                GUILayout.Label("Other:");
                motionController.e_currentSelectedNode.damageDealing = EditorGUILayout.Toggle(new GUIContent("Damage Dealing", "Is this motion node damage dealing?"),
                    motionController.e_currentSelectedNode.damageDealing);
                GUILayout.EndVertical();
            }

            if (GUILayout.Button("Remove"))
            {
                RemoveCurrentNode();
            }
        }

        if (GUILayout.Button("Create New Node"))
        {
            CreateNode();
        }

        if (GUILayout.Button("Remove All"))
        {
            RemoveAllNodes();
        }

        confirm = EditorGUILayout.Toggle(new GUIContent("Confirm"), confirm);

        EditorGUILayout.LabelField("Options");
        motionController.e_drawAllPaths = EditorGUILayout.Toggle(new GUIContent("Draw All Paths"), motionController.e_drawAllPaths);
        motionController.e_drawOtherPaths = EditorGUILayout.Toggle(new GUIContent("Draw Old Paths"), motionController.e_drawOtherPaths);
        if (motionController.e_drawOtherPaths)
            motionController.e_drawOldPaths = EditorGUILayout.Toggle(new GUIContent("Draw Just Previous"), motionController.e_drawOldPaths);

        motionController.e_currentEditMode = (MotionEditMode)EditorGUILayout.EnumFlagsField(motionController.e_currentEditMode);

        if (motionController.e_currentSelectedIndex < 0 || motionController.e_currentSelectedIndex > motionController.e_currentMotionNodes.Count )
        {
            ResetToDefaultNode();
        }

        if (EditorGUI.EndChangeCheck())
        {
            UpdateStuff();
        }

        GUILayout.Label("Defaults");
        EditorGUILayout.PropertyField(puppetObject, true);
        EditorGUILayout.PropertyField(motions, true);
        EditorGUILayout.PropertyField(startPlayingIdle, true);

        motionController.idleMotionIndex = EditorGUILayout.IntField("Idle Motion Index", motionController.idleMotionIndex);

        if (Application.isPlaying)
        {
            GUILayout.Label("Debug");
            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.ObjectField("Current Motion", motionController.playingMotion, typeof(CustomMotion), false);
            EditorGUILayout.IntField("Current Motion Index", motionController.playingMotionNodeIndex);

            EditorGUILayout.PropertyField(playing);
            EditorGUILayout.PropertyField(playingIdle);
            EditorGUILayout.PropertyField(returnToIdleWaiting);
            EditorGUI.EndDisabledGroup();
        }

        serializedObject.ApplyModifiedProperties();
    }

    private void CopyToMotion()
    {
        if (copyToMotion && confirm)
        {
            copyToMotion.nodes = motionController.e_currentMotion.nodes;
            motionController.e_currentMotion = copyToMotion;

            copyToMotion = null;
            confirm = false;
        }
    }

    private void SelectNewMotion()
    {
        if (motionController.e_currentMotion)
        {
            motionController.e_currentMotionNodes = motionController.e_currentMotion.nodes;

            if (motionController.e_currentMotion.nodes != null && motionController.e_currentMotion.nodes.Count > 0)
            {
                ChangeToIndex(0);
            }
            else empty = true;
        }
    }

    private void ChangeToIndex(int index)
    {
        if (index >= 0 && index < motionController.e_currentMotionNodes.Count)
        {
            motionController.e_currentSelectedIndex = index;
            motionController.e_currentSelectedNode = motionController.e_currentMotionNodes[motionController.e_currentSelectedIndex];
        }
    }

    private void ReloadIndex()
    {
        if (motionController.e_currentMotionNodes.Count > 0)
            motionController.e_currentSelectedNode = motionController.e_currentMotionNodes[motionController.e_currentSelectedIndex];
        else empty = true;
    }

    private void UpdateWeaponIndex()
    {
        if (!empty)
        {
            motionController.e_currentMotionNodes[motionController.e_currentSelectedIndex] = motionController.e_currentSelectedNode;
        }
    }

    private void ChangeLeft()
    {
        if (motionController.e_currentSelectedIndex > 0)
        {
            motionController.e_currentSelectedIndex--;
            motionController.e_currentSelectedNode = motionController.e_currentMotionNodes[motionController.e_currentSelectedIndex];
        }
    }

    private void MoveLeft()
    {
        if (motionController.e_currentSelectedIndex > 0)
        {
            MotionNode prevNode = motionController.e_currentSelectedNode;
            int prevIndex = motionController.e_currentSelectedIndex;

            motionController.e_currentSelectedIndex--;
            motionController.e_currentSelectedNode = motionController.e_currentMotionNodes[motionController.e_currentSelectedIndex];

            motionController.e_currentMotionNodes[motionController.e_currentSelectedIndex] = prevNode;
            motionController.e_currentMotionNodes[prevIndex] = motionController.e_currentSelectedNode;

            motionController.e_currentSelectedIndex = prevIndex;
            motionController.e_currentSelectedNode = motionController.e_currentMotionNodes[motionController.e_currentSelectedIndex];
        }
    }

    private void ChangeRight()
    {
        if (motionController.e_currentSelectedIndex < motionController.e_currentMotionNodes.Count - 1)
        {
            motionController.e_currentSelectedIndex++;
            motionController.e_currentSelectedNode = motionController.e_currentMotionNodes[motionController.e_currentSelectedIndex];
        }
    }

    private void MoveRight()
    {
        if (motionController.e_currentSelectedIndex < motionController.e_currentMotionNodes.Count - 1)
        {
            MotionNode prevNode = motionController.e_currentSelectedNode;
            int prevIndex = motionController.e_currentSelectedIndex;

            motionController.e_currentSelectedIndex++;
            motionController.e_currentSelectedNode = motionController.e_currentMotionNodes[motionController.e_currentSelectedIndex];

            motionController.e_currentMotionNodes[motionController.e_currentSelectedIndex] = prevNode;
            motionController.e_currentMotionNodes[prevIndex] = motionController.e_currentSelectedNode;

            motionController.e_currentSelectedIndex = prevIndex;
            motionController.e_currentSelectedNode = motionController.e_currentMotionNodes[motionController.e_currentSelectedIndex];
        }
    }

    private void AttachNodeStartToLastEnd()
    {
        if (motionController.e_currentSelectedIndex > 0)
        {
            MotionNode nextNode = motionController.e_currentMotionNodes[motionController.e_currentSelectedIndex - 1];

            motionController.e_currentSelectedNode.startPosition = nextNode.endPosition;
        }
    }

    private void AttachNodeEndToNextStart()
    {
        if (motionController.e_currentSelectedIndex < motionController.e_currentMotionNodes.Count - 1)
        {
            MotionNode nextNode = motionController.e_currentMotionNodes[motionController.e_currentSelectedIndex + 1];

            motionController.e_currentSelectedNode.endPosition = nextNode.startPosition;
        }
    }

    private void ResetToDefaultNode()
    {
        ChangeToIndex(0);
    }

    private void CreateNode()
    {
        empty = false;
        //weaponController.currentSelectedNode.startPosition = weaponController.currentSelectedNode.endPosition;
        //weaponController.currentSelectedNode.startRotation = weaponController.currentSelectedNode.endRotation;

        if (motionController.e_currentMotionNodes != null && motionController.e_currentMotionNodes.Count > 0)
        {
            motionController.e_currentMotionNodes.Add(motionController.e_currentSelectedNode);
            ChangeToIndex(motionController.e_currentMotionNodes.Count - 1);
            MotionNode prevNode = motionController.e_currentMotionNodes[motionController.e_currentSelectedIndex - 1];
            motionController.e_currentSelectedNode.startPosition = prevNode.endPosition;
            motionController.e_currentSelectedNode.endPosition = Vector2.zero;
            motionController.e_currentSelectedNode.startRotation = prevNode.endRotation;
            motionController.e_currentSelectedNode.endRotation = 0;
        }
        else
        {
            motionController.e_currentMotionNodes = new List<MotionNode> { motionController.e_currentSelectedNode };
            ChangeToIndex(0);
        }
    }

    private void RemoveCurrentNode()
    {
        if (confirm)
        {
            motionController.e_currentMotionNodes.RemoveAt(motionController.e_currentSelectedIndex);
            if (motionController.e_currentMotionNodes.Count <= 0)
                empty = true;
            else if (motionController.e_currentSelectedIndex > motionController.e_currentMotionNodes.Count - 1)
                ChangeToIndex(motionController.e_currentMotionNodes.Count - 1);

            confirm = false;
        }
    }

    private void RemoveAllNodes()
    {
        if (confirm)
        {
            motionController.e_currentMotionNodes.Clear();
            empty = true;

            confirm = false;
        }
    }

    private void UpdateStuff()
    {
        if (!empty)
        {
            motionController.e_currentMotionNodes[motionController.e_currentSelectedIndex] = motionController.e_currentSelectedNode;

            if (motionController.e_currentMotion)
                motionController.e_currentMotion.nodes = motionController.e_currentMotionNodes;
        }
    }

    private void OnSceneGUI()
    {
        if (!motionController.e_currentMotion)
        {
            return;
        }

        if (!empty)
        {
            EditorGUI.BeginChangeCheck();

            Vector2 endPosition = motionController.e_currentSelectedNode.endPosition;
            Vector2 startPosition = motionController.e_currentSelectedNode.startPosition;
            Vector2 endPositionTan = motionController.e_currentSelectedNode.endPositionTan;
            Vector2 startPositionTan = motionController.e_currentSelectedNode.startPositionTan;

            if ((motionController.e_currentEditMode & MotionEditMode.EditEndNode) == MotionEditMode.EditEndNode)
                endPosition = (Vector2)Handles.FreeMoveHandle(motionController.e_currentSelectedNode.endPosition + (Vector2)motionController.transform.position, Quaternion.identity, 
                    HandleUtility.GetHandleSize(motionController.e_currentSelectedNode.endPosition) * 0.1f, Vector2.one * 0.5f, Handles.CircleHandleCap) - (Vector2)motionController.transform.position;
            if ((motionController.e_currentEditMode & MotionEditMode.EditStartNode) == MotionEditMode.EditStartNode)
                startPosition = (Vector2)Handles.FreeMoveHandle(motionController.e_currentSelectedNode.startPosition + (Vector2)motionController.transform.position, Quaternion.identity, 
                    HandleUtility.GetHandleSize(motionController.e_currentSelectedNode.startPosition) * 0.1f, Vector2.one * 0.5f, Handles.CircleHandleCap) - (Vector2)motionController.transform.position;

            if ((motionController.e_currentEditMode & MotionEditMode.EditEndCurve) == MotionEditMode.EditEndCurve)
            {
                Vector2 sumPos = motionController.e_currentSelectedNode.endPosition + motionController.e_currentSelectedNode.endPositionTan + (Vector2)motionController.transform.position;
                Handles.DrawLine(motionController.e_currentSelectedNode.endPosition + (Vector2)motionController.transform.position, sumPos);
                endPositionTan = (Vector2)Handles.FreeMoveHandle(sumPos, Quaternion.identity, HandleUtility.GetHandleSize(motionController.e_currentSelectedNode.endPositionTan) * 0.1f, 
                    Vector2.one * 0.5f, Handles.RectangleHandleCap) - motionController.e_currentSelectedNode.endPosition - (Vector2)motionController.transform.position;
            }

            if ((motionController.e_currentEditMode & MotionEditMode.EditStartCurve) == MotionEditMode.EditStartCurve)
            {
                Vector2 sumPos = motionController.e_currentSelectedNode.startPosition + motionController.e_currentSelectedNode.startPositionTan + (Vector2)motionController.transform.position;
                Handles.DrawLine(motionController.e_currentSelectedNode.startPosition + (Vector2)motionController.transform.position, sumPos);
                startPositionTan = (Vector2)Handles.FreeMoveHandle(sumPos, Quaternion.identity, HandleUtility.GetHandleSize(motionController.e_currentSelectedNode.startPositionTan) * 0.1f, 
                    Vector2.one * 0.5f, Handles.RectangleHandleCap) - motionController.e_currentSelectedNode.startPosition - (Vector2)motionController.transform.position;
            }

            if (EditorGUI.EndChangeCheck())
            {
                motionController.e_currentSelectedNode.endPosition = endPosition;
                motionController.e_currentSelectedNode.startPosition = startPosition;
                motionController.e_currentSelectedNode.endPositionTan = endPositionTan;
                motionController.e_currentSelectedNode.startPositionTan = startPositionTan;
                UpdateWeaponIndex();
            }

            List<Vector2> pointsDottedLine = new List<Vector2>();
            List<Vector2> tangentsDottedLine = new List<Vector2>();

            if (motionController.e_drawAllPaths)
            {
                for (int i = 0; i < motionController.e_currentMotionNodes.Count; i++)
                {
                    MotionNode node = motionController.e_currentMotionNodes[i];

                    if (i != motionController.e_currentSelectedIndex)
                    {
                        pointsDottedLine.Add(node.startPosition);
                        pointsDottedLine.Add(node.endPosition);

                        tangentsDottedLine.Add(node.startPositionTan);
                        tangentsDottedLine.Add(node.endPositionTan);
                    }
                }
            }
            else if (motionController.e_drawOtherPaths)
            {
                if (motionController.e_drawOldPaths)
                {
                    for (int i = motionController.e_currentSelectedIndex - 1; i <= motionController.e_currentSelectedIndex; i++)
                    {
                        if (i < 0)
                            continue;

                        MotionNode node = motionController.e_currentMotionNodes[i];

                        if (i != motionController.e_currentSelectedIndex)
                        {
                            pointsDottedLine.Add(node.startPosition);
                            pointsDottedLine.Add(node.endPosition);

                            tangentsDottedLine.Add(node.startPositionTan);
                            tangentsDottedLine.Add(node.endPositionTan);
                        }
                    }
                }
                else
                {
                    for (int i = 0; i <= motionController.e_currentSelectedIndex; i++)
                    {
                        MotionNode node = motionController.e_currentMotionNodes[i];

                        if (i != motionController.e_currentSelectedIndex)
                        {
                            pointsDottedLine.Add(node.startPosition);
                            pointsDottedLine.Add(node.endPosition);

                            tangentsDottedLine.Add(node.startPositionTan);
                            tangentsDottedLine.Add(node.endPositionTan);
                        }
                    }
                }
            }

            Handles.DrawBezier(startPosition + (Vector2)motionController.transform.position, endPosition + (Vector2)motionController.transform.position, 
                startPosition + startPositionTan + (Vector2)motionController.transform.position, 
                endPosition + endPositionTan + (Vector2)motionController.transform.position, Color.black, null, 2);
            
            Debug.Assert(pointsDottedLine.Count % 2 == 0);

            for (int i = 0; i < pointsDottedLine.Count; i += 2) 
            {
                Vector2 start = pointsDottedLine[i] + (Vector2)motionController.transform.position;
                Vector2 end = pointsDottedLine[i + 1] + (Vector2)motionController.transform.position;

                Vector2 startTan = tangentsDottedLine[i];
                Vector2 endTan = tangentsDottedLine[i + 1];

                Handles.DrawBezier(start, end, start + startTan, end + endTan, Color.gray, null, 1);
            }
        }
    }
}