﻿using UnityEngine;
using UnityEditor;

public static class AttackSystemCreation
{
    public const int hitboxlayer = 11;

    [MenuItem("AttackSystemCreation", menuItem = "GameObject/Grind/Create Hitbox")]
    private static void CreateHitbox(MenuCommand menuCommand)
    {
        GameObject context = menuCommand.context as GameObject;

        GameObject go = new GameObject("Hitbox");

        go.AddComponent<BoxCollider2D>().isTrigger = true;
        go.AddComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        go.AddComponent<FreezeTransform>().calculateOffBasePosition = true;
        go.GetComponent<FreezeTransform>().freezeLocally = true;
        Hitbox hitbox = go.AddComponent<Hitbox>();

        go.layer = hitboxlayer;

        if (context)
        {
            go.transform.SetParent(context.transform);
            go.transform.position = context.transform.position;
        }
    }

    [MenuItem("AttackSystemCreation", menuItem = "GameObject/Grind/Create Hurtbox")]
    private static void CreateHurtbox(MenuCommand menuCommand)
    {
        GameObject context = menuCommand.context as GameObject;

        GameObject go = new GameObject("Hurtbox");
        
        go.AddComponent<BoxCollider2D>().isTrigger = true;
        go.AddComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        go.AddComponent<FreezeTransform>().calculateOffBasePosition = true;
        go.GetComponent<FreezeTransform>().freezeLocally = true;
        Hurtbox hurtbox = go.AddComponent<Hurtbox>();

        go.layer = hitboxlayer;

        if (context)
        {
            go.transform.SetParent(context.transform);
            go.transform.position = context.transform.position;

            HurtboxManager manager = context.GetComponentInParent<HurtboxManager>();
            if (manager)
            {
                hurtbox.hurtboxManager = manager;
            }
        }
    }
}