﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEditor;
using System.Collections.Generic;

public class CreateNavNode : ScriptableObject
{
    [MenuItem("Test", menuItem = "GameObject/Grind/Create Attached Node", priority = 10)]
    private static void CreateAttachedNavNode(MenuCommand menuCommand)
    {
        if (menuCommand.context && menuCommand.context as GameObject)
        {
            if ((menuCommand.context as GameObject).GetComponent<NavNode>())
            {
                GameObject go = new GameObject();
                go.AddComponent<BoxCollider2D>();

                NavNode node = go.AddComponent<NavNode>();
                NavNode contextNode = (menuCommand.context as GameObject).GetComponent<NavNode>();

                bool canAssignNode = true;

                //check the right node first - we prefer the right over the left
                if (contextNode.rightNode)   //can't put it in left
                {
                    if (contextNode.leftNode)
                    {
                        canAssignNode = false;
                    }
                    else
                    {
                        contextNode.leftNode = node;
                    }
                }
                else
                {   //can put it in left
                    contextNode.rightNode = node;
                }

                if (canAssignNode)
                {
                    node.leftNode = contextNode;

                    /*node.connectedNodes = new List<NavNode>();
                    node.connectedNodes.Add(contextNode);
                    contextNode.connectedNodes.Add(node);*/

                    node.transform.position = contextNode.transform.position + new Vector3(0, 1, 0);
                    if (contextNode.transform.parent)
                        node.transform.parent = contextNode.transform.parent;

                    node.name = "Attached Node";

                    Selection.activeGameObject = node.gameObject;

                    Undo.RegisterCreatedObjectUndo(go, "Created Attached Node");
                }
                else
                {
                    Debug.LogWarning("Creating an attached node requires that the selected node to have an open left or right node slot.");
                    Destroy(go);
                }
            }
            else Debug.LogWarning("Creating an attached node requires a node to be attached to. Select a GameObject with the component NavNode.");
        }
        else
        {
            Debug.LogWarning("Creating an attached node requires a node to be attached to. Select a GameObject with the component NavNode.");
        }
    }

    [MenuItem("Test", menuItem = "GameObject/Grind/Create New Node", priority = 10)]
    private static void CreateNewNavNode(MenuCommand menuCommand)
    {
        GameObject go = new GameObject();
        go.AddComponent<BoxCollider2D>();

        NavNode node = go.AddComponent<NavNode>();

        if (menuCommand.context is GameObject)
        {
            node.transform.parent = (menuCommand.context as GameObject).transform;
        }

        node.name = "New Node";

        Selection.activeGameObject = node.gameObject;

        Undo.RegisterCreatedObjectUndo(go, "Created New Node");
    }
}