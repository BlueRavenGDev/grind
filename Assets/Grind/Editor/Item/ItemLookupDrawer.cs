﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(ItemLookup))]
public class ItemLookupDrawer : Editor
{
    public override void OnInspectorGUI()
    {
        ItemLookup itemLookup = (ItemLookup)target;

        EditorGUI.BeginChangeCheck();

        EditorGUILayout.PropertyField(serializedObject.FindProperty("items"), true);

        if (GUILayout.Button("Clear Item Assets"))
        {
            itemLookup.ResetList();
        }

        if (GUILayout.Button("Find Item Assets"))
        {
            FindAllItemAssets(itemLookup);
        }

        EditorGUILayout.LabelField("Don't worry about duplicates, they are ignored.");

        if (EditorGUI.EndChangeCheck())
        {
            serializedObject.ApplyModifiedProperties();

            EditorUtility.SetDirty(itemLookup);
            AssetDatabase.SaveAssets();
        }
    }

    private void FindAllItemAssets(ItemLookup itemLookup)
    {
        var guids = AssetDatabase.FindAssets("t:Item");

        foreach (string guid in guids)
        {
            string path = AssetDatabase.GUIDToAssetPath(guid);

            var item = AssetDatabase.LoadAssetAtPath<Item>(path);
            itemLookup.items.Add(item);
        }

        itemLookup.ResetTable();
    }
}