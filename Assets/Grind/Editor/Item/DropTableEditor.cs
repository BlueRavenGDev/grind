﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DropTable))]
public class DropTableEditor : Editor
{
    private SerializedProperty numDrops;
    private SerializedProperty drops;

    private DropTable dropTable;

    private bool confirm;

    private void OnEnable()
    {
        dropTable = (DropTable)target;

        numDrops = serializedObject.FindProperty("numDrops");
        drops = serializedObject.FindProperty("drops");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUI.BeginChangeCheck();

        EditorGUILayout.PropertyField(numDrops);
        for (int i = 0; i < drops.arraySize; i++)
        {
            SerializedProperty element = drops.GetArrayElementAtIndex(i);
            GUILayout.BeginVertical("Box");
            EditorGUILayout.PropertyField(element.FindPropertyRelative("dropChance"));

            SerializedProperty droppedItem = element.FindPropertyRelative("droppedItem");

            EditorGUILayout.PropertyField(droppedItem.FindPropertyRelative("attachedItem"));
            EditorGUILayout.PropertyField(droppedItem.FindPropertyRelative("stackSize"));

            if (confirm && GUILayout.Button("Remove Dropped Item"))
            {
                dropTable.drops.RemoveAt(i);
                confirm = false;
            }
            GUILayout.EndVertical();
        }

        if (GUILayout.Button("Add New Dropped Item"))
        {
            if (dropTable == null)
                dropTable = new DropTable();
            dropTable.drops.Add(new DropChance());
        }

        confirm = GUILayout.Toggle(confirm, "Confirm");

        if (EditorGUI.EndChangeCheck())
        {
            EditorUtility.SetDirty(dropTable);
        }

        serializedObject.ApplyModifiedProperties();
    }
}