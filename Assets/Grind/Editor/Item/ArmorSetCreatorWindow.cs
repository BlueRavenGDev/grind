﻿using UnityEngine;
using UnityEditor;

public class ArmorSetCreatorWindow : EditorWindow
{
    private string localArmorPathTooltip = "The path to create the assets in. Note that this should end in a '/', as it will cause issues otherwise.";
    public string localArmorPath = "Assets/Grind/Data/Item/Armor/Armorsets/";

    public string localCraftPath = "Assets/Grind/Data/Recipes/Blacksmith/Crafting/Armor/ArmorSets";

    public string findName = "Armor";

    private string namePrefixTooltip ="The 'first' part of the name. For instance, if you wanted a leather armor set with a Leather Helmet, you would set this to 'Leather.'";
    public string namePrefix;

    public bool namesOpen;
    public string headName = "Helmet";
    public string chestName = "ChestPiece";
    public string armsName = "Gloves";
    public string legsName = "Leggings";
    public string feetName = "Feet";

    public bool spritesOpen;
    public Sprite headSprite;
    public Sprite chestSprite;
    public Sprite armsSprite;
    public Sprite legsSprite;
    public Sprite feetSprite;

    public int defense;
    public int rarity;

    public int sellCost;

    public bool createRecipes;
    public int recipeCost;

    [MenuItem("Window/Grind/Armor Set Creator")]
    public static void ShowWindow()
    {
        GetWindow<ArmorSetCreatorWindow>();
    }

    private void OnGUI()
    {
        GUILayout.Label("Creation Settings");
        localArmorPath = EditorGUILayout.TextField(new GUIContent("Local Armor Path", localArmorPathTooltip), localArmorPath);

        namePrefix = EditorGUILayout.TextField(new GUIContent("Name Prefix", namePrefixTooltip), namePrefix);

        namesOpen = EditorGUILayout.Foldout(namesOpen, new GUIContent("Names"), true);
        if (namesOpen)
        {
            headName = EditorGUILayout.TextField("Head Name", headName);
            chestName = EditorGUILayout.TextField("Chest Name", chestName);
            armsName = EditorGUILayout.TextField("Arms Name", armsName);
            legsName = EditorGUILayout.TextField("Legs Name", legsName);
            feetName = EditorGUILayout.TextField("Feet Name", feetName);
        }

        spritesOpen = EditorGUILayout.Foldout(spritesOpen, new GUIContent("Sprites"), true);
        if (spritesOpen)
        {
            headSprite = EditorGUILayout.ObjectField(new GUIContent("Head Sprite"), headSprite, typeof(Sprite), true) as Sprite;
            chestSprite = EditorGUILayout.ObjectField(new GUIContent("Chest Sprite"), chestSprite, typeof(Sprite), true) as Sprite;
            armsSprite = EditorGUILayout.ObjectField(new GUIContent("Arms Sprite"), armsSprite, typeof(Sprite), true) as Sprite;
            legsSprite = EditorGUILayout.ObjectField(new GUIContent("Legs Sprite"), legsSprite, typeof(Sprite), true) as Sprite;
            feetSprite = EditorGUILayout.ObjectField(new GUIContent("Feet Sprite"), feetSprite, typeof(Sprite), true) as Sprite;
        }

        defense = EditorGUILayout.IntField("Defense", defense);
        rarity = EditorGUILayout.IntField("Rarity", rarity);
        sellCost = EditorGUILayout.IntField("Sell Gold", sellCost);

        createRecipes = EditorGUILayout.Toggle(new GUIContent("Create Recipes", "Create recipes to craft the armor pieces."), createRecipes);
        if (createRecipes)
        {
            localCraftPath = EditorGUILayout.TextField(new GUIContent("Local Craft Path"), localCraftPath);
            recipeCost = EditorGUILayout.IntField("Recipe Cost", recipeCost);
        }

        if (GUILayout.Button(new GUIContent("Reset to Defaults", "Reset the values of variables like localArmorPath, headName, etc to their original default values.")))
        {

        }

        if (GUILayout.Button(new GUIContent("Create Armor Set", "Creates the assets for the armor sets and adds them to the project.")))
        {
            CreateArmors();
        }

    }

    private void CreateArmors()
    {
        ItemArmor head = CreateInstance<ItemArmor>();
        head.name = namePrefix + headName;
        head.iname = namePrefix + " " + headName;
        head.armorType = ItemArmor.ArmorType.Head;
        head.defense = defense;
        head.sellGold = sellCost;
        head.icon = headSprite;
        head.colorOffRarity = true;
        head.rarity = rarity;

        ItemArmor chest = CreateInstance<ItemArmor>();
        chest.name = namePrefix + chestName;
        chest.iname = namePrefix + " " + chestName;
        chest.armorType = ItemArmor.ArmorType.Chest;
        chest.defense = defense;
        chest.sellGold = sellCost;
        chest.icon = chestSprite;
        chest.colorOffRarity = true;
        chest.rarity = rarity;

        ItemArmor arms = CreateInstance<ItemArmor>();
        arms.name = namePrefix + armsName;
        arms.iname = namePrefix + " " + armsName;
        arms.armorType = ItemArmor.ArmorType.Arms;
        arms.defense = defense;
        arms.sellGold = sellCost;
        arms.icon = armsSprite;
        arms.colorOffRarity = true;
        arms.rarity = rarity;

        ItemArmor legs = CreateInstance<ItemArmor>();
        legs.name = namePrefix + legsName;
        legs.iname = namePrefix + " " + legsName;
        legs.armorType = ItemArmor.ArmorType.Legs;
        legs.defense = defense;
        legs.sellGold = sellCost;
        legs.icon = legsSprite;
        legs.colorOffRarity = true;
        legs.rarity = rarity;

        ItemArmor feet = CreateInstance<ItemArmor>();
        feet.name = namePrefix + feetName;
        feet.iname = namePrefix + " " + feetName;
        feet.armorType = ItemArmor.ArmorType.Feet;
        feet.defense = defense;
        feet.sellGold = sellCost;
        feet.icon = feetSprite;
        feet.colorOffRarity = true;
        feet.rarity = rarity;

        if (createRecipes)
        {
            Recipe headRecipe = CreateInstance<Recipe>();
            headRecipe.name = namePrefix + headName + "Recipe";
            headRecipe.outputItem = new ItemStack() { attachedItem = head, stackSize = 1 };
            headRecipe.costsGold = true;
            headRecipe.goldCost = recipeCost;
            headRecipe.recipeType = Recipe.RecipeType.BlacksmithCraft;

            Recipe chestRecipe = CreateInstance<Recipe>();
            chestRecipe.name = namePrefix + chestName + "Recipe";
            chestRecipe.outputItem = new ItemStack() { attachedItem = chest, stackSize = 1 };
            chestRecipe.costsGold = true;
            chestRecipe.goldCost = recipeCost;
            chestRecipe.recipeType = Recipe.RecipeType.BlacksmithCraft;

            Recipe armsRecipe = CreateInstance<Recipe>();
            armsRecipe.name = namePrefix + armsName + "Recipe";
            armsRecipe.outputItem = new ItemStack() { attachedItem = arms, stackSize = 1 };
            armsRecipe.costsGold = true;
            armsRecipe.goldCost = recipeCost;
            armsRecipe.recipeType = Recipe.RecipeType.BlacksmithCraft;

            Recipe legsRecipe = CreateInstance<Recipe>();
            legsRecipe.name = namePrefix + legsName + "Recipe";
            legsRecipe.outputItem = new ItemStack() { attachedItem = legs, stackSize = 1 };
            legsRecipe.costsGold = true;
            legsRecipe.goldCost = recipeCost;
            legsRecipe.recipeType = Recipe.RecipeType.BlacksmithCraft;

            Recipe feetRecipe = CreateInstance<Recipe>();
            feetRecipe.name = namePrefix + feetName + "Recipe";
            feetRecipe.outputItem = new ItemStack() { attachedItem = feet, stackSize = 1 };
            feetRecipe.costsGold = true;
            feetRecipe.goldCost = recipeCost;
            feetRecipe.recipeType = Recipe.RecipeType.BlacksmithCraft;

            AssetDatabase.CreateAsset(headRecipe, localCraftPath + headRecipe.name + ".asset");
            AssetDatabase.CreateAsset(chestRecipe, localCraftPath + chestRecipe.name + ".asset");
            AssetDatabase.CreateAsset(armsRecipe, localCraftPath + armsRecipe.name + ".asset");
            AssetDatabase.CreateAsset(legsRecipe, localCraftPath + legsRecipe.name + ".asset");
            AssetDatabase.CreateAsset(feetRecipe, localCraftPath + feetRecipe.name + ".asset");
        }

        AssetDatabase.CreateAsset(head, localArmorPath + head.name + ".asset");
        AssetDatabase.CreateAsset(chest, localArmorPath + chest.name + ".asset");
        AssetDatabase.CreateAsset(arms, localArmorPath + arms.name + ".asset");
        AssetDatabase.CreateAsset(legs, localArmorPath + legs.name + ".asset");
        AssetDatabase.CreateAsset(feet, localArmorPath + feet.name + ".asset");

        AssetDatabase.SaveAssets();
    }
}