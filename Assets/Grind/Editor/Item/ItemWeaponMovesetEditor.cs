﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(ItemWeaponMoveset))]
public class ItemWeaponMovesetEditor : Editor
{
    private ReorderableList motionWeights;

    private SerializedProperty weaponType;

    private void OnEnable()
    {
        motionWeights = new ReorderableList(serializedObject, serializedObject.FindProperty("motionWeights"), true, true, true, true);

        motionWeights.elementHeightCallback = (int index) =>
        {
            return 10 + (2 + EditorGUIUtility.singleLineHeight * 3);
        };

        motionWeights.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            var element = motionWeights.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;
            EditorGUI.PropertyField(new Rect(rect.x, rect.y, EditorGUIUtility.currentViewWidth, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("motion"), new GUIContent("Motion Name"));
            rect.y += 2 + EditorGUIUtility.singleLineHeight;
            EditorGUI.PropertyField(new Rect(rect.x, rect.y, EditorGUIUtility.currentViewWidth, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("weight"), new GUIContent("Motion Weight"));
            rect.y += 2 + EditorGUIUtility.singleLineHeight;
            EditorGUI.PropertyField(new Rect(rect.x, rect.y, EditorGUIUtility.currentViewWidth, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("weightHits"), new GUIContent("Motion Hits"));
        };
        weaponType = serializedObject.FindProperty("weaponType");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.PropertyField(weaponType);
        motionWeights.DoLayoutList();

        if (GUILayout.Button("Assign To All of Weapon Type"))
        {
            AssignToAllOfWeaponType();
        }

        serializedObject.ApplyModifiedProperties();
    }

    /// <summary>
    /// Assign this moveset to all weapons of the given type.
    /// </summary>
    private void AssignToAllOfWeaponType()
    {
        var guids = AssetDatabase.FindAssets("t:ItemWeapon");

        foreach (string guid in guids)
        {
            string path = AssetDatabase.GUIDToAssetPath(guid);

            var item = AssetDatabase.LoadAssetAtPath<ItemWeapon>(path);
            item.moveset = target as ItemWeaponMoveset;

            AssetDatabase.SaveAssets();
        }
    }
}