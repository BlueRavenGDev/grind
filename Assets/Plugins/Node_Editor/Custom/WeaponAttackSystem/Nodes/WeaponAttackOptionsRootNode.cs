﻿using UnityEngine;
using System.Collections;
using NodeEditorFramework;

[Node(false, "Grind/WeaponAttackOptionsRootNode")]
public class WeaponAttackOptionsRootNode : Node
{
    public const string ID = "weaponAttackOptionsRootNode";
    public override string GetID { get { return ID; } }

    [ValueConnectionKnob("Node 1", Direction.Out, "WeaponAttackNodeConnection")]
    public ValueConnectionKnob node1;
    [ValueConnectionKnob("Node 2", Direction.Out, "WeaponAttackNodeConnection")]
    public ValueConnectionKnob node2;
    [ValueConnectionKnob("Node 3", Direction.Out, "WeaponAttackNodeConnection")]
    public ValueConnectionKnob node3;
    [ValueConnectionKnob("Node 4", Direction.Out, "WeaponAttackNodeConnection")]
    public ValueConnectionKnob node4;

    [ValueConnectionKnob("Loop Input", Direction.In, "WeaponAttackOptionsNodeConnection")]
    public ValueConnectionKnob loopInput;

    public WeaponAttackNode GetNextNode(int nodeNum)
    {
        switch (nodeNum)
        {
            case 0:
                if (node1.connected())
                    return node1.connections[0].body as WeaponAttackNode;
                return null;
            case 1:
                if (node2.connected())
                    return node2.connections[0].body as WeaponAttackNode;
                return null;
            case 2:
                if (node3.connected())
                    return node3.connections[0].body as WeaponAttackNode;
                return null;
            case 3:
                if (node4.connected())
                    return node4.connections[0].body as WeaponAttackNode;
                return null;
            default:
                return null;
        }
    }
}
