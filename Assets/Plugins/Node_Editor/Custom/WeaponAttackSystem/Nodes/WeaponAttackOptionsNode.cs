﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;
using System.Collections.Generic;
using NodeEditorFramework;

[Node(false, "Grind/WeaponAttackOptionsNode")]
public class WeaponAttackOptionsNode : WeaponAttackOptionsRootNode
{
    public override bool AllowRecursion { get { return true; } }

    public new const string ID = "weaponAttackOptionsNode";
    public override string GetID { get { return ID; } }

    [ValueConnectionKnob("Input", Direction.In, "WeaponAttackOptionsNodeConnection")]
    public ValueConnectionKnob input;
}

public class WeaponAttackNodeConnection : ValueConnectionType
{
    public override string Identifier { get { return "WeaponAttackOptionsNodeConnection"; } }
    public override Type Type { get { return typeof(WeaponAttackOptionsNode); } }
    public override Color Color { get { return Color.yellow; } }
}
