﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;
using System.Collections.Generic;
using NodeEditorFramework;

public enum WeaponLeadButtons
{
    None,
    Attack1,
    Attack2,
    AttackMod,
    Attack1AndAttackMod,
    Attack2AndAttackMod
}

[Node(false, "Grind/WeaponAttackNode")]
public class WeaponAttackNode : NodeEditorFramework.Node
{
    public const string ID = "weaponAttackNode";
    public override string GetID { get { return ID; } }

    public string motion;
    public WeaponLeadButtons leadButton;

    [ValueConnectionKnob("Comes From", Direction.In, "WeaponAttackNodeConnection")]
    public ValueConnectionKnob comesFrom;
    [ValueConnectionKnob("Leads To", Direction.Out, "WeaponAttackOptionsNodeConnection")]
    public ValueConnectionKnob leadsTo;

    public override void NodeGUI()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("Comes From");
        GUILayout.Label("Leads To");
        GUILayout.EndHorizontal();

        //GUILayout.Space(16);
        motion = GUILayout.TextField(motion);
        leadButton = (WeaponLeadButtons)EditorGUILayout.EnumPopup(leadButton);
    }

    public WeaponAttackOptionsNode GetNextOptions()
    {
        if (leadsTo.connected())
            return leadsTo.connections[0].body as WeaponAttackOptionsNode;
        return null;
    }
}

public class WeaponAttackBranchConnection : ValueConnectionType
{
    public override string Identifier { get { return "WeaponAttackNodeConnection"; } }
    public override Type Type { get { return typeof(WeaponAttackNode); } }
    public override Color Color { get { return Color.green; } }
}