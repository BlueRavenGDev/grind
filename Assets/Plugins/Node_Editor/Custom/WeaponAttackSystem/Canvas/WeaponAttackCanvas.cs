﻿using UnityEngine;
using System.Collections;
using NodeEditorFramework;
using NodeEditorFramework.Standard;
using System.Collections.Generic;
using System.Linq;

[NodeCanvasType("Weapon Attack Canvas")]
public class WeaponAttackCanvas : NodeCanvas
{
    public string idleOptionsID { get { return "weaponAttackOptionsRootNode"; } }
    public WeaponAttackOptionsRootNode idleOptions;

    public WeaponAttackNode GetNextNode(WeaponAttackOptionsRootNode node, WeaponLeadButtons currentButtons)
    {
        if (node)
        {
            WeaponAttackNode nextNode1 = node.GetNextNode(0);
            WeaponAttackNode nextNode2 = node.GetNextNode(1);
            WeaponAttackNode nextNode3 = node.GetNextNode(2);
            WeaponAttackNode nextNode4 = node.GetNextNode(3);

            if (nextNode1 && nextNode1.leadButton == currentButtons)
            {
                return nextNode1;
            }
            else if (nextNode2 && nextNode2.leadButton == currentButtons)
            {
                return nextNode2;
            }
            else if (nextNode3 && nextNode3.leadButton == currentButtons)
            {
                return nextNode3;
            }
            else if (nextNode4 && nextNode4.leadButton == currentButtons)
            {
                return nextNode4;
            }
        }
        return null;
    }

    public WeaponAttackOptionsRootNode GetRootNode()
    {
        return idleOptions = nodes.Find(n => n.GetID == idleOptionsID) as WeaponAttackOptionsRootNode;
    }

    protected override void OnCreate()
    {
        idleOptions = NodeEditorFramework.Node.Create("weaponAttackOptionsRootNode", Vector2.zero) as WeaponAttackOptionsRootNode;
    }

    protected override void ValidateSelf()
    {
        if (idleOptions == null && (idleOptions = nodes.Find(n => n.GetID == idleOptionsID) as WeaponAttackOptionsRootNode) == null)
            idleOptions = NodeEditorFramework.Node.Create(idleOptionsID, Vector2.zero, this) as WeaponAttackOptionsRootNode;
    }
}