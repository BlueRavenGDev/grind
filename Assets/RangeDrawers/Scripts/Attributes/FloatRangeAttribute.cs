﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This code was taken from https://gist.github.com/HilariousCow/1d056da2e3324670a087 and modified slightly for my purposes.
/// </summary>
namespace RangeDrawers
{
    public class FloatRangeAttribute : PropertyAttribute
    {
        public float minLimit, maxLimit;

        public FloatRangeAttribute(float minLimit, float maxLimit)
        {
            this.minLimit = minLimit;
            this.maxLimit = maxLimit;
        }
    }

    [System.Serializable]
    public class FloatRange
    {
        public float rangeStart, rangeEnd;

        public FloatRange(int rangeStart, int rangeEnd)
        {
            this.rangeStart = rangeStart;
            this.rangeEnd = rangeEnd;
        }

        private float GetRandomValue()
        {
            return Random.Range(rangeStart, rangeEnd);
        }

        public bool ValueLiesInRange(float value)
        {
            if (value > rangeStart && value <= rangeEnd)
                return true;
            return false;
        }

        public static implicit operator float(FloatRange d)  // implicit digit to byte conversion operator
        {
            return d.GetRandomValue();
        }
    }
}