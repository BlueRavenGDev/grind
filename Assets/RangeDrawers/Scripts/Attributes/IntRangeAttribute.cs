﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This code was taken from https://gist.github.com/HilariousCow/560db765cf24eb589b00 and modified slightly for my purposes.
/// </summary>
namespace RangeDrawers
{
    public class IntRangeAttribute : PropertyAttribute
    {
        public int minLimit, maxLimit;

        public IntRangeAttribute(int minLimit, int maxLimit)
        {
            this.minLimit = minLimit;
            this.maxLimit = maxLimit;
        }
    }

    [System.Serializable]
    public class IntRange
    {
        public int rangeStart, rangeEnd;

        public IntRange(int rangeStart, int rangeEnd)
        {
            this.rangeStart = rangeStart;
            this.rangeEnd = rangeEnd;
        }

        public bool ValueLiesInRange(float value)
        {
            if (value > rangeStart && value <= rangeEnd)
                return true;
            return false;
        }

        private int GetRandomValue()
        {
            return Random.Range(rangeStart, rangeEnd);
        }

        public static implicit operator int(IntRange d)
        {
            return d.GetRandomValue();
        }
    }
}