﻿//Place in an "Editor" Folder. Keep your folders tidy or I will get you.
//apologies for the magic values below
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This code was taken from https://gist.github.com/HilariousCow/1d056da2e3324670a087 and modified slightly for my purposes.
/// </summary>
namespace RangeDrawers
{
    [CustomPropertyDrawer(typeof(FloatRangeAttribute))]
    public class FloatRangeDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label) + 16;
        }

        // Draw the property inside the given rect
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Now draw the property as a Slider or an IntSlider based on whether it’s a float or integer.
            if (property.type != typeof(FloatRange).Name)
                Debug.LogWarning("Use only with FloatRange type");
            else
            {
                FloatRangeAttribute range = attribute as FloatRangeAttribute;
                SerializedProperty minValue = property.FindPropertyRelative("rangeStart");
                SerializedProperty maxValue = property.FindPropertyRelative("rangeEnd");
                float newMin = minValue.floatValue;
                float newMax = maxValue.floatValue;

                float xDivision = position.width * 0.4f;
                float xLabelDiv = xDivision * 0.125f;

                float yDivision = position.height * 0.5f;
                EditorGUI.LabelField(new Rect(position.x, position.y, xDivision, yDivision)
                , label);


                Rect mmRect = new Rect(position.x + xDivision + xLabelDiv, position.y, position.width - (xDivision + xLabelDiv * 2), yDivision);

                EditorGUI.MinMaxSlider(mmRect, ref newMin, ref newMax, range.minLimit, range.maxLimit);


                Rect minRangeRect = new Rect(position.x + xDivision, position.y, xLabelDiv, yDivision);
                minRangeRect.x += xLabelDiv * 0.5f - 12;
                minRangeRect.width = 24;
                EditorGUI.LabelField(minRangeRect, range.minLimit.ToString());

                Rect maxRangeRect = new Rect(minRangeRect);
                maxRangeRect.x = mmRect.xMax + xLabelDiv * 0.5f - 12;
                maxRangeRect.width = 24;
                EditorGUI.LabelField(maxRangeRect, range.maxLimit.ToString());

                Rect minLabelRect = new Rect(mmRect);
                minLabelRect.x = minLabelRect.x + minLabelRect.width * (newMin / range.maxLimit);
                minLabelRect.x -= 12;
                minLabelRect.y += yDivision;
                minLabelRect.width = 24;
                newMin = Mathf.Clamp(EditorGUI.FloatField(minLabelRect, newMin), range.minLimit, newMax);
                //EditorGUI.LabelField(minLabelRect, newMin.ToString());

                Rect maxLabelRect = new Rect(mmRect);
                maxLabelRect.x = maxLabelRect.x + maxLabelRect.width * (newMax / range.maxLimit);
                maxLabelRect.x -= 12;
                maxLabelRect.x = Mathf.Max(maxLabelRect.x, minLabelRect.xMax);
                maxLabelRect.y += yDivision;
                maxLabelRect.width = 24;
                newMax = Mathf.Clamp(EditorGUI.FloatField(maxLabelRect, newMax), newMin, range.maxLimit);
                //EditorGUI.LabelField(maxLabelRect, newMax.ToString());


                minValue.floatValue = newMin;
                maxValue.floatValue = newMax;
            }
        }
    }
}