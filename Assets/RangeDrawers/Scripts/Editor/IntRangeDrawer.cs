﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This code was taken from https://gist.github.com/HilariousCow/560db765cf24eb589b00 and modified slightly for my purposes.
/// </summary>
namespace RangeDrawers
{
    [CustomPropertyDrawer(typeof(IntRangeAttribute))]
    public class IntRangeDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label) + 16;
        }

        // Draw the property inside the given rect
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Now draw the property as a Slider or an IntSlider based on whether it’s a float or integer.
            if (property.type != typeof(IntRange).Name)
                Debug.LogWarning("Use only with IntRange type");
            else
            {
                IntRangeAttribute range = attribute as IntRangeAttribute;
                SerializedProperty minValue = property.FindPropertyRelative("rangeStart");
                SerializedProperty maxValue = property.FindPropertyRelative("rangeEnd");
                float newMin = minValue.intValue;
                float newMax = maxValue.intValue;

                float xDivision = position.width * 0.4f;
                float xLabelDiv = xDivision * 0.125f;

                float yDivision = position.height * 0.5f;
                EditorGUI.LabelField(new Rect(position.x, position.y, xDivision, yDivision)
                , label);


                Rect mmRect = new Rect(position.x + xDivision + xLabelDiv, position.y, position.width - (xDivision + xLabelDiv * 2), yDivision);

                EditorGUI.MinMaxSlider(mmRect, ref newMin, ref newMax, range.minLimit, range.maxLimit);


                //to deal with rounding on negative values:
                int newMinI = (int)(newMin - (float)range.minLimit) + range.minLimit;
                int newMaxI = (int)(newMax - (float)range.minLimit) + range.minLimit;

                //left label
                Rect minRangeRect = new Rect(position.x + xDivision, position.y, xLabelDiv, yDivision);
                minRangeRect.x += xLabelDiv * 0.5f - 12;
                minRangeRect.width = 24;
                EditorGUI.LabelField(minRangeRect, range.minLimit.ToString());

                //right label
                Rect maxRangeRect = new Rect(minRangeRect);
                maxRangeRect.x = mmRect.xMax;
                maxRangeRect.x = mmRect.xMax + xLabelDiv * 0.5f - 12;
                maxRangeRect.width = 24;
                EditorGUI.LabelField(maxRangeRect, range.maxLimit.ToString());

                int totalRange = Mathf.Max(range.maxLimit - range.minLimit, 1);
                Rect minLabelRect = new Rect(mmRect);
                minLabelRect.x = minLabelRect.x + minLabelRect.width * ((newMin - range.minLimit) / totalRange);
                minLabelRect.x -= 12;
                minLabelRect.y += yDivision;
                minLabelRect.width = 24;
                newMinI = Mathf.Clamp(EditorGUI.IntField(minLabelRect, newMinI), range.minLimit, newMaxI);
                //EditorGUI.LabelField(minLabelRect, newMin.ToString());//old style non moving label

                Rect maxLabelRect = new Rect(mmRect);
                maxLabelRect.x = maxLabelRect.x + maxLabelRect.width * ((newMax - range.minLimit) / totalRange);
                maxLabelRect.x -= 12;
                maxLabelRect.x = Mathf.Max(maxLabelRect.x, minLabelRect.xMax);
                maxLabelRect.y += yDivision;
                maxLabelRect.width = 24;
                newMaxI = Mathf.Clamp(EditorGUI.IntField(maxLabelRect, newMaxI), newMinI, range.maxLimit);
                //EditorGUI.LabelField(maxLabelRect, newMax.ToString());//old style non moving label


                minValue.intValue = (int)newMinI;
                maxValue.intValue = (int)newMaxI;
            }
        }
    }
}